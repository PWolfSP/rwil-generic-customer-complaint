# D!AS Customer Complaint Information

This API provides access to the details of existing after sales customer complaints which are provided by the backend system "DISS".
The default error response types and codes for gateway and authorization can be found within the Hello RW.IL World service description.

IA connector: `ms-connector-ia-workshop-complaint-management-service-v13`


##### `/{partnerkey}/complaint/{vin}`

**Test Data:**

partnerKey: `DEU74623V` 

vin: `WVWZZZ3CZ6E206295`

orderNumberID = `23514375`

acceptanceDate = `2020-08-19`

languageID= `de-DE`
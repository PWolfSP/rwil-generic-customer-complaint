package com.vw.rwil.ccms;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.ccms.config.ServiceConfiguration;
import com.vw.rwil.ccms.config.TestUtils;
import com.vw.rwil.ccms.data.CustomerComplaints;
import com.vw.rwil.rwilutils.exception.RWILException;

@RunWith(SpringRunner.class)
@ContextConfiguration(
		  classes = { ServiceConfiguration.class }, 
		  loader = AnnotationConfigContextLoader.class)
@TestPropertySource(locations="classpath:bootstrap.yaml", properties = { "endpoint.connector=http://localhost:8080/" })
public class ServiceTest {
	
	@MockBean
	RestTemplate restTemplate;
	
	String partnerkey = "DEU99975V";
	String VIN = "WAUZZZF4XGA001654";
	String orderNumberID = "123456";
	String acceptanceDate = "2020-08-29";
	String languageID= "en-EN";
	
	@Autowired
	com.vw.rwil.ccms.service.Service Service;
	
	@Autowired
	TestUtils testUtils;
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetWorkshopComplaints() throws Exception {

		when(restTemplate.exchange(
				Mockito.anyString(), Mockito.any(), Mockito.any(),
				ArgumentMatchers.any(Class.class))
                )
		.thenReturn(new ResponseEntity<Object>(testUtils.mapperJsonToObject("GetWorkshoppComplaintsResponse.json"), HttpStatus.CREATED));
		
		ResponseEntity<CustomerComplaints> responseEntity = Service.serviceComplaint(partnerkey, VIN, orderNumberID, acceptanceDate, languageID);

		assertEquals(200, responseEntity.getStatusCodeValue());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = RWILException.class)
	public void testGetWorkshopComplaintsPartnerkeyInvalid() throws Exception {

		when(restTemplate.exchange(
				Mockito.anyString(), Mockito.any(), Mockito.any(),
				ArgumentMatchers.any(Class.class))
                )
		.thenReturn(new ResponseEntity<Object>(testUtils.mapperJsonToObject("GetWorkshoppComplaintsResponse.json"), HttpStatus.CREATED));
		
		ResponseEntity<CustomerComplaints> responseEntity = Service.serviceComplaint("DEU123456V", VIN, orderNumberID, acceptanceDate, languageID);

		assertEquals(400, responseEntity.getStatusCodeValue());
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = RWILException.class)
	public void testGetWorkshopComplaintsVINInvalid() throws Exception {

		when(restTemplate.exchange(
				Mockito.anyString(), Mockito.any(), Mockito.any(),
				ArgumentMatchers.any(Class.class))
                )
		.thenReturn(new ResponseEntity<Object>(testUtils.mapperJsonToObject("GetWorkshoppComplaintsResponse.json"), HttpStatus.CREATED));
		
		ResponseEntity<CustomerComplaints> responseEntity = Service.serviceComplaint(partnerkey, "WCS123", orderNumberID, acceptanceDate, languageID);

		assertEquals(400, responseEntity.getStatusCodeValue());
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = RWILException.class)
	public void testGetWorkshopComplaintsDateInvalid() throws Exception {

		when(restTemplate.exchange(
				Mockito.anyString(), Mockito.any(), Mockito.any(),
				ArgumentMatchers.any(Class.class))
                )
		.thenReturn(new ResponseEntity<Object>(testUtils.mapperJsonToObject("GetWorkshoppComplaintsResponse.json"), HttpStatus.CREATED));
		
		ResponseEntity<CustomerComplaints> responseEntity = Service.serviceComplaint(partnerkey, VIN, orderNumberID, "2020-13-01", languageID);

		assertEquals(400, responseEntity.getStatusCodeValue());
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = RWILException.class)
	public void testGetWorkshopComplaintsLanguageInvalid() throws Exception {

		when(restTemplate.exchange(
				Mockito.anyString(), Mockito.any(), Mockito.any(),
				ArgumentMatchers.any(Class.class))
                )
		.thenReturn(new ResponseEntity<Object>(testUtils.mapperJsonToObject("GetWorkshoppComplaintsResponse.json"), HttpStatus.CREATED));
		
		ResponseEntity<CustomerComplaints> responseEntity = Service.serviceComplaint(partnerkey, VIN, orderNumberID, acceptanceDate, "deDE");

		assertEquals(400, responseEntity.getStatusCodeValue());
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
	}
}
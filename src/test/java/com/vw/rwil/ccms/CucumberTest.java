package com.vw.rwil.ccms;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/features", glue = {"com.vw.rwil.ccms.stepDefinitions"}, plugin = {"json:target/cucumber.json"})
public class CucumberTest {

}

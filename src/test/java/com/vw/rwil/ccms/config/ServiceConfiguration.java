package com.vw.rwil.ccms.config;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.Properties;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.ccms.controller.Controller;
import com.vw.rwil.ccms.mapping.ObjectMapping;
import com.vw.rwil.ccms.service.Service;
import com.vw.rwil.ccms.service.ServiceImpl;
import com.vw.rwil.ccms.service.exceptionhandler.CustomExceptionResponseHandler;
import com.vw.rwil.ccms.service.utils.Utils;

@Configuration
public class ServiceConfiguration {
	     
		@Bean
		public RestTemplate createRestTemplate() {
			return new RestTemplate();
		}
		
		@Bean
		public ObjectMapping objectMapping() {
			return new ObjectMapping();
		}
		
		@Bean
		public TestUtils testUtils() {
			return new TestUtils();
		}
		
		@Bean
		public Utils utils() {
			return new Utils();
		};
		
		@Bean
		public CustomExceptionResponseHandler customExceptionResponseHandler() {
			return new CustomExceptionResponseHandler();
		}
		
	    @Bean
	    @ConditionalOnMissingBean(value = ServiceImpl.class)
	    public Service defaultService() throws IOException, GeneralSecurityException, URISyntaxException{
	    	ServiceImpl result = new ServiceImpl(); 
	    		return result;
	    }
	    
	    @Bean
	       public Properties myProps(){
	             Properties properties = new Properties();
	             properties.setProperty("endpoint.connector", "myUrl");
	             return properties;
	        }
	    
	    @Bean
	    public Controller defaultController() throws IOException, GeneralSecurityException, URISyntaxException{
	    	Controller result = new Controller();
	        return result;
	    }
}
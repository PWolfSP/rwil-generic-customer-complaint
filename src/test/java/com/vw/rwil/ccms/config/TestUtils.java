package com.vw.rwil.ccms.config;

import java.io.InputStream;
import java.util.Scanner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vw.rwil.rwilutils.common.RWILUtil;

public class TestUtils {
	     
	public Object mapperJsonToObject (String fileName) throws Exception {
		
		ObjectMapper mapperObject = new ObjectMapper();
		Object connectorResponse = mapperObject.readValue(bodyBuilder(fileName), Object.class);
		
		return connectorResponse;
	}
	
	@SuppressWarnings("resource")
	public String bodyBuilder (String jsonFile) throws Exception {
		
		String body = "{}";
		InputStream stream = RWILUtil.getResourceAsInputStream(jsonFile);
		if (stream != null) {
			Scanner scanner = new Scanner(stream).useDelimiter("\\A");
			body = scanner.hasNext() ? scanner.next() : "";
		}

		return body;
	}
}
package com.vw.rwil.ccms.stepDefinitions;

import com.github.fge.jsonschema.SchemaVersion;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.RestAssured.*;
import io.restassured.matcher.RestAssuredMatchers.*;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Slf4j
public class Steps {

    private static final String BASE_URL = "https://localhost:90";
    private static final String IA_CONNETCOR_URL = "https://localhost:90";
    private static final String TOKEN_URL = "https://localhost:90";


    private static String partnerKey;
    private static String ordernumberid;
    private static String vehicleIdentificationNumber;
    private static Response response;
    private static ClientHttpResponse responseIA;
    private static String request;
    private static String token = "";
    private static String jsonString;

    JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory.newBuilder()
            .setValidationConfiguration(
                    ValidationConfiguration.newBuilder()
                            .setDefaultVersion(SchemaVersion.DRAFTV4)
                            .freeze())
            .freeze();

    private CountDownLatch timeout = new CountDownLatch(1);

    @Autowired
    private static RestTemplate restTemplate;

    @Before
    private void prepareRestTemplate(){
        List<ClientHttpRequestInterceptor> interceptors
                = restTemplate.getInterceptors();

        interceptors.add(new ClientHttpRequestInterceptor() {
            @Override
            public ClientHttpResponse intercept(HttpRequest requestHeader, byte[] requestBody, ClientHttpRequestExecution execution) throws IOException {
                request = new JSONObject(new String(requestBody)).toString();
                ClientHttpResponse response = execution.execute(requestHeader,requestBody);
                timeout.countDown();
                responseIA = response;
                return response;
            }
        });
        restTemplate.setInterceptors(interceptors);
    }

    @Given("^an available IA Connector for workshop complaint management$")
    public void an_available_IA_Connector_for_workshop_complaint_management() throws Throwable {

        RestAssured.baseURI = IA_CONNETCOR_URL;

        RequestSpecification request = RestAssured.given();

        request.header("Authorization", "Bearer " + token);

        try {
            request.get("/alive");
        }catch (Exception e){

        }

        log.info("checking for available IA connector");
    }

    @Given("^an authenticated client$")
    public void an_authenticated_client() throws Throwable {

        RestAssured.baseURI = TOKEN_URL;
        RequestSpecification request = RestAssured.given();

        request.header("Content-Type", "application/json");
        response = request.body("").post("TOKEN_URL");

        String jsonString = response.asString();
        token = JsonPath.from(jsonString).get("token");

        log.info("Token received for Authenticated User");
    }

    @Given("^the PartnerKey (.*)$")
    public void the_PartnerKey_WVWZZZ_NZ_D(String arg1) throws Throwable {
        partnerKey = arg1;
        log.info("Using PartnerKey " + partnerKey);
    }

    @Given("^the Vehicle Identification Number (.*)$")
    public void the_Vehicle_Identification_Number_DEU_V(String arg1) throws Throwable {
        vehicleIdentificationNumber = arg1;
        log.info("Using Vehicle Identification Number " + vehicleIdentificationNumber);
    }

    @When("^the client makes a GET Request to Customer Complaint Information Endpoint$")
    public void the_client_makes_a_GET_Request_to_Customer_Complaint_Information_Endpoint() throws Throwable {
        RestAssured.baseURI = BASE_URL;

        RequestSpecification request = RestAssured.given();

        request.header("Authorization", "Bearer " + token);

        request.get( "/" + partnerKey + "/complaint/" + vehicleIdentificationNumber);
        log.info("Request send to Customer Complaint Information Endpoint");
    }

    @Then("^a GET Request that can be validated against the json schema '(.*)' is created$")
    public void a_GET_Request_that_can_be_validated_against_the_json_schema_ia_connector_request_schema_json_is_created(String arg1) throws Throwable {

        timeout.await(0, TimeUnit.MILLISECONDS);


        URL schema = getClass().getClassLoader().getResource( "schemas/" + arg1);

        assertTrue(JsonSchemaValidator.matchesJsonSchemaInClasspath(schema.getPath()).using(jsonSchemaFactory).matches(request));

        log.info("Request matches Schema");
    }

    @Then("^the GET Request is send to the IA Connector$")
    public void the_GET_Request_is_send_to_the_IA_Connector() throws Throwable {
        RestAssured.baseURI = IA_CONNETCOR_URL;
        RequestSpecification request = RestAssured.given();

        request.header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json");

        response = request.get("/" + partnerKey + "/getWorkshopComplaint");
        log.info("Request send to IA Connector");
    }

    @When("^a Response is received from IA Connector within (\\d+) milliseconds$")
    public void a_Response_is_received_from_IA_Connector(int arg1) throws Throwable {

        timeout.await(arg1, TimeUnit.MILLISECONDS);

        assertNotNull(responseIA);

        log.info("Response received from IA Connector");
    }

    @When("^the Response can be validated against the json schema '(.*)'$")
    public void the_Response_can_be_validated_against_the_json_schema_ia_connector_response_schema_json(String arg1) throws Throwable {

        URL schema = getClass().getClassLoader().getResource( "schemas/" + arg1);

        assertTrue(JsonSchemaValidator.matchesJsonSchemaInClasspath(schema.getPath()).using(jsonSchemaFactory).matches(responseIA));

        log.info("Response matches Schema");
    }

    @Given("^the Order Number ID (.*)$")
    public void the_Order_Number_ID(String arg1) throws Throwable {
        ordernumberid = arg1;
        log.info("Using PartnerKey " + partnerKey);
    }

    @Given("^valid customer data available to the IA Connector$")
    public void valid_customer_data_available_to_the_IA_Connector() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Given("^the Acceptance Date (.*)$")
    public void the_Acceptance_Date(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^a response for an invalid request compliant with '(.*)' is created$")
    public void a_response_for_an_invalid_request_compliant_with_error_failure_schema_json_is_created(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^a Response that can be validated against the json schema '(.*)' is created$")
    public void a_Response_that_can_be_validated_against_the_json_schema_workshop_complaint_management_response_schema_json_is_created(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the Response is send to client with status code (.*)$")
    public void the_Response_is_send_to_client_with_status_code(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the response is send to the client with status code (.*)$")
    public void the_response_is_send_to_the_client_with_status_code(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Given("^the Partner Key (.*)$")
    public void the_Partner_Key_WVWZZZ_NZ_D(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}

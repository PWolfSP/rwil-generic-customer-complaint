package com.vw.rwil.ccms.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.GetWorkshopComplaintsType;
import com.vw.rwil.ccms.config.ServiceConfiguration;
import com.vw.rwil.ccms.config.TestUtils;
import com.vw.rwil.ccms.mapping.ObjectMapping;


@RunWith(SpringRunner.class)
@ContextConfiguration(
		  classes = { ServiceConfiguration.class }, 
		  loader = AnnotationConfigContextLoader.class,
		  initializers = ConfigFileApplicationContextInitializer.class)
@TestPropertySource(locations="classpath:bootstrap.yaml", properties = {"endpoint.connector=http://localhost:8080/"})
@WebAppConfiguration
public class ControllerTest {
	
	protected MockMvc mvc;
	
	@MockBean
	RestTemplate restTemplate;
	
	@Autowired
    private Controller controller;

	String partnerkey = "DEU99975V";
	String VIN = "WAUZZZF4XGA001654";
	String orderNumberID = "123456";
	String acceptanceDate = "2020-08-29";
	String languageID= "en-EN";
	
	@Autowired
	com.vw.rwil.ccms.service.Service Service;
	
	@Autowired
	TestUtils testUtils;
	
	@Autowired
	ObjectMapping objectMapping;
	
	@Before
	public void setUp() throws RestClientException, Exception {
		mvc = MockMvcBuilders.standaloneSetup(controller).build();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testControllerComplaints() throws Exception {

		when(restTemplate.exchange(
				Mockito.anyString(), Mockito.any(), Mockito.any(),
				ArgumentMatchers.any(Class.class))
                )
		.thenReturn(new ResponseEntity<Object>(testUtils.mapperJsonToObject("GetWorkshoppComplaintsResponse.json"), HttpStatus.CREATED));
		
		String bodyJSON = new Gson().toJson(objectMapping.buildWorkshopComplaintsRequest(partnerkey, VIN, orderNumberID, acceptanceDate, languageID), GetWorkshopComplaintsType.class);
		
		URI uri = new URI("/" + partnerkey + "/complaints/" + VIN + "?acceptanceDate=" + acceptanceDate + "&languageID="+languageID+"&orderNumberID="+orderNumberID+"");
		
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
			    .contentType(MediaType.APPLICATION_JSON)
			    .content(bodyJSON)).andReturn();
		
		MockHttpServletResponse mockHttpServletResponse = mvcResult.getResponse();
		
		assertEquals(200, mockHttpServletResponse.getStatus());
	}
}
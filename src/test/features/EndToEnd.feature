#noinspection CucumberUndefinedStep
  @RWIL-8
  Feature: End to End test for workshop complaint management service API
    Description: These tests cover the successful transfer of Data through the API

  Link to Swagger UI: http://localhost:8082/swagger-ui.html

  Background: Connection to IA Connector can be established
    Given an available IA Connector for workshop complaint management
    And valid customer data available to the IA Connector
    And an authenticated client

  @id:8
  Scenario Outline: A client can request complaint information from to the backend
    Given the PartnerKey <PartnerKey>
    And the Vehicle Identification Number <Vehicle Identification Number>
    And the Acceptance Date <Acceptance Date>
    And the Language ID <Language ID>
    And the Order Number ID <Order Number ID>
    When the client makes a GET Request to Customer Complaint Information Endpoint
    Then a GET Request that can be validated against the json schema 'ia-connector-request-schema.json' is created
    And the GET Request is send to the IA Connector

    Examples:
      |        PartnerKey | Vehicle Identification Number | Acceptance Date | Language ID | Order Number ID |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |             |                 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |             |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |       de-DE |                 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |       de-DE |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |       de-DE |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |       de-DE |                 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |             |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |             |                 |

  @id:9
  Scenario Outline: A client receives complaint data from the backend after a request
    Given the PartnerKey <PartnerKey>
    And the Vehicle Identification Number <Vehicle Identification Number>
    And the Acceptance Date <Acceptance Date>
    And the Language ID <Language ID>
    And the Order Number ID <Order Number ID>
    When the client makes a GET Request to Customer Complaint Information Endpoint
    Then a Response is received from IA Connector within 50000 milliseconds
    And the Response can be validated against the json schema 'ia-connector-response-schema.json'

    Examples:
      |        PartnerKey | Vehicle Identification Number | Acceptance Date | Language ID | Order Number ID |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |             |                 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |             |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |       de-DE |                 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |       de-DE |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |       de-DE |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |       de-DE |                 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |             |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |             |                 |

  @id:10
  Scenario Outline: A client sends a request with invalid parameters and receives an error
    Given the Partner Key <PartnerKey>
    And the Vehicle Identification Number <Vehicle Identification Number>
    And the Acceptance Date <Acceptance Date>
    And the Language ID <Language ID>
    When the client makes a GET Request to Customer Complaint Information Endpoint
    Then a response for an invalid request compliant with 'error-400-failure-schema.json' is created
    And the response is send to the client with status code 400


    Examples:
      |        PartnerKey | Vehicle Identification Number | Acceptance Date | Language ID |
      |          invalid  |                     DEU74623V |                 |             |
      |          invalid  |                     DEU74623V |        invalid  |    invalid  |
      |          invalid  |                      invalid  |        invalid  |    invalid  |
      |          invalid  |                      invalid  |                 |    invalid  |
      |          invalid  |                      invalid  |                 |             |
      | WVWZZZ9NZ5D010313 |                      invalid  |                 |             |
      | WVWZZZ9NZ5D010313 |                      invalid  |        invalid  |    invalid  |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |        invalid  |             |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |        invalid  |    invalid  |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |    invalid  |

  @id:4 @fast
  Scenario Outline: A client receives complaint data from the backend after a request
    Given the PartnerKey <PartnerKey>
    And the Vehicle Identification Number <Vehicle Identification Number>
    And the Acceptance Date <Acceptance Date>
    And the Language ID <Language ID>
    And the Order Number ID <Order Number ID>
    When the client has made a request to the complaint service
    Then a Response that can be validated against the json schema 'workshop-complaint-management-response-schema.json' is created
    And the Response is send to client with status code 200

    Examples:
      |        PartnerKey | Vehicle Identification Number | Acceptance Date | Language ID | Order Number ID |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |             |                 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |             |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |       de-DE |                 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |                 |       de-DE |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |       de-DE |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |       de-DE |                 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |             |        23514375 |
      | WVWZZZ9NZ5D010313 |                     DEU74623V |      2020-08-19 |             |                 |
@RWIL-2
Feature: The Service should be configurable for requests and

  Background:
    Given an available IA Connector for workshop complaint management
    And valid customer data available to the IA Connector
    And an authenticated client


	#    Description: These tests cover the successful transfer of Data through the API
	#
	#  Link to Swagger UI: http://localhost:8082/swagger-ui.html
  @id:7
  Scenario Outline: The Service is configured only with jslt and schemas and a request is made to the API
    Given the Partner Key <PartnerKey>
    And the Vehicle Identification Number <Vehicle Identification Number>
    And the Acceptance Date <Acceptance Date>
    And the Language ID <Language ID>
    When the client makes a GET Request to Customer Complaint Information Endpoint
    Then a response is send to the client with status code 200

    Examples:
      | PartnerKey        | Vehicle Identification Number | Acceptance Date | Language ID |
      | WVWZZZ9NZ5D010313 | DEU74623V                     |                 |             |

  Scenario Outline: A request that does not comply with the schemas is made against Service API
    Given the Partner Key <PartnerKey>
    And the Vehicle Identification Number <Vehicle Identification Number>
    And the Acceptance Date <Acceptance Date>
    And the Language ID <Language ID>
    When the client makes a GET Request to Customer Complaint Information Endpoint
    Then a response is send to the client with status code 400

    Examples:
      | PartnerKey        | Vehicle Identification Number | Acceptance Date | Language ID |
      | WVWZZZ9NZ5D010313 | <invalid>                     |                 |             |
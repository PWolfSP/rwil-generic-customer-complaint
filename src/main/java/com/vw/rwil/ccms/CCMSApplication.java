package com.vw.rwil.ccms;

import com.vw.rwil.rwilutils.RWILSpringApplication;

import lombok.Generated;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@Generated
@SpringBootApplication
public class CCMSApplication {

    public static void main(String[] args) {
        RWILSpringApplication.run();
    }
}

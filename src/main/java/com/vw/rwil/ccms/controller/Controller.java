package com.vw.rwil.ccms.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vw.rwil.ccms.data.CustomerComplaints;
import com.vw.rwil.ccms.service.Service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(value = "Customer Complaint Information",  description = " ", tags = {"Customer Complaint Information"})
public class Controller {

    @Autowired
    private Service service;

    @ApiOperation(value = "Retrieve customer complaints for a specific vehicle", notes= "This operation delivers the customer complaint information for a specific vehicle.\r\n\n "
    		+ "<h2>Connected Services and Backends: </h2><ul><li>CSP/AfterSales/WorkshopComplaintManagementService V1.3.1.2 (IA) - DISS</li></ul></br>")
    @RequestMapping(value = {"{partnerkey}/complaints/{vin}"}, 
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerComplaints> customerComplaintInformation(
        @ApiParam(required = true, example = "DEU74623V", value = 
            "The partner key is build up from three elements: country, partner number and brand:\r\n" + 
            "Country: The country the dealer works in. Using ISO3166-1-3alpha codes.\r\n" + 
            "Example: DEU\r\n" + 
            "Partner number: 5-digit alphanumeric string given by the importer.\r\n" + 
            "Example: 74623.\r\n" + 
            "Brand: Brands inside Volkswagen (1-digit).\r\n" + 
            "Example: V\r\n\n"
            + "<i>Example: DEU74623V</i>")
        @PathVariable String partnerkey,
        @ApiParam(required = true, example = "WVWZZZ9NZ5D010313", value = "Vehicle Identification Number (Chassis number)\r\n\n"
        		+ "<i>Example: WVWZZZ9NZ5D010313</i>")
        @PathVariable String vin,
        @ApiParam(required = false, example = "23514375", value = "Unique number of the order in the dealership system\r\n\n"
        		+ "<i>Example: 23514375</i>")
        @RequestParam Optional<String> orderNumberID,
        @ApiParam(required = false, example = "2020-08-19", value = "Vehicle acceptance date in the workshop\r\n\n"
        		+ "<i>Example: 2020-08-19</i>")
        @RequestParam Optional<String> acceptanceDate,
        @ApiParam(required = false, example = "de-DE", value = "Language in which the texts should be provided. Supported format is <iso 639-1> - <iso 3166-1 alpha-2>\r\n\n"
        		+ "<i>Example: de-DE</i>")
        @RequestParam Optional<String> languageID
    ) throws Exception {
        return service.serviceComplaint(partnerkey, vin, orderNumberID.isPresent() ? orderNumberID.get() : null,
        		acceptanceDate.isPresent() ? acceptanceDate.get() : null, languageID.isPresent() ? languageID.get() : null);
    }
}

package com.vw.rwil.ccms.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@lombok.Generated
@Data
public class CustomerComplaint {
	
	@ApiModelProperty(notes = "Complaint / customer statement identification Number.", example = "548796", 
	        required = false)
    String complaintID;
	
	@ApiModelProperty(notes = "Complaint / customer statement, related to coding.", example = "something rattles when driving", 
	        required = false)
    String customerStatement;
}

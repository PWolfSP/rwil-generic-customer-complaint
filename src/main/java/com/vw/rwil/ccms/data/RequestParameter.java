package com.vw.rwil.ccms.data;

import lombok.Data;

@lombok.Generated
@Data
public class RequestParameter {
	// required
	public String partnerkey;
	public String vin;
	public String urlPart;
	// optional
	public String acceptanceDate;
	public String languageID;
	public String orderNumberID;
}


package com.vw.rwil.ccms.data;

import java.util.List;

import lombok.Data;

@lombok.Generated
@Data
public class CustomerComplaints {
	
    List<CustomerComplaint> customerComplaints;
}

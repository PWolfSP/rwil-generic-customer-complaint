package com.vw.rwil.ccms.mapping;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.GetWorkshopComplaintsType;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.ShowWorkshopComplaintsType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.CountryCodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;
import com.volkswagenag.xmldefs.dd.businesspartner.DealerIdentifierType;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerIdentifierType;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerKeyType;
import com.volkswagenag.xmldefs.dd.vehicle.VehicleIdentifierType;
import com.volkswagenag.xmldefs.dd.workshopcomplaint.ClassificationType;
import com.volkswagenag.xmldefs.dd.workshopcomplaint.WorkshopComplaintType;
import com.volkswagenag.xmldefs.dd.workshoporder.DealerType;
import com.volkswagenag.xmldefs.dd.workshoporder.WorkshopOrderIdentifierType;
import com.vw.rwil.ccms.data.CustomerComplaint;
import com.vw.rwil.ccms.data.CustomerComplaints;
import com.vw.rwil.rwilutils.patterns.ia.utils.WSDLUtil;

@Component
public class ObjectMapping {

	
	public GetWorkshopComplaintsType buildWorkshopComplaintsRequest(String partnerkey, String VIN, String orderNumberID, String acceptanceDate, String languageID) {
		
		GetWorkshopComplaintsType workshopComplaintsType = new GetWorkshopComplaintsType();
		WorkshopComplaintType workshopComplaintType = new WorkshopComplaintType();
		WorkshopOrderIdentifierType workshopOrderIdentifierType = new WorkshopOrderIdentifierType();
		
		DealerType dealerType = new DealerType();
		PartnerIdentifierType partnerIdentifierType = new PartnerIdentifierType();
		DealerIdentifierType dealerIdentifierType = new DealerIdentifierType();
		PartnerKeyType partnerKeyType = new PartnerKeyType();
		CountryCodeType country = new CountryCodeType();
		country.setValue(partnerkey.substring(0,3));
		partnerKeyType.setCountry(country);
		partnerKeyType.setPartnerNumber(partnerkey.substring(3,8));
		CodeType brand = new CodeType();
		brand.setValue(WSDLUtil.getBrand(partnerkey));
		partnerKeyType.setBrand(brand);
	
		dealerIdentifierType.setPartnerKey(partnerKeyType);
		partnerIdentifierType.setDealerIdentifier(dealerIdentifierType);
		dealerType.setDealerRef(partnerIdentifierType);
		workshopOrderIdentifierType.setDealer(dealerType);
		
		VehicleIdentifierType vehicleIdentifierType = new VehicleIdentifierType();
		IdentifierType vinIdentifierType = new IdentifierType();
		vinIdentifierType.setValue(VIN);
		vehicleIdentifierType.setVIN(vinIdentifierType);
		workshopComplaintType.setVehicleRef(vehicleIdentifierType);
		
		if(orderNumberID != null) {
			
			IdentifierType identifierType = new IdentifierType();
			identifierType.setValue(orderNumberID);
			workshopOrderIdentifierType.setWorkshopOrderUID(identifierType);
		}

		if(acceptanceDate != null) {
			workshopOrderIdentifierType.setAcceptanceDate(acceptanceDate);
		}
		
		if (languageID != null) {
			workshopComplaintsType.setLanguageID(languageID);
		}
		
		workshopComplaintType.setWorkshopOrderRef(workshopOrderIdentifierType);
		workshopComplaintsType.setWorkshopComplaint(workshopComplaintType);
		
		return workshopComplaintsType;
	}
	
	public CustomerComplaints mapCustomerComplaintsResponse(ShowWorkshopComplaintsType objectResponse){
		
		CustomerComplaints serviceResponse = new CustomerComplaints();
        
        List<CustomerComplaint> listCustomerComplaints = new LinkedList<CustomerComplaint>();
        List<WorkshopComplaintType> listWorkshopComplaintType = objectResponse.getWorkshopComplaints().getWorkshopComplaint();
        
        for(WorkshopComplaintType workshopComplaintType : listWorkshopComplaintType) {	
        	
        	CustomerComplaint workshopComplaint = new CustomerComplaint();
        	workshopComplaint.setComplaintID(workshopComplaintType.getWorkshopComplaintIdentifier().getWorkshopComplaintUID().getValue());
        	
        	if(workshopComplaintType.getClassifications() != null) {
        		if(!workshopComplaintType.getClassifications().getClassification().isEmpty()) {
            		
                	List<ClassificationType> listClassificationTypes = workshopComplaintType.getClassifications().getClassification();
                	workshopComplaint.setCustomerStatement(listClassificationTypes.get(0).getTestimony() == null ? null 
                			: listClassificationTypes.get(0).getTestimony().getValue());
                }
        	}
        	
        	listCustomerComplaints.add(workshopComplaint);
		}
        serviceResponse.setCustomerComplaints(listCustomerComplaints);
        return serviceResponse;
	}
}
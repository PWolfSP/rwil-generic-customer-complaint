package com.vw.rwil.ccms.config;

import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.rwilutils.common.RWILUtil;

import lombok.Generated;
import lombok.NoArgsConstructor;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;

@Configuration
@NoArgsConstructor
@Generated
public class RestTemplateConfig {

	@Bean(name = "restTemplate")
	public RestTemplate getProxyAutoConfigRestTemplate() {

		Builder okClientBuilder = new OkHttpClient().newBuilder();
		
		okClientBuilder.readTimeout(Long.parseLong(RWILUtil.getProperty("timeout.read", "10")), TimeUnit.SECONDS);
		okClientBuilder.writeTimeout(Long.parseLong(RWILUtil.getProperty("timeout.write", "10")), TimeUnit.SECONDS);
		okClientBuilder.connectTimeout(Long.parseLong(RWILUtil.getProperty("timeout.connect", "10")), TimeUnit.SECONDS);
		
		return new RestTemplate(new OkHttp3ClientHttpRequestFactory(okClientBuilder.build()));
	}
}
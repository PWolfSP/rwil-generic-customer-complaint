package com.vw.rwil.ccms.factory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResponseDutyImpl implements ResponseDuty {

	@Override
	public void transformToXmlIaRequest() {
		log.debug("Mock: transform entry to IA-request.XML");
	}

	// check if IA-Request.XML is valid
	@Override
	public Boolean validateWSDL() {
		log.debug("Mock: validate IA-Request.XML");
		return true;
	}

	// if WDSL valid process IA-Request and generate IA-Response.XML
	@Override
	public void createXmlIaResponse() {
		log.debug("Mock: process valid IA-Request.XML and generate IA-Response.XML");
	}

	// if WDSL invalid transform response to IA-Response.JSON
	@Override
	public void transformToJsonIaResponse() {
		log.debug("Mock: transform Response to IA-Resonse.JSON");
	}

	@Override
	public void processJsonIaResponse() {
		log.debug("Mock: process IA-Response.JSON");
	}

	@Override
	public void createJsonResponse() {
		log.debug("Mock: create BS-Response.JSON");
	}

}

package com.vw.rwil.ccms.factory;

public interface ResponseDuty {

	// transform entry to IA-request.XML
	void transformToXmlIaRequest();

	// check if IA-Request.XML is valid
	Boolean validateWSDL();

	// if WDSL valid process IA-Request and generate IA-Response.XML
	void createXmlIaResponse();

	// if WDSL invalid transform response to IA-Response.JSON
	void transformToJsonIaResponse();

	// process IA-Response.JSON
	void processJsonIaResponse();

	// create BS-Response.JSON
	void createJsonResponse();
}

package com.vw.rwil.ccms.factory;

import com.vw.rwil.ccms.data.RequestParameter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExchangeFactory {

	RequestDutyImpl requestDuty = new RequestDutyImpl();
	ResponseDutyImpl responseDuty = new ResponseDutyImpl();

	// TDB
	public RequestDutyImpl handleRequest(RequestParameter requestParam) throws Exception {
		requestDuty.validateParameter(requestParam);
		requestDuty.createJsonRequest(requestParam);
		requestDuty.checkRedisCache();

		return requestDuty;
	}

	// opt. if no cache entry available and specific operations has to be done
	public void performSpecificRequestOperations() {
		log.debug("Mock: no specific operation has to be performed on the request");
	}

	// with jslt
	// req. if no cache entry available transform to
	// IA-Request.JSON / BS-request.jslt
	public void transformToJsonIaRequest(RequestParameter reqestParam) {
		log.debug("Mock: transform to IA-Request.JSON: ");
		String iaRequest = "\n{\r\n" + "    \"workshopComplaint\": {\r\n" + "        \"workshopOrderRef\": {\r\n"
				+ "            \"workshopOrderUID\": {\r\n" + "                \"value\": \"7464401\"\r\n"
				+ "            },\r\n" + "            \"dealer\": {\r\n" + "                \"dealerRef\": {\r\n"
				+ "                    \"dealerIdentifier\": {\r\n" + "                        \"partnerKey\": {\r\n"
				+ "                            \"country\": {\r\n"
				+ "                                \"value\": \"DEU\"\r\n" + "                            },\r\n"
				+ "                            \"brand\": {\r\n"
				+ "                                \"value\": \"V\"\r\n" + "                            },\r\n"
				+ "                            \"partnerNumber\": \"99975\"\r\n" + "                        }\r\n"
				+ "                    }\r\n" + "                }\r\n" + "            },\r\n"
				+ "            \"acceptanceDate\": \"" + reqestParam.acceptanceDate + "\"\r\n" + "        },\r\n"
				+ "        \"vehicleRef\": {\r\n" + "            \"vin\": {\r\n" + "                \"value\": \""
				+ reqestParam.vin + "\"\r\n" + "            }\r\n" + "        }\r\n" + "    }\r\n" + "}";
		log.debug(iaRequest);
	}

	// TBD see retail uis ois orch as reference
	public void orchestrate() {
		log.debug(
				"Mock: orchestrate (transform JSON Request to WSDL XML, transform of Response XML to JSON)");
		responseDuty.transformToXmlIaRequest();
		if (responseDuty.validateWSDL()) {
			log.debug("Mock: IA-Request.XMLcis valid");
			responseDuty.createXmlIaResponse();
		} else {
			log.debug("Mock: IA-Request.XML is invalid");
			responseDuty.transformToJsonIaResponse();
		}
	}

	// process IA-Response.JSON and create BS-Response.JSON
	public void handleResponse() {
		log.debug("Mock: handle Response (process IA-Response.JSON and create BS-Response.JSON)");
		responseDuty.processJsonIaResponse();
		responseDuty.createJsonResponse();

	}

	// if cached entry available and specific operations has to be done
	public void performSpecificResponseOperations() {
		log.debug("Mock: no specific operation has to be performed on the response");
	}

}

package com.vw.rwil.ccms.factory.validation;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class ValidationHelper {

	static public Map<String, String> buildIssues(String partnerkey, String VIN, String orderNumberID,
			String acceptanceDate,
			String languageID) {
		Map<String, String> details = new HashMap<>();

		if (partnerkey != null) {
			details.put("partnerkey", partnerkey);
		}
		if (VIN != null) {
			details.put("vin", VIN);
		}
		if (orderNumberID != null) {
			details.put("orderNumberID", orderNumberID);
		}
		if (acceptanceDate != null) {
			details.put("acceptanceDate", acceptanceDate);
		}
		if (languageID != null) {
			details.put("languageID", languageID);
		}
		return details;
	}

	static public Boolean isDateFormat(String acceptanceDate) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(acceptanceDate);
			return true;
		} catch (ParseException ex) {
			return false;
		}
	}

	static public Boolean isLanguageFormat(String languageID) {

		Pattern LANGUAGE_PATTERN = Pattern.compile("^\\S{2}-\\S{2}$");
		return LANGUAGE_PATTERN.matcher(languageID).matches();
	}
}

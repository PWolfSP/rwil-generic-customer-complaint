package com.vw.rwil.ccms.factory;

import com.google.gson.Gson;
import com.vw.rwil.ccms.data.RequestParameter;
import com.vw.rwil.ccms.factory.validation.ValidationHelper;
import com.vw.rwil.rwilutils.exception.RWILExceptionBuilder;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RequestDutyImpl implements RequestDuty {

	// validate parameter, if invalid send BAD_REQUEST error
	@Override
	public void validateParameter(RequestParameter requestParam) throws Exception {
		log.debug("Mock: Parameter are valid");

		if (requestParam.partnerkey.length() != 9) {
			throw new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("Partnerkey is invalid!")
					.withDetail("Partnerkey is invalid!")
					.withIssues(ValidationHelper.buildIssues(requestParam.partnerkey, null, null, null, null)).build();
		}
		if (requestParam.vin.length() != 17) {
			throw new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("VIN format is invalid!")
					.withDetail("VIN format is invalid!")
					.withIssues(ValidationHelper.buildIssues(null, requestParam.vin, null, null, null)).build();
		}
		if (requestParam.acceptanceDate != null) {
			if (!ValidationHelper.isDateFormat(requestParam.acceptanceDate)) {
				throw new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("Date format is invalid!")
						.withDetail("Date format is invalid!")
						.withIssues(ValidationHelper.buildIssues(null, null, null, requestParam.acceptanceDate, null))
						.build();
			}
		}
		if (requestParam.languageID != null) {
			if (!ValidationHelper.isLanguageFormat(requestParam.languageID)) {
				throw new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST)
						.withTitle("LanguageID format is invalid!").withDetail("LanguageID format is invalid!")
						.withIssues(ValidationHelper.buildIssues(null, null, null, null, requestParam.languageID))
						.build();
			}
		}
	}

	// TDB: validate JSON
	@Override
	public String createJsonRequest(RequestParameter requestParam) {
		log.debug("Mock: create BS-Request.JSON: ");
		String bsRequest = "{\r\n" + "		    \"URL\": [ \"" + requestParam.partnerkey + "\", \""
				+ requestParam.urlPart + "\", \"" + requestParam.vin + "\" ],\r\n" + "		    \"orderNumberID\": "
				+ requestParam.orderNumberID + ",\r\n" + "		    \"acceptanceDate\": \""
				+ requestParam.acceptanceDate + "\"\r\n" + "		}";
		log.debug("\n" + bsRequest);
		String json = new Gson().toJson(bsRequest);
		log.debug("Mock: validate BS-Request.JSON ");
		// TDB validate BS-Request.JSON with justify
//		JsonValidationService service = JsonValidationService.newInstance();
//		StringReader reader = new StringReader(bsRequest);
//		JsonSchema schema = service.readSchema(reader);
//		ProblemHandler handler = service.createProblemPrinter(log::info);
//
//		Path path = Paths.get("BS-Request.json");
//		try (JsonParser parser = service.createParser(path, schema, handler)) {
//			while (parser.hasNext()) {
//				JsonParser.Event event = parser.next();
//			}
//		}
		log.debug("Mock: BS-Request.JSON is valid");
		return json;
	}

	// TBD
	// if valide AND cached: fetch entry and send it as response
	// if valide AND uncached: create new entry and fill it with data
	@Override
	public void checkRedisCache() {
		log.debug("Mock: checked REDIS Cache: no entry found.");
	}

}

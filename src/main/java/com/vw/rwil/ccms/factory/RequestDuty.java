package com.vw.rwil.ccms.factory;

import com.vw.rwil.ccms.data.RequestParameter;

public interface RequestDuty {

	// if invalid send error
	void validateParameter(RequestParameter requestParam) throws Exception;

	// create BS-Request.JSON
	String createJsonRequest(RequestParameter requestParam);

	// TDB: see aftersalespackagecatalogue as reference
	// if valide AND cached: fetch entry and send it as response
	// if valide AND uncached: create new entry and fill it with data
	void checkRedisCache();

}

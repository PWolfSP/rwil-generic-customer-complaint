package com.vw.rwil.ccms.service.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.vw.rwil.rwilutils.exception.RWILExceptionBuilder;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

/**
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
@Component
@org.springframework.stereotype.Service
public class Utils {

	public Map<String, String> buildIssues(String partnerkey, String VIN, String orderNumberID, String acceptanceDate, String languageID){
    	Map<String, String> details = new HashMap<>();
		
    	if (partnerkey != null) {
    		details.put("partnerkey", partnerkey);
		} if (VIN != null) {
    		details.put("vin", VIN);
		} if (orderNumberID != null) {
			details.put("orderNumberID", orderNumberID);
		} if (acceptanceDate != null) {
			details.put("acceptanceDate", acceptanceDate);
		} if (languageID != null) {
			details.put("languageID", languageID);
		}
		return details;
    }
	
	 public void validateRequest(String partnerkey, String VIN, String orderNumberID, String acceptanceDate, String languageID) {
	    	
	    	if (partnerkey.length() != 9) {
	    		throw new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("Partnerkey is invalid!")
	        	.withDetail("Partnerkey is invalid!").withIssues(buildIssues(partnerkey, null, null, null, null)).build();
	    	}
	    	if (VIN.length() != 17) {
	    		throw new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("VIN format is invalid!")
	        	.withDetail("VIN format is invalid!").withIssues(buildIssues(null, VIN, null, null, null)).build();
	    	}
	    	if(acceptanceDate != null) {
	    		if(!isDateFormat(acceptanceDate)) {
	    			throw new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("Date format is invalid!")
	            	.withDetail("Date format is invalid!").withIssues(buildIssues(null, null, null, acceptanceDate, null)).build();
	    		}
	    	}
	    	if(languageID != null) {
	    		if(!isLanguageFormat(languageID)) {
	    			throw new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("LanguageID format is invalid!")
	            	.withDetail("LanguageID format is invalid!").withIssues(buildIssues(null, null, null, null, languageID)).build();
	    		}
	    	}
	    }
	 
	 private Boolean isDateFormat(String acceptanceDate) {
	    	
	    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    	dateFormat.setLenient(false);
	    	try {
	    		dateFormat.parse(acceptanceDate);
	    		return true;
	    	} catch (ParseException ex) {
	    		return false;
	    	}
	    }
	    
	 private Boolean isLanguageFormat(String languageID) {
	    	
	    	Pattern LANGUAGE_PATTERN = Pattern.compile("^\\S{2}-\\S{2}$");
	    	return LANGUAGE_PATTERN.matcher(languageID).matches();
	    }
}
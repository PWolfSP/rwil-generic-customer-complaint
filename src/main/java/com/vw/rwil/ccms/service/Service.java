package com.vw.rwil.ccms.service;

import org.springframework.http.ResponseEntity;

import com.vw.rwil.ccms.data.CustomerComplaints;

/**
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
public interface Service {

    public ResponseEntity<CustomerComplaints> serviceComplaint(String partnerkey, String VIN, String orderNumberID, String acceptanceDate,String languageID) throws Exception; 
    
}
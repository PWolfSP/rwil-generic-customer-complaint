package com.vw.rwil.ccms.service.exceptionhandler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.classmate.TypeResolver;
import com.vw.rwil.rwilutils.exception.handlers.ExceptionResponseHandler;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

import io.swagger.annotations.ApiModelProperty;
import lombok.Generated;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@Generated
public class CustomExceptionResponseHandler implements ExceptionResponseHandler<CustomRFC7807ErrorResponse> {

	@Override
	public CustomRFC7807ErrorResponse getErrorResponseObject(RFC7807ErrorResponse rfc7807ErrorResponse) {
		return new CustomRFC7807ErrorResponse(rfc7807ErrorResponse);
	}

	@Override
	public void configureSwaggerDocket(Docket docket, TypeResolver typeResolver) {

		// Disabling defaultResponseMessages
		docket.useDefaultResponseMessages(false);
		
		Set<Class<?>> modelClasses = new HashSet<>();
		List<ResponseMessage> defaultGetResponseMessages = new ArrayList<>();
		defaultGetResponseMessages
			.add(new ResponseMessageBuilder().code(ExceptionTypeWithBody.UNAUTHORIZED.statusCode()).message("Unauthorized").build());
		defaultGetResponseMessages
			.add(new ResponseMessageBuilder().code(HttpStatus.FORBIDDEN.value()).message("Forbidden").build());
		defaultGetResponseMessages
			.add(getResponseMessage(HttpStatus.NOT_FOUND, RFC7807ErrorResponse404.class, modelClasses));
		defaultGetResponseMessages
			.add(getResponseMessage(HttpStatus.BAD_REQUEST, RFC7807ErrorResponse400.class, modelClasses));
		defaultGetResponseMessages
			.add(getResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR, RFC7807ErrorResponse500.class, modelClasses));
		docket.globalResponseMessage(RequestMethod.GET, defaultGetResponseMessages);
		
		modelClasses.stream().forEach(modelClass -> docket.additionalModels(typeResolver.resolve(modelClass)));
	}

	private ResponseMessage getResponseMessage(HttpStatus status, Class<?> responseClass, Set<Class<?>> modelClasses) {
		modelClasses.add(responseClass);
		return new ResponseMessageBuilder().code(status.value()).message(status.toString())
				.responseModel(new ModelRef(responseClass.getSimpleName())).build();
	}
}

@Generated
class RFC7807ErrorResponse400 extends CustomRFC7807ErrorResponse {
	public RFC7807ErrorResponse400(RFC7807ErrorResponse rfc7807ErrorResponse) {
		super(rfc7807ErrorResponse);
	}

	@ApiModelProperty(notes = "The name of the Micro Service where the exception occuered", position = 1, example = "ms-service-customer-complaint-information")
	String instance;
	
	@ApiModelProperty(notes = "Explanation specific to this occurrence of the problem or issue", position = 2, example = "Partnerkey is invalid!")
	String detail;
	
	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 3, example = "BAD_REQUEST")
	String type;
	
	@ApiModelProperty(notes = " A short, summary of the problem type", position = 4, example = "Partnerkey is invalid!")
	String title;

	@ApiModelProperty(notes = "Additional information for to debug the issue - as an array", position = 5, example = "[{\r\n" + 
			"      \"param\": \"partnerkey\",\r\n" + 
			"      \"value\": \"DEU7462V\"\r\n" + 
			"    } ]")
	List<Issue> issues;
	
	@ApiModelProperty(notes = "The HTTP status code", position = 6, example = "400")
	int status;
}

@Generated
class RFC7807ErrorResponse404 extends CustomRFC7807ErrorResponse {
	public RFC7807ErrorResponse404(RFC7807ErrorResponse rfc7807ErrorResponse) {
		super(rfc7807ErrorResponse);
	}
	
	@ApiModelProperty(notes = "The name of the Micro Service where the exception occuered", position = 1, example = "ms-service-customer-complaint-information")
	String instance;
	
	@ApiModelProperty(notes = "Explanation specific to this occurrence of the problem or issue", position = 2, example = "No complaints found for given parameters!")
	String detail;
	
	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 3, example = "NOT_FOUND")
	String type;
	
	@ApiModelProperty(notes = "A short, summary of the problem type", position = 4, example = "No complaints found!")
	String title;

	@ApiModelProperty(notes = "Additional information for to debug the issue - as an array", position = 5, example = "[{\r\n" + 
			"      \"param\": \"partnerkey\",\r\n" + 
			"      \"value\": \"DEU74623V\"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"      \"param\": \"vin\",\r\n" + 
			"      \"value\": \"wvwzzz9nz5d010313\"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"      \"param\": \"orderNumberID\",\r\n" + 
			"      \"value\": \"23514375\"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"      \"param\": \"acceptanceDate\",\r\n" + 
			"      \"value\": \"2020-08-19\"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"      \"param\": \"languageID\",\r\n" + 
			"      \"value\": \"de-DE\"\r\n" + 
			"    } ]")
	List<Issue> issues;
	
	@ApiModelProperty(notes = "The HTTP status code", position = 5, example = "404")
	int status;
}

@Generated
class RFC7807ErrorResponse500 extends CustomRFC7807ErrorResponse {
	public RFC7807ErrorResponse500(RFC7807ErrorResponse rfc7807ErrorResponse) {
		super(rfc7807ErrorResponse);
	}
	
	@ApiModelProperty(notes = "The name of the Micro Service where the exception occuered", position = 1, example = "ms-service-customer-complaint-information")
	String instance;

	@ApiModelProperty(notes = "Additional information for to debug the issue - as an array", position = 2, example = "[{\r\n" + 
			"      \"param\": \"value\",\r\n" + 
			"      \"value\": \"null\"\r\n" + 
			"    }]")
	List<Issue> issues;

}
package com.vw.rwil.ccms.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.GetWorkshopComplaintsType;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.ShowWorkshopComplaintsType;
import com.vw.rwil.ccms.data.CustomerComplaints;
import com.vw.rwil.ccms.data.RequestParameter;
import com.vw.rwil.ccms.factory.ExchangeFactory;
import com.vw.rwil.ccms.mapping.ObjectMapping;
import com.vw.rwil.ccms.service.exceptionhandler.CustomExceptionResponseHandler;
import com.vw.rwil.ccms.service.utils.Utils;
import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.RWILExceptionBuilder;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

import lombok.extern.slf4j.Slf4j;

/**
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
@Slf4j
@org.springframework.stereotype.Service
public class ServiceImpl implements Service {

	@Autowired
	CustomExceptionResponseHandler customExceptionResponseHandler;
	
    @Autowired
    RestTemplate restTemplate;
    
    @Autowired
    ObjectMapping objectMapping;
    
    @Autowired
    Utils utils;
    
    @Value("${endpoint.connector}")
    String controllerEndpoint;

    @Override
    public ResponseEntity<CustomerComplaints> serviceComplaint(String partnerkey, String VIN, String orderNumberID, String acceptanceDate, String languageID)
        throws Exception {

    	utils.validateRequest(partnerkey, VIN, orderNumberID, acceptanceDate, languageID);
    	
        log.info("*****    Connector URL: " + controllerEndpoint);

        String bodyJSON = new Gson().toJson(objectMapping.buildWorkshopComplaintsRequest(partnerkey, VIN, orderNumberID, acceptanceDate, languageID), GetWorkshopComplaintsType.class);
        
        String requestURL = controllerEndpoint.endsWith("/") ? controllerEndpoint + partnerkey + "/getWorkshopComplaints"
                : controllerEndpoint + "/" + partnerkey + "/getWorkshopComplaints";

        log.info("Request URL: " + requestURL);

        org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

		RequestParameter requestParam = new RequestParameter();
		requestParam.acceptanceDate = acceptanceDate;
		requestParam.languageID = languageID;
		requestParam.orderNumberID = orderNumberID;
		requestParam.partnerkey = partnerkey;
		requestParam.vin = VIN;
		requestParam.urlPart = "complaint";

		ExchangeFactory exchangeFactory = new ExchangeFactory();
		exchangeFactory.handleRequest(requestParam);
		exchangeFactory.performSpecificRequestOperations();
		exchangeFactory.transformToJsonIaRequest(requestParam);

        HttpEntity<String> entity = new HttpEntity<String>(bodyJSON, headers);
        ResponseEntity<Object> connectorResponse = restTemplate.exchange(requestURL, HttpMethod.POST, entity, Object.class);

        if (connectorResponse.getStatusCodeValue() != 201 && !(connectorResponse.getBody() instanceof RWILException)) {
            throw new RWILExceptionBuilder((RFC7807ErrorResponse) connectorResponse.getBody()).build();
        } else if (connectorResponse.getStatusCodeValue() != 201 && (connectorResponse.getBody() instanceof RWILException)) {
        	
        	RWILException exception = (RWILException) connectorResponse.getBody();
        	if (exception.getTitle().contains("Client received SOAP Fault from server")) {
        		throw new RWILExceptionBuilder(ExceptionTypeWithBody.NOT_FOUND).withTitle("No complaint found!")
            	.withDetail("No complaint found for given parameters!").withIssues(utils.buildIssues(partnerkey, VIN, orderNumberID, acceptanceDate, languageID)).build();
        	}
            throw (RWILException) connectorResponse.getBody();
        }

		// for each array entry
		exchangeFactory.orchestrate();
		exchangeFactory.handleResponse();
		exchangeFactory.performSpecificResponseOperations();

        String jsonResponse = new Gson().toJson(connectorResponse.getBody(), Map.class);
        ShowWorkshopComplaintsType objectResponse = new Gson().fromJson(jsonResponse, ShowWorkshopComplaintsType.class);
        
        ResponseEntity<CustomerComplaints> result = new ResponseEntity<CustomerComplaints>(
        		objectMapping.mapCustomerComplaintsResponse(objectResponse), HttpStatus.OK);
        return result;
    }
}
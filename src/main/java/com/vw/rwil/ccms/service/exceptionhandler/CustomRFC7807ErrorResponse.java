package com.vw.rwil.ccms.service.exceptionhandler;

import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Generated;

@Data
@EqualsAndHashCode(callSuper = true)
@Generated
public class CustomRFC7807ErrorResponse extends RFC7807ErrorResponse {


    public CustomRFC7807ErrorResponse(RFC7807ErrorResponse rfc7807ErrorResponse) {
        
        this.setDetail(rfc7807ErrorResponse.getDetail());
        this.setIssues(rfc7807ErrorResponse.getIssues());
        this.setStatus(rfc7807ErrorResponse.getStatus());
        this.setTitle(rfc7807ErrorResponse.getTitle());
        this.setType(rfc7807ErrorResponse.getType());
        this.setInstance(rfc7807ErrorResponse.getInstance());
    }

}
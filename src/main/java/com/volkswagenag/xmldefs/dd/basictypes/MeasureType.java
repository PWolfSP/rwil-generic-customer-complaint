
package com.volkswagenag.xmldefs.dd.basictypes;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.Generated;


/**
 * 
 *             A numeric value determined by measuring an object along with the specified unit of
 *           measure. Values allowed in the 'unit' attribute are determined by one of the code lists:
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00124&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Volume
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00125&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Weight
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00126&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Length
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00127&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Liquid volume
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00129&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Odometer
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00179&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Consumption
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00180&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Emission
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00181&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Power
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00183&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Time
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;CL00184&lt;/ann:CodeList&gt;
 * </pre>
 * 
 *             Packaging unit
 *           
 * 
 * <p>Java-Klasse für MeasureType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MeasureType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>decimal">
 *       &lt;attribute name="unit" use="required" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeasureType", propOrder = {
    "value"
})
public class MeasureType {

    @XmlValue
    protected BigDecimal value;
    @XmlAttribute(name = "unit", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String unit;

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * Ruft den Wert der unit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Legt den Wert der unit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnit(String value) {
        this.unit = value;
    }

}

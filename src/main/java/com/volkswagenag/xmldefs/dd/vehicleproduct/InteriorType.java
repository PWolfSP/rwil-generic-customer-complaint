
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für InteriorType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InteriorType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InteriorOption" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}InteriorOptionType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InteriorType", propOrder = {
    "interiorOption"
})



public class InteriorType {
	
	
    @XmlElement(name = "InteriorOption", required = true)
    private List<InteriorOptionType> interiorOption;


		public void setInteriorOptionType(List<InteriorOptionType> interiorOption) {
			this.interiorOption = interiorOption;
		}
		
	    public List<InteriorOptionType> getInteriorOption() {
	        if (interiorOption == null) {
	            interiorOption = new ArrayList<InteriorOptionType>();
	        }
	        return this.interiorOption;
	    }

}

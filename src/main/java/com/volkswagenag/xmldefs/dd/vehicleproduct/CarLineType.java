
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für CarLineType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CarLineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CarLine" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="CarLineDescriptions" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}DescriptionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarLineType", propOrder = {
    "carLine",
    "carLineDescriptions"
})
public class CarLineType {

    @XmlElement(name = "CarLine")
    protected CodeType carLine;
    @XmlElement(name = "CarLineDescriptions")
    protected DescriptionsType carLineDescriptions;

    /**
     * Ruft den Wert der carLine-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getCarLine() {
        return carLine;
    }

    /**
     * Legt den Wert der carLine-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setCarLine(CodeType value) {
        this.carLine = value;
    }

    /**
     * Ruft den Wert der carLineDescriptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DescriptionsType }
     *     
     */
    public DescriptionsType getCarLineDescriptions() {
        return carLineDescriptions;
    }

    /**
     * Legt den Wert der carLineDescriptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DescriptionsType }
     *     
     */
    public void setCarLineDescriptions(DescriptionsType value) {
        this.carLineDescriptions = value;
    }

}


package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EquipmentOptionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EquipmentOptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OptionCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="OptionType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="EquipmentClass" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="OptionGroup" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ISetCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="XSetCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="Prices" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ProductPricesType" minOccurs="0"/>
 *         &lt;element name="Descriptions" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}EquipmentDescriptionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentOptionType", propOrder = {
    "optionCode",
    "optionType",
    "equipmentClass",
    "optionGroup",
    "iSetCode",
    "xSetCode",
    "prices",
    "descriptions"
})
public class EquipmentOptionType {

    @XmlElement(name = "OptionCode")
    protected CodeType optionCode;
    @XmlElement(name = "OptionType")
    protected CodeType optionType;
    @XmlElement(name = "EquipmentClass")
    protected CodeType equipmentClass;
    @XmlElement(name = "OptionGroup")
    protected CodeType optionGroup;
    @XmlElement(name = "ISetCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String iSetCode;
    @XmlElement(name = "XSetCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String xSetCode;
    @XmlElement(name = "Prices")
    protected ProductPricesType prices;
    @XmlElement(name = "Descriptions")
    protected EquipmentDescriptionsType descriptions;

    /**
     * Ruft den Wert der optionCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOptionCode() {
        return optionCode;
    }

    /**
     * Legt den Wert der optionCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOptionCode(CodeType value) {
        this.optionCode = value;
    }

    /**
     * Ruft den Wert der optionType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOptionType() {
        return optionType;
    }

    /**
     * Legt den Wert der optionType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOptionType(CodeType value) {
        this.optionType = value;
    }

    /**
     * Ruft den Wert der equipmentClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getEquipmentClass() {
        return equipmentClass;
    }

    /**
     * Legt den Wert der equipmentClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setEquipmentClass(CodeType value) {
        this.equipmentClass = value;
    }

    /**
     * Ruft den Wert der optionGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOptionGroup() {
        return optionGroup;
    }

    /**
     * Legt den Wert der optionGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOptionGroup(CodeType value) {
        this.optionGroup = value;
    }

    /**
     * Ruft den Wert der iSetCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISetCode() {
        return iSetCode;
    }

    /**
     * Legt den Wert der iSetCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISetCode(String value) {
        this.iSetCode = value;
    }

    /**
     * Ruft den Wert der xSetCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXSetCode() {
        return xSetCode;
    }

    /**
     * Legt den Wert der xSetCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXSetCode(String value) {
        this.xSetCode = value;
    }

    /**
     * Ruft den Wert der prices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProductPricesType }
     *     
     */
    public ProductPricesType getPrices() {
        return prices;
    }

    /**
     * Legt den Wert der prices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductPricesType }
     *     
     */
    public void setPrices(ProductPricesType value) {
        this.prices = value;
    }

    /**
     * Ruft den Wert der descriptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentDescriptionsType }
     *     
     */
    public EquipmentDescriptionsType getDescriptions() {
        return descriptions;
    }

    /**
     * Legt den Wert der descriptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentDescriptionsType }
     *     
     */
    public void setDescriptions(EquipmentDescriptionsType value) {
        this.descriptions = value;
    }

}


package com.volkswagenag.xmldefs.dd.vehicleproduct;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse für VehicleProductType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VehicleProductType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VehicleProductIdentifier" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}VehicleProductIdentifierType" minOccurs="0"/>
 *         &lt;element name="ModelDescription" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ProductDescriptionType" minOccurs="0"/>
 *         &lt;element name="VehicleClassDescription" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="CarLine" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}CarLineType" minOccurs="0"/>
 *         &lt;element name="ModelGroup" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ModelGroupType" minOccurs="0"/>
 *         &lt;element name="SalesGroup" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}SalesGroupType" minOccurs="0"/>
 *         &lt;element name="Prices" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ProductPricesType" minOccurs="0"/>
 *         &lt;element name="ADI" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ADIType" minOccurs="0"/>
 *         &lt;element name="TechnicalSpecifications" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}TechnicalSpecificationsType" minOccurs="0"/>
 *         &lt;element name="PriceCatalog" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}PriceCatalogType" minOccurs="0"/>
 *         &lt;element name="Mix" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="Equipments" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}EquipmentsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleProductType", propOrder = {
    "vehicleProductIdentifier",
    "modelDescription",
    "vehicleClassDescription",
    "carLine",
    "modelGroup",
    "salesGroup",
    "prices",
    "adi",
    "technicalSpecifications",
    "priceCatalog",
    "mix",
    "equipments"
})
public class VehicleProductType {

    @XmlElement(name = "VehicleProductIdentifier")
    protected VehicleProductIdentifierType vehicleProductIdentifier;
    @XmlElement(name = "ModelDescription")
    protected ProductDescriptionType modelDescription;
    @XmlElement(name = "VehicleClassDescription")
    protected TextType vehicleClassDescription;
    @XmlElement(name = "CarLine")
    protected CarLineType carLine;
    @XmlElement(name = "ModelGroup")
    protected ModelGroupType modelGroup;
    @XmlElement(name = "SalesGroup")
    protected SalesGroupType salesGroup;
    @XmlElement(name = "Prices")
    protected ProductPricesType prices;
    @XmlElement(name = "ADI")
    protected ADIType adi;
    @XmlElement(name = "TechnicalSpecifications")
    protected TechnicalSpecificationsType technicalSpecifications;
    @XmlElement(name = "PriceCatalog")
    protected PriceCatalogType priceCatalog;
    @XmlElement(name = "Mix")
    protected BigDecimal mix;
    @XmlElement(name = "Equipments")
    protected EquipmentsType equipments;

    /**
     * Ruft den Wert der vehicleProductIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehicleProductIdentifierType }
     *     
     */
    public VehicleProductIdentifierType getVehicleProductIdentifier() {
        return vehicleProductIdentifier;
    }

    /**
     * Legt den Wert der vehicleProductIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleProductIdentifierType }
     *     
     */
    public void setVehicleProductIdentifier(VehicleProductIdentifierType value) {
        this.vehicleProductIdentifier = value;
    }

    /**
     * Ruft den Wert der modelDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProductDescriptionType }
     *     
     */
    public ProductDescriptionType getModelDescription() {
        return modelDescription;
    }

    /**
     * Legt den Wert der modelDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductDescriptionType }
     *     
     */
    public void setModelDescription(ProductDescriptionType value) {
        this.modelDescription = value;
    }

    /**
     * Ruft den Wert der vehicleClassDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getVehicleClassDescription() {
        return vehicleClassDescription;
    }

    /**
     * Legt den Wert der vehicleClassDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setVehicleClassDescription(TextType value) {
        this.vehicleClassDescription = value;
    }

    /**
     * Ruft den Wert der carLine-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CarLineType }
     *     
     */
    public CarLineType getCarLine() {
        return carLine;
    }

    /**
     * Legt den Wert der carLine-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CarLineType }
     *     
     */
    public void setCarLine(CarLineType value) {
        this.carLine = value;
    }

    /**
     * Ruft den Wert der modelGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ModelGroupType }
     *     
     */
    public ModelGroupType getModelGroup() {
        return modelGroup;
    }

    /**
     * Legt den Wert der modelGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelGroupType }
     *     
     */
    public void setModelGroup(ModelGroupType value) {
        this.modelGroup = value;
    }

    /**
     * Ruft den Wert der salesGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SalesGroupType }
     *     
     */
    public SalesGroupType getSalesGroup() {
        return salesGroup;
    }

    /**
     * Legt den Wert der salesGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesGroupType }
     *     
     */
    public void setSalesGroup(SalesGroupType value) {
        this.salesGroup = value;
    }

    /**
     * Ruft den Wert der prices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProductPricesType }
     *     
     */
    public ProductPricesType getPrices() {
        return prices;
    }

    /**
     * Legt den Wert der prices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductPricesType }
     *     
     */
    public void setPrices(ProductPricesType value) {
        this.prices = value;
    }

    /**
     * Ruft den Wert der adi-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADIType }
     *     
     */
    public ADIType getADI() {
        return adi;
    }

    /**
     * Legt den Wert der adi-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADIType }
     *     
     */
    public void setADI(ADIType value) {
        this.adi = value;
    }

    /**
     * Ruft den Wert der technicalSpecifications-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalSpecificationsType }
     *     
     */
    public TechnicalSpecificationsType getTechnicalSpecifications() {
        return technicalSpecifications;
    }

    /**
     * Legt den Wert der technicalSpecifications-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalSpecificationsType }
     *     
     */
    public void setTechnicalSpecifications(TechnicalSpecificationsType value) {
        this.technicalSpecifications = value;
    }

    /**
     * Ruft den Wert der priceCatalog-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PriceCatalogType }
     *     
     */
    public PriceCatalogType getPriceCatalog() {
        return priceCatalog;
    }

    /**
     * Legt den Wert der priceCatalog-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceCatalogType }
     *     
     */
    public void setPriceCatalog(PriceCatalogType value) {
        this.priceCatalog = value;
    }

    /**
     * Ruft den Wert der mix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMix() {
        return mix;
    }

    /**
     * Legt den Wert der mix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMix(BigDecimal value) {
        this.mix = value;
    }

    /**
     * Ruft den Wert der equipments-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentsType }
     *     
     */
    public EquipmentsType getEquipments() {
        return equipments;
    }

    /**
     * Legt den Wert der equipments-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentsType }
     *     
     */
    public void setEquipments(EquipmentsType value) {
        this.equipments = value;
    }

}

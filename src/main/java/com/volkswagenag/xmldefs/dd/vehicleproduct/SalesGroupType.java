
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für SalesGroupType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SalesGroupType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SalesGroup" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="SalesGroupDescriptions" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}DescriptionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesGroupType", propOrder = {
    "salesGroup",
    "salesGroupDescriptions"
})
public class SalesGroupType {

    @XmlElement(name = "SalesGroup")
    protected CodeType salesGroup;
    @XmlElement(name = "SalesGroupDescriptions")
    protected DescriptionsType salesGroupDescriptions;

    /**
     * Ruft den Wert der salesGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getSalesGroup() {
        return salesGroup;
    }

    /**
     * Legt den Wert der salesGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setSalesGroup(CodeType value) {
        this.salesGroup = value;
    }

    /**
     * Ruft den Wert der salesGroupDescriptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DescriptionsType }
     *     
     */
    public DescriptionsType getSalesGroupDescriptions() {
        return salesGroupDescriptions;
    }

    /**
     * Legt den Wert der salesGroupDescriptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DescriptionsType }
     *     
     */
    public void setSalesGroupDescriptions(DescriptionsType value) {
        this.salesGroupDescriptions = value;
    }

}

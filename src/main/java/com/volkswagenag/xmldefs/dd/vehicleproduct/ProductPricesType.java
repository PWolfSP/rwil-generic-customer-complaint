
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ProductPricesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProductPricesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Price" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ProductPriceType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductPricesType", propOrder = {
    "price"
})



public class ProductPricesType {
	
	
    @XmlElement(name = "Price", required = true)
    private List<ProductPriceType> price;


		public void setProductPriceType(List<ProductPriceType> price) {
			this.price = price;
		}
		
	    public List<ProductPriceType> getPrice() {
	        if (price == null) {
	            price = new ArrayList<ProductPriceType>();
	        }
	        return this.price;
	    }

}

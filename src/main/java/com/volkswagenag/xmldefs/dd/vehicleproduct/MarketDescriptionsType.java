
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für MarketDescriptionsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MarketDescriptionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MarketDescription" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}MarketDescriptionType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketDescriptionsType", propOrder = {
    "marketDescription"
})



public class MarketDescriptionsType {
	
	
    @XmlElement(name = "MarketDescription", required = true)
    private List<MarketDescriptionType> marketDescription;


		public void setMarketDescriptionType(List<MarketDescriptionType> marketDescription) {
			this.marketDescription = marketDescription;
		}
		
	    public List<MarketDescriptionType> getMarketDescription() {
	        if (marketDescription == null) {
	            marketDescription = new ArrayList<MarketDescriptionType>();
	        }
	        return this.marketDescription;
	    }

}

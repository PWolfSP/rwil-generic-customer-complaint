
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * Identifier of a vehicle order
 * 
 * <p>Java-Klasse f�r VehicleProductIdentifierType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VehicleProductIdentifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductUID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="ApplicationIdentifiers" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ApplicationIdentifiersType" minOccurs="0"/>
 *         &lt;element name="Brand" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="SalesProgram" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ModelCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ModelExtension" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ModelVersion" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="ModelYear" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}YearType" minOccurs="0"/>
 *         &lt;element name="VehicleClass" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="WholesalerCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="ImporterId" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleProductIdentifierType", propOrder = {
    "productUID",
    "applicationIdentifiers",
    "brand",
    "salesProgram",
    "modelCode",
    "modelExtension",
    "modelVersion",
    "modelYear",
    "vehicleClass",
    "wholesalerCode",
    "importerId"
})
public class VehicleProductIdentifierType {

    @XmlElement(name = "ProductUID")
    protected IdentifierType productUID;
    @XmlElement(name = "ApplicationIdentifiers")
    protected ApplicationIdentifiersType applicationIdentifiers;
    @XmlElement(name = "Brand")
    protected CodeType brand;
    @XmlElement(name = "SalesProgram")
    protected CodeType salesProgram;
    @XmlElement(name = "ModelCode")
    protected CodeType modelCode;
    @XmlElement(name = "ModelExtension")
    protected CodeType modelExtension;
    @XmlElement(name = "ModelVersion")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String modelVersion;
    @XmlElement(name = "ModelYear")
    protected String modelYear;
    @XmlElement(name = "VehicleClass")
    protected CodeType vehicleClass;
    @XmlElement(name = "WholesalerCode")
    protected IdentifierType wholesalerCode;
    @XmlElement(name = "ImporterId")
    protected IdentifierType importerId;

    /**
     * Ruft den Wert der productUID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getProductUID() {
        return productUID;
    }

    /**
     * Legt den Wert der productUID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setProductUID(IdentifierType value) {
        this.productUID = value;
    }

    /**
     * Ruft den Wert der applicationIdentifiers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public ApplicationIdentifiersType getApplicationIdentifiers() {
        return applicationIdentifiers;
    }

    /**
     * Legt den Wert der applicationIdentifiers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public void setApplicationIdentifiers(ApplicationIdentifiersType value) {
        this.applicationIdentifiers = value;
    }

    /**
     * Ruft den Wert der brand-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBrand() {
        return brand;
    }

    /**
     * Legt den Wert der brand-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBrand(CodeType value) {
        this.brand = value;
    }

    /**
     * Ruft den Wert der salesProgram-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getSalesProgram() {
        return salesProgram;
    }

    /**
     * Legt den Wert der salesProgram-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setSalesProgram(CodeType value) {
        this.salesProgram = value;
    }

    /**
     * Ruft den Wert der modelCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getModelCode() {
        return modelCode;
    }

    /**
     * Legt den Wert der modelCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setModelCode(CodeType value) {
        this.modelCode = value;
    }

    /**
     * Ruft den Wert der modelExtension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getModelExtension() {
        return modelExtension;
    }

    /**
     * Legt den Wert der modelExtension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setModelExtension(CodeType value) {
        this.modelExtension = value;
    }

    /**
     * Ruft den Wert der modelVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelVersion() {
        return modelVersion;
    }

    /**
     * Legt den Wert der modelVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelVersion(String value) {
        this.modelVersion = value;
    }

    /**
     * Ruft den Wert der modelYear-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelYear() {
        return modelYear;
    }

    /**
     * Legt den Wert der modelYear-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelYear(String value) {
        this.modelYear = value;
    }

    /**
     * Ruft den Wert der vehicleClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getVehicleClass() {
        return vehicleClass;
    }

    /**
     * Legt den Wert der vehicleClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setVehicleClass(CodeType value) {
        this.vehicleClass = value;
    }

    /**
     * Ruft den Wert der wholesalerCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getWholesalerCode() {
        return wholesalerCode;
    }

    /**
     * Legt den Wert der wholesalerCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setWholesalerCode(IdentifierType value) {
        this.wholesalerCode = value;
    }

    /**
     * Ruft den Wert der importerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getImporterId() {
        return importerId;
    }

    /**
     * Legt den Wert der importerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setImporterId(IdentifierType value) {
        this.importerId = value;
    }

}

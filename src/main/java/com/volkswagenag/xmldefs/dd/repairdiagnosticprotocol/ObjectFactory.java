
package com.volkswagenag.xmldefs.dd.repairdiagnosticprotocol;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.repairdiagnosticprotocol package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _RepairDiagnosticProtocol_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/RepairDiagnosticProtocol", "RepairDiagnosticProtocol");
    private final static QName _RepairDiagnosticProtocolRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/RepairDiagnosticProtocol", "RepairDiagnosticProtocolRef");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.repairdiagnosticprotocol
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RepairDiagnosticProtocolIdentifierType }
     * 
     */
    public RepairDiagnosticProtocolIdentifierType createRepairDiagnosticProtocolIdentifierType() {
        return new RepairDiagnosticProtocolIdentifierType();
    }

    /**
     * Create an instance of {@link RepairDiagnosticProtocolType }
     * 
     */
    public RepairDiagnosticProtocolType createRepairDiagnosticProtocolType() {
        return new RepairDiagnosticProtocolType();
    }

    /**
     * Create an instance of {@link ApplicationIdentifiersType }
     * 
     */
    public ApplicationIdentifiersType createApplicationIdentifiersType() {
        return new ApplicationIdentifiersType();
    }

    /**
     * Create an instance of {@link DiagnosticDeviceType }
     * 
     */
    public DiagnosticDeviceType createDiagnosticDeviceType() {
        return new DiagnosticDeviceType();
    }

    /**
     * Create an instance of {@link ProtocolTimestampsType }
     * 
     */
    public ProtocolTimestampsType createProtocolTimestampsType() {
        return new ProtocolTimestampsType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepairDiagnosticProtocolType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/RepairDiagnosticProtocol", name = "RepairDiagnosticProtocol")
    public JAXBElement<RepairDiagnosticProtocolType> createRepairDiagnosticProtocol(RepairDiagnosticProtocolType value) {
        return new JAXBElement<RepairDiagnosticProtocolType>(_RepairDiagnosticProtocol_QNAME, RepairDiagnosticProtocolType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepairDiagnosticProtocolIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/RepairDiagnosticProtocol", name = "RepairDiagnosticProtocolRef")
    public JAXBElement<RepairDiagnosticProtocolIdentifierType> createRepairDiagnosticProtocolRef(RepairDiagnosticProtocolIdentifierType value) {
        return new JAXBElement<RepairDiagnosticProtocolIdentifierType>(_RepairDiagnosticProtocolRef_QNAME, RepairDiagnosticProtocolIdentifierType.class, null, value);
    }

}


package com.volkswagenag.xmldefs.dd.repairdiagnosticprotocol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.BinaryObjectType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.MeasureType;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerIdentifierType;
import com.volkswagenag.xmldefs.dd.vehicle.VehicleIdentifierType;
import com.volkswagenag.xmldefs.dd.workshoporder.WorkshopOrderIdentifierType;

import lombok.Generated;


/**
 * In After sales systems repair diagnostic protocols are needed for different
 *           purposes. Primarily they are used to specify and locate faults or malfunctions of vehicles in the context of workshop
 *           complaints. But also accounting and payment systems are interested in information coming from repair diagnostic
 *           protocols e.g. to determine the working hours spent to create a repair diagnostic protocol. A repair diagnostic
 *           protocol is created by a VAS-Tester or the newer ODIS system. In both cases a tester device is connected to the
 *           vehicle and a protocol is created and afterwards transmitted to VAG via DMS-Backbone.
 * 
 * <p>Java-Klasse für RepairDiagnosticProtocolType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RepairDiagnosticProtocolType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RepairDiagnosticProtocolIdentifier" type="{http://xmldefs.volkswagenag.com/DD/RepairDiagnosticProtocol}RepairDiagnosticProtocolIdentifierType" minOccurs="0"/>
 *         &lt;element name="ProtocolStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ProtocolBinaryData" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}BinaryObjectType" minOccurs="0"/>
 *         &lt;element name="Size" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="ProtocolDiagnosticDevice" type="{http://xmldefs.volkswagenag.com/DD/RepairDiagnosticProtocol}DiagnosticDeviceType" minOccurs="0"/>
 *         &lt;element name="DiagnosticSession" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="ProtocolTimestamps" type="{http://xmldefs.volkswagenag.com/DD/RepairDiagnosticProtocol}ProtocolTimestampsType" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/Vehicle}VehicleRef" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DealerRef" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}WorkshopOrderRef" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepairDiagnosticProtocolType", propOrder = {
    "repairDiagnosticProtocolIdentifier",
    "protocolStatus",
    "protocolBinaryData",
    "size",
    "protocolDiagnosticDevice",
    "diagnosticSession",
    "protocolTimestamps",
    "vehicleRef",
    "dealerRef",
    "workshopOrderRef"
})
public class RepairDiagnosticProtocolType {

    @XmlElement(name = "RepairDiagnosticProtocolIdentifier")
    protected RepairDiagnosticProtocolIdentifierType repairDiagnosticProtocolIdentifier;
    @XmlElement(name = "ProtocolStatus")
    protected CodeType protocolStatus;
    @XmlElement(name = "ProtocolBinaryData")
    protected BinaryObjectType protocolBinaryData;
    @XmlElement(name = "Size")
    protected MeasureType size;
    @XmlElement(name = "ProtocolDiagnosticDevice")
    protected DiagnosticDeviceType protocolDiagnosticDevice;
    @XmlElement(name = "DiagnosticSession")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String diagnosticSession;
    @XmlElement(name = "ProtocolTimestamps")
    protected ProtocolTimestampsType protocolTimestamps;
    @XmlElement(name = "VehicleRef", namespace = "http://xmldefs.volkswagenag.com/DD/Vehicle")
    protected VehicleIdentifierType vehicleRef;
    @XmlElement(name = "DealerRef", namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner")
    protected PartnerIdentifierType dealerRef;
    @XmlElement(name = "WorkshopOrderRef", namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopOrder")
    protected WorkshopOrderIdentifierType workshopOrderRef;

    /**
     * Ruft den Wert der repairDiagnosticProtocolIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RepairDiagnosticProtocolIdentifierType }
     *     
     */
    public RepairDiagnosticProtocolIdentifierType getRepairDiagnosticProtocolIdentifier() {
        return repairDiagnosticProtocolIdentifier;
    }

    /**
     * Legt den Wert der repairDiagnosticProtocolIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RepairDiagnosticProtocolIdentifierType }
     *     
     */
    public void setRepairDiagnosticProtocolIdentifier(RepairDiagnosticProtocolIdentifierType value) {
        this.repairDiagnosticProtocolIdentifier = value;
    }

    /**
     * Ruft den Wert der protocolStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getProtocolStatus() {
        return protocolStatus;
    }

    /**
     * Legt den Wert der protocolStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setProtocolStatus(CodeType value) {
        this.protocolStatus = value;
    }

    /**
     * Ruft den Wert der protocolBinaryData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BinaryObjectType }
     *     
     */
    public BinaryObjectType getProtocolBinaryData() {
        return protocolBinaryData;
    }

    /**
     * Legt den Wert der protocolBinaryData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BinaryObjectType }
     *     
     */
    public void setProtocolBinaryData(BinaryObjectType value) {
        this.protocolBinaryData = value;
    }

    /**
     * Ruft den Wert der size-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getSize() {
        return size;
    }

    /**
     * Legt den Wert der size-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setSize(MeasureType value) {
        this.size = value;
    }

    /**
     * Ruft den Wert der protocolDiagnosticDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DiagnosticDeviceType }
     *     
     */
    public DiagnosticDeviceType getProtocolDiagnosticDevice() {
        return protocolDiagnosticDevice;
    }

    /**
     * Legt den Wert der protocolDiagnosticDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DiagnosticDeviceType }
     *     
     */
    public void setProtocolDiagnosticDevice(DiagnosticDeviceType value) {
        this.protocolDiagnosticDevice = value;
    }

    /**
     * Ruft den Wert der diagnosticSession-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiagnosticSession() {
        return diagnosticSession;
    }

    /**
     * Legt den Wert der diagnosticSession-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiagnosticSession(String value) {
        this.diagnosticSession = value;
    }

    /**
     * Ruft den Wert der protocolTimestamps-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolTimestampsType }
     *     
     */
    public ProtocolTimestampsType getProtocolTimestamps() {
        return protocolTimestamps;
    }

    /**
     * Legt den Wert der protocolTimestamps-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolTimestampsType }
     *     
     */
    public void setProtocolTimestamps(ProtocolTimestampsType value) {
        this.protocolTimestamps = value;
    }

    /**
     * Ruft den Wert der vehicleRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehicleIdentifierType }
     *     
     */
    public VehicleIdentifierType getVehicleRef() {
        return vehicleRef;
    }

    /**
     * Legt den Wert der vehicleRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleIdentifierType }
     *     
     */
    public void setVehicleRef(VehicleIdentifierType value) {
        this.vehicleRef = value;
    }

    /**
     * Ruft den Wert der dealerRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getDealerRef() {
        return dealerRef;
    }

    /**
     * Legt den Wert der dealerRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setDealerRef(PartnerIdentifierType value) {
        this.dealerRef = value;
    }

    /**
     * Ruft den Wert der workshopOrderRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopOrderIdentifierType }
     *     
     */
    public WorkshopOrderIdentifierType getWorkshopOrderRef() {
        return workshopOrderRef;
    }

    /**
     * Legt den Wert der workshopOrderRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopOrderIdentifierType }
     *     
     */
    public void setWorkshopOrderRef(WorkshopOrderIdentifierType value) {
        this.workshopOrderRef = value;
    }

}

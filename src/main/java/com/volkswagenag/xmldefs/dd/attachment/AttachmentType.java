
package com.volkswagenag.xmldefs.dd.attachment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.BinaryObjectType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.MeasureType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AttachmentType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttachmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttachmentIdentifier" type="{http://xmldefs.volkswagenag.com/DD/Attachment}AttachmentIdentifierType" minOccurs="0"/>
 *         &lt;element name="AttachmentBinaryData" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}BinaryObjectType" minOccurs="0"/>
 *         &lt;element name="Size" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachmentType", propOrder = {
    "attachmentIdentifier",
    "attachmentBinaryData",
    "size",
    "language"
})
public class AttachmentType {

    @XmlElement(name = "AttachmentIdentifier")
    protected AttachmentIdentifierType attachmentIdentifier;
    @XmlElement(name = "AttachmentBinaryData")
    protected BinaryObjectType attachmentBinaryData;
    @XmlElement(name = "Size")
    protected MeasureType size;
    @XmlElement(name = "Language")
    protected CodeType language;

    /**
     * Ruft den Wert der attachmentIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentIdentifierType }
     *     
     */
    public AttachmentIdentifierType getAttachmentIdentifier() {
        return attachmentIdentifier;
    }

    /**
     * Legt den Wert der attachmentIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentIdentifierType }
     *     
     */
    public void setAttachmentIdentifier(AttachmentIdentifierType value) {
        this.attachmentIdentifier = value;
    }

    /**
     * Ruft den Wert der attachmentBinaryData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BinaryObjectType }
     *     
     */
    public BinaryObjectType getAttachmentBinaryData() {
        return attachmentBinaryData;
    }

    /**
     * Legt den Wert der attachmentBinaryData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BinaryObjectType }
     *     
     */
    public void setAttachmentBinaryData(BinaryObjectType value) {
        this.attachmentBinaryData = value;
    }

    /**
     * Ruft den Wert der size-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getSize() {
        return size;
    }

    /**
     * Legt den Wert der size-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setSize(MeasureType value) {
        this.size = value;
    }

    /**
     * Ruft den Wert der language-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getLanguage() {
        return language;
    }

    /**
     * Legt den Wert der language-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setLanguage(CodeType value) {
        this.language = value;
    }

}


package com.volkswagenag.xmldefs.dd.attachment;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.attachment package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _Attachment_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Attachment", "Attachment");
    private final static QName _AttachmentRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Attachment", "AttachmentRef");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.attachment
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AttachmentType }
     * 
     */
    public AttachmentType createAttachmentType() {
        return new AttachmentType();
    }

    /**
     * Create an instance of {@link AttachmentIdentifierType }
     * 
     */
    public AttachmentIdentifierType createAttachmentIdentifierType() {
        return new AttachmentIdentifierType();
    }

    /**
     * Create an instance of {@link ApplicationIdentifiersType }
     * 
     */
    public ApplicationIdentifiersType createApplicationIdentifiersType() {
        return new ApplicationIdentifiersType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachmentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Attachment", name = "Attachment")
    public JAXBElement<AttachmentType> createAttachment(AttachmentType value) {
        return new JAXBElement<AttachmentType>(_Attachment_QNAME, AttachmentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachmentIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Attachment", name = "AttachmentRef")
    public JAXBElement<AttachmentIdentifierType> createAttachmentRef(AttachmentIdentifierType value) {
        return new JAXBElement<AttachmentIdentifierType>(_AttachmentRef_QNAME, AttachmentIdentifierType.class, null, value);
    }

}

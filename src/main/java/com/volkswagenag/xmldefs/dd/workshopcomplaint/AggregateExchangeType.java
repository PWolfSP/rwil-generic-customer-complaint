
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.sparepart.SparePartIdentifierType;

import lombok.Generated;


/**
 * Information related to the exchange case of the vehicle
 *           aggregate.
 * 
 * <p>Java-Klasse f�r AggregateExchangeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AggregateExchangeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/SparePart}SparePartRef" minOccurs="0"/>
 *         &lt;element name="SparePartInStockIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="AllocationNumber" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="SAPNumber" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="DeliveryDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="AnalysingPartner" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}AnalysingPartnerType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AggregateExchangeType", propOrder = {
    "sparePartRef",
    "sparePartInStockIND",
    "allocationNumber",
    "sapNumber",
    "deliveryDate",
    "analysingPartner"
})
public class AggregateExchangeType {

    @XmlElement(name = "SparePartRef", namespace = "http://xmldefs.volkswagenag.com/DD/SparePart")
    protected SparePartIdentifierType sparePartRef;
    @XmlElement(name = "SparePartInStockIND")
    protected Boolean sparePartInStockIND;
    @XmlElement(name = "AllocationNumber")
    protected BigDecimal allocationNumber;
    @XmlElement(name = "SAPNumber")
    protected BigDecimal sapNumber;
    @XmlElement(name = "DeliveryDate")
    protected String deliveryDate;
    @XmlElement(name = "AnalysingPartner")
    protected AnalysingPartnerType analysingPartner;

    /**
     * Ruft den Wert der sparePartRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SparePartIdentifierType }
     *     
     */
    public SparePartIdentifierType getSparePartRef() {
        return sparePartRef;
    }

    /**
     * Legt den Wert der sparePartRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SparePartIdentifierType }
     *     
     */
    public void setSparePartRef(SparePartIdentifierType value) {
        this.sparePartRef = value;
    }

    /**
     * Ruft den Wert der sparePartInStockIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSparePartInStockIND() {
        return sparePartInStockIND;
    }

    /**
     * Legt den Wert der sparePartInStockIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSparePartInStockIND(Boolean value) {
        this.sparePartInStockIND = value;
    }

    /**
     * Ruft den Wert der allocationNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAllocationNumber() {
        return allocationNumber;
    }

    /**
     * Legt den Wert der allocationNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAllocationNumber(BigDecimal value) {
        this.allocationNumber = value;
    }

    /**
     * Ruft den Wert der sapNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSAPNumber() {
        return sapNumber;
    }

    /**
     * Legt den Wert der sapNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSAPNumber(BigDecimal value) {
        this.sapNumber = value;
    }

    /**
     * Ruft den Wert der deliveryDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Legt den Wert der deliveryDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryDate(String value) {
        this.deliveryDate = value;
    }

    /**
     * Ruft den Wert der analysingPartner-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnalysingPartnerType }
     *     
     */
    public AnalysingPartnerType getAnalysingPartner() {
        return analysingPartner;
    }

    /**
     * Legt den Wert der analysingPartner-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnalysingPartnerType }
     *     
     */
    public void setAnalysingPartner(AnalysingPartnerType value) {
        this.analysingPartner = value;
    }

}

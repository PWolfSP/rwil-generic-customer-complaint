
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * A complaint attachment.
 * 
 * <p>Java-Klasse für AttachmentsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttachmentsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Attachment" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}AttachmentType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachmentsType", propOrder = {
    "attachment"
})



public class AttachmentsType {
	
	
    @XmlElement(name = "Attachment", required = true)
    private List<AttachmentType> attachment;


		public void setAttachmentType(List<AttachmentType> attachment) {
			this.attachment = attachment;
		}
		
	    public List<AttachmentType> getAttachment() {
	        if (attachment == null) {
	            attachment = new ArrayList<AttachmentType>();
	        }
	        return this.attachment;
	    }

}

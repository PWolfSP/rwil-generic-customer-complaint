
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PartnerFeedbackType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PartnerFeedbackType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsSupportHelpfulIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="AssessmentText" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerFeedbackType", propOrder = {
    "isSupportHelpfulIND",
    "assessmentText"
})
public class PartnerFeedbackType {

    @XmlElement(name = "IsSupportHelpfulIND")
    protected Boolean isSupportHelpfulIND;
    @XmlElement(name = "AssessmentText")
    protected TextType assessmentText;

    /**
     * Ruft den Wert der isSupportHelpfulIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSupportHelpfulIND() {
        return isSupportHelpfulIND;
    }

    /**
     * Legt den Wert der isSupportHelpfulIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSupportHelpfulIND(Boolean value) {
        this.isSupportHelpfulIND = value;
    }

    /**
     * Ruft den Wert der assessmentText-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getAssessmentText() {
        return assessmentText;
    }

    /**
     * Legt den Wert der assessmentText-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setAssessmentText(TextType value) {
        this.assessmentText = value;
    }

}


package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * A complaint event.
 * 
 * <p>Java-Klasse f�r ComplaintEventType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComplaintEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="EventTimestamp" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="ComplaintStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ComplaintRequestStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ComplaintView" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="UserTrigger" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}UserInfoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplaintEventType", propOrder = {
    "eventType",
    "eventTimestamp",
    "complaintStatus",
    "complaintRequestStatus",
    "complaintView",
    "userTrigger"
})
public class ComplaintEventType {

    @XmlElement(name = "EventType")
    protected CodeType eventType;
    @XmlElement(name = "EventTimestamp")
    protected String eventTimestamp;
    @XmlElement(name = "ComplaintStatus")
    protected CodeType complaintStatus;
    @XmlElement(name = "ComplaintRequestStatus")
    protected CodeType complaintRequestStatus;
    @XmlElement(name = "ComplaintView")
    protected CodeType complaintView;
    @XmlElement(name = "UserTrigger")
    protected UserInfoType userTrigger;

    /**
     * Ruft den Wert der eventType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getEventType() {
        return eventType;
    }

    /**
     * Legt den Wert der eventType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setEventType(CodeType value) {
        this.eventType = value;
    }

    /**
     * Ruft den Wert der eventTimestamp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventTimestamp() {
        return eventTimestamp;
    }

    /**
     * Legt den Wert der eventTimestamp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventTimestamp(String value) {
        this.eventTimestamp = value;
    }

    /**
     * Ruft den Wert der complaintStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getComplaintStatus() {
        return complaintStatus;
    }

    /**
     * Legt den Wert der complaintStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setComplaintStatus(CodeType value) {
        this.complaintStatus = value;
    }

    /**
     * Ruft den Wert der complaintRequestStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getComplaintRequestStatus() {
        return complaintRequestStatus;
    }

    /**
     * Legt den Wert der complaintRequestStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setComplaintRequestStatus(CodeType value) {
        this.complaintRequestStatus = value;
    }

    /**
     * Ruft den Wert der complaintView-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getComplaintView() {
        return complaintView;
    }

    /**
     * Legt den Wert der complaintView-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setComplaintView(CodeType value) {
        this.complaintView = value;
    }

    /**
     * Ruft den Wert der userTrigger-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserInfoType }
     *     
     */
    public UserInfoType getUserTrigger() {
        return userTrigger;
    }

    /**
     * Legt den Wert der userTrigger-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserInfoType }
     *     
     */
    public void setUserTrigger(UserInfoType value) {
        this.userTrigger = value;
    }

}


package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerIdentifierType;
import com.volkswagenag.xmldefs.dd.sparepart.SparePartIdentifierType;
import com.volkswagenag.xmldefs.dd.vehicle.VehicleIdentifierType;
import com.volkswagenag.xmldefs.dd.workshoporder.WorkshopOrderIdentifierType;

import lombok.Generated;


/**
 * A workshop complaint states a malfunction of a vehicle that has to be repaired in
 *           the workshop. In addition to all customer and workshop testimonies concerning the fault, even hotline channel
 *           requests and communication between hotline channel and business partner via text, attachment files or diagnostic
 *           protocols are covered in this business object. The WorkshopComplaint also provides information about its current
 *           progress of process. It states by whom and when its dependent entities have been last updated and which state of
 *           progress the complaint and its dependencies have been reached.
 * 
 * <p>Java-Klasse für WorkshopComplaintType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="WorkshopComplaintType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WorkshopComplaintIdentifier" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}WorkshopComplaintIdentifierType" minOccurs="0"/>
 *         &lt;element name="ComplaintType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="RequestType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}WorkshopOrderRef" minOccurs="0"/>
 *         &lt;element name="WorkshopLeadID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/Vehicle}VehicleRef" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/SparePart}SparePartRef" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}CustomerRef" minOccurs="0"/>
 *         &lt;element name="TechnicalSolution" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}TechnicalSolutionType" minOccurs="0"/>
 *         &lt;element name="VehicleTransfer" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}VehicleTransferType" minOccurs="0"/>
 *         &lt;element name="CustomerInformation" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}CustomerInformationType" minOccurs="0"/>
 *         &lt;element name="WorkshopInformation" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}WorkshopInformationType" minOccurs="0"/>
 *         &lt;element name="SystemInformation" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}SystemInformationType" minOccurs="0"/>
 *         &lt;element name="Classifications" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}ClassificationsType" minOccurs="0"/>
 *         &lt;element name="Timestamps" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}TimestampsType" minOccurs="0"/>
 *         &lt;element name="ComplaintRequest" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}ComplaintRequestType" minOccurs="0"/>
 *         &lt;element name="AggregateExchange" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}AggregateExchangeType" minOccurs="0"/>
 *         &lt;element name="Attachments" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}AttachmentsType" minOccurs="0"/>
 *         &lt;element name="EventsHistory" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}ComplaintEventsType" minOccurs="0"/>
 *         &lt;element name="RepairDiagnosticProtocols" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}RepairDiagnosticProtocolsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkshopComplaintType", propOrder = {
    "workshopComplaintIdentifier",
    "complaintType",
    "requestType",
    "status",
    "workshopOrderRef",
    "workshopLeadID",
    "vehicleRef",
    "sparePartRef",
    "customerRef",
    "technicalSolution",
    "vehicleTransfer",
    "customerInformation",
    "workshopInformation",
    "systemInformation",
    "classifications",
    "timestamps",
    "complaintRequest",
    "aggregateExchange",
    "attachments",
    "eventsHistory",
    "repairDiagnosticProtocols"
})
public class WorkshopComplaintType {

    @XmlElement(name = "WorkshopComplaintIdentifier")
    protected WorkshopComplaintIdentifierType workshopComplaintIdentifier;
    @XmlElement(name = "ComplaintType")
    protected CodeType complaintType;
    @XmlElement(name = "RequestType")
    protected CodeType requestType;
    @XmlElement(name = "Status")
    protected CodeType status;
    @XmlElement(name = "WorkshopOrderRef", namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopOrder")
    protected WorkshopOrderIdentifierType workshopOrderRef;
    @XmlElement(name = "WorkshopLeadID")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String workshopLeadID;
    @XmlElement(name = "VehicleRef", namespace = "http://xmldefs.volkswagenag.com/DD/Vehicle")
    protected VehicleIdentifierType vehicleRef;
    @XmlElement(name = "SparePartRef", namespace = "http://xmldefs.volkswagenag.com/DD/SparePart")
    protected SparePartIdentifierType sparePartRef;
    @XmlElement(name = "CustomerRef", namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner")
    protected PartnerIdentifierType customerRef;
    @XmlElement(name = "TechnicalSolution")
    protected TechnicalSolutionType technicalSolution;
    @XmlElement(name = "VehicleTransfer")
    protected VehicleTransferType vehicleTransfer;
    @XmlElement(name = "CustomerInformation")
    protected CustomerInformationType customerInformation;
    @XmlElement(name = "WorkshopInformation")
    protected WorkshopInformationType workshopInformation;
    @XmlElement(name = "SystemInformation")
    protected SystemInformationType systemInformation;
    @XmlElement(name = "Classifications")
    protected ClassificationsType classifications;
    @XmlElement(name = "Timestamps")
    protected TimestampsType timestamps;
    @XmlElement(name = "ComplaintRequest")
    protected ComplaintRequestType complaintRequest;
    @XmlElement(name = "AggregateExchange")
    protected AggregateExchangeType aggregateExchange;
    @XmlElement(name = "Attachments")
    protected AttachmentsType attachments;
    @XmlElement(name = "EventsHistory")
    protected ComplaintEventsType eventsHistory;
    @XmlElement(name = "RepairDiagnosticProtocols")
    protected RepairDiagnosticProtocolsType repairDiagnosticProtocols;

    /**
     * Ruft den Wert der workshopComplaintIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopComplaintIdentifierType }
     *     
     */
    public WorkshopComplaintIdentifierType getWorkshopComplaintIdentifier() {
        return workshopComplaintIdentifier;
    }

    /**
     * Legt den Wert der workshopComplaintIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopComplaintIdentifierType }
     *     
     */
    public void setWorkshopComplaintIdentifier(WorkshopComplaintIdentifierType value) {
        this.workshopComplaintIdentifier = value;
    }

    /**
     * Ruft den Wert der complaintType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getComplaintType() {
        return complaintType;
    }

    /**
     * Legt den Wert der complaintType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setComplaintType(CodeType value) {
        this.complaintType = value;
    }

    /**
     * Ruft den Wert der requestType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getRequestType() {
        return requestType;
    }

    /**
     * Legt den Wert der requestType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setRequestType(CodeType value) {
        this.requestType = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setStatus(CodeType value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der workshopOrderRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopOrderIdentifierType }
     *     
     */
    public WorkshopOrderIdentifierType getWorkshopOrderRef() {
        return workshopOrderRef;
    }

    /**
     * Legt den Wert der workshopOrderRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopOrderIdentifierType }
     *     
     */
    public void setWorkshopOrderRef(WorkshopOrderIdentifierType value) {
        this.workshopOrderRef = value;
    }

    /**
     * Ruft den Wert der workshopLeadID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkshopLeadID() {
        return workshopLeadID;
    }

    /**
     * Legt den Wert der workshopLeadID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkshopLeadID(String value) {
        this.workshopLeadID = value;
    }

    /**
     * Ruft den Wert der vehicleRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehicleIdentifierType }
     *     
     */
    public VehicleIdentifierType getVehicleRef() {
        return vehicleRef;
    }

    /**
     * Legt den Wert der vehicleRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleIdentifierType }
     *     
     */
    public void setVehicleRef(VehicleIdentifierType value) {
        this.vehicleRef = value;
    }

    /**
     * Ruft den Wert der sparePartRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SparePartIdentifierType }
     *     
     */
    public SparePartIdentifierType getSparePartRef() {
        return sparePartRef;
    }

    /**
     * Legt den Wert der sparePartRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SparePartIdentifierType }
     *     
     */
    public void setSparePartRef(SparePartIdentifierType value) {
        this.sparePartRef = value;
    }

    /**
     * Ruft den Wert der customerRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getCustomerRef() {
        return customerRef;
    }

    /**
     * Legt den Wert der customerRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setCustomerRef(PartnerIdentifierType value) {
        this.customerRef = value;
    }

    /**
     * Ruft den Wert der technicalSolution-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalSolutionType }
     *     
     */
    public TechnicalSolutionType getTechnicalSolution() {
        return technicalSolution;
    }

    /**
     * Legt den Wert der technicalSolution-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalSolutionType }
     *     
     */
    public void setTechnicalSolution(TechnicalSolutionType value) {
        this.technicalSolution = value;
    }

    /**
     * Ruft den Wert der vehicleTransfer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehicleTransferType }
     *     
     */
    public VehicleTransferType getVehicleTransfer() {
        return vehicleTransfer;
    }

    /**
     * Legt den Wert der vehicleTransfer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleTransferType }
     *     
     */
    public void setVehicleTransfer(VehicleTransferType value) {
        this.vehicleTransfer = value;
    }

    /**
     * Ruft den Wert der customerInformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CustomerInformationType }
     *     
     */
    public CustomerInformationType getCustomerInformation() {
        return customerInformation;
    }

    /**
     * Legt den Wert der customerInformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerInformationType }
     *     
     */
    public void setCustomerInformation(CustomerInformationType value) {
        this.customerInformation = value;
    }

    /**
     * Ruft den Wert der workshopInformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopInformationType }
     *     
     */
    public WorkshopInformationType getWorkshopInformation() {
        return workshopInformation;
    }

    /**
     * Legt den Wert der workshopInformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopInformationType }
     *     
     */
    public void setWorkshopInformation(WorkshopInformationType value) {
        this.workshopInformation = value;
    }

    /**
     * Ruft den Wert der systemInformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemInformationType }
     *     
     */
    public SystemInformationType getSystemInformation() {
        return systemInformation;
    }

    /**
     * Legt den Wert der systemInformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemInformationType }
     *     
     */
    public void setSystemInformation(SystemInformationType value) {
        this.systemInformation = value;
    }

    /**
     * Ruft den Wert der classifications-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ClassificationsType }
     *     
     */
    public ClassificationsType getClassifications() {
        return classifications;
    }

    /**
     * Legt den Wert der classifications-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassificationsType }
     *     
     */
    public void setClassifications(ClassificationsType value) {
        this.classifications = value;
    }

    /**
     * Ruft den Wert der timestamps-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimestampsType }
     *     
     */
    public TimestampsType getTimestamps() {
        return timestamps;
    }

    /**
     * Legt den Wert der timestamps-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimestampsType }
     *     
     */
    public void setTimestamps(TimestampsType value) {
        this.timestamps = value;
    }

    /**
     * Ruft den Wert der complaintRequest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ComplaintRequestType }
     *     
     */
    public ComplaintRequestType getComplaintRequest() {
        return complaintRequest;
    }

    /**
     * Legt den Wert der complaintRequest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplaintRequestType }
     *     
     */
    public void setComplaintRequest(ComplaintRequestType value) {
        this.complaintRequest = value;
    }

    /**
     * Ruft den Wert der aggregateExchange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AggregateExchangeType }
     *     
     */
    public AggregateExchangeType getAggregateExchange() {
        return aggregateExchange;
    }

    /**
     * Legt den Wert der aggregateExchange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AggregateExchangeType }
     *     
     */
    public void setAggregateExchange(AggregateExchangeType value) {
        this.aggregateExchange = value;
    }

    /**
     * Ruft den Wert der attachments-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentsType }
     *     
     */
    public AttachmentsType getAttachments() {
        return attachments;
    }

    /**
     * Legt den Wert der attachments-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentsType }
     *     
     */
    public void setAttachments(AttachmentsType value) {
        this.attachments = value;
    }

    /**
     * Ruft den Wert der eventsHistory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ComplaintEventsType }
     *     
     */
    public ComplaintEventsType getEventsHistory() {
        return eventsHistory;
    }

    /**
     * Legt den Wert der eventsHistory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplaintEventsType }
     *     
     */
    public void setEventsHistory(ComplaintEventsType value) {
        this.eventsHistory = value;
    }

    /**
     * Ruft den Wert der repairDiagnosticProtocols-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RepairDiagnosticProtocolsType }
     *     
     */
    public RepairDiagnosticProtocolsType getRepairDiagnosticProtocols() {
        return repairDiagnosticProtocols;
    }

    /**
     * Legt den Wert der repairDiagnosticProtocols-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RepairDiagnosticProtocolsType }
     *     
     */
    public void setRepairDiagnosticProtocols(RepairDiagnosticProtocolsType value) {
        this.repairDiagnosticProtocols = value;
    }

}

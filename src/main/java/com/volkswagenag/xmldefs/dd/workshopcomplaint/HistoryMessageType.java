
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse für HistoryMessageType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HistoryMessageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MessageType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="JustifiableRequestIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="MessageOriginType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="MessageDestinationType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="MessageTransmittedIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="FeedbackRequiredIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="MessageTimestamps" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}MessageTimestampsType" minOccurs="0"/>
 *         &lt;element name="WorkshopComplaintCodeChoice" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}ComplaintCodeChoiceType" minOccurs="0"/>
 *         &lt;element name="UserEditor" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}UserInfoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoryMessageType", propOrder = {
    "messageType",
    "text",
    "justifiableRequestIND",
    "messageOriginType",
    "messageDestinationType",
    "messageTransmittedIND",
    "feedbackRequiredIND",
    "messageTimestamps",
    "workshopComplaintCodeChoice",
    "userEditor"
})
public class HistoryMessageType {

    @XmlElement(name = "MessageType")
    protected CodeType messageType;
    @XmlElement(name = "Text")
    protected TextType text;
    @XmlElement(name = "JustifiableRequestIND")
    protected Boolean justifiableRequestIND;
    @XmlElement(name = "MessageOriginType")
    protected CodeType messageOriginType;
    @XmlElement(name = "MessageDestinationType")
    protected CodeType messageDestinationType;
    @XmlElement(name = "MessageTransmittedIND")
    protected Boolean messageTransmittedIND;
    @XmlElement(name = "FeedbackRequiredIND")
    protected Boolean feedbackRequiredIND;
    @XmlElement(name = "MessageTimestamps")
    protected MessageTimestampsType messageTimestamps;
    @XmlElement(name = "WorkshopComplaintCodeChoice")
    protected ComplaintCodeChoiceType workshopComplaintCodeChoice;
    @XmlElement(name = "UserEditor")
    protected UserInfoType userEditor;

    /**
     * Ruft den Wert der messageType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getMessageType() {
        return messageType;
    }

    /**
     * Legt den Wert der messageType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setMessageType(CodeType value) {
        this.messageType = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setText(TextType value) {
        this.text = value;
    }

    /**
     * Ruft den Wert der justifiableRequestIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isJustifiableRequestIND() {
        return justifiableRequestIND;
    }

    /**
     * Legt den Wert der justifiableRequestIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setJustifiableRequestIND(Boolean value) {
        this.justifiableRequestIND = value;
    }

    /**
     * Ruft den Wert der messageOriginType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getMessageOriginType() {
        return messageOriginType;
    }

    /**
     * Legt den Wert der messageOriginType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setMessageOriginType(CodeType value) {
        this.messageOriginType = value;
    }

    /**
     * Ruft den Wert der messageDestinationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getMessageDestinationType() {
        return messageDestinationType;
    }

    /**
     * Legt den Wert der messageDestinationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setMessageDestinationType(CodeType value) {
        this.messageDestinationType = value;
    }

    /**
     * Ruft den Wert der messageTransmittedIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMessageTransmittedIND() {
        return messageTransmittedIND;
    }

    /**
     * Legt den Wert der messageTransmittedIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMessageTransmittedIND(Boolean value) {
        this.messageTransmittedIND = value;
    }

    /**
     * Ruft den Wert der feedbackRequiredIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFeedbackRequiredIND() {
        return feedbackRequiredIND;
    }

    /**
     * Legt den Wert der feedbackRequiredIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFeedbackRequiredIND(Boolean value) {
        this.feedbackRequiredIND = value;
    }

    /**
     * Ruft den Wert der messageTimestamps-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MessageTimestampsType }
     *     
     */
    public MessageTimestampsType getMessageTimestamps() {
        return messageTimestamps;
    }

    /**
     * Legt den Wert der messageTimestamps-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageTimestampsType }
     *     
     */
    public void setMessageTimestamps(MessageTimestampsType value) {
        this.messageTimestamps = value;
    }

    /**
     * Ruft den Wert der workshopComplaintCodeChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ComplaintCodeChoiceType }
     *     
     */
    public ComplaintCodeChoiceType getWorkshopComplaintCodeChoice() {
        return workshopComplaintCodeChoice;
    }

    /**
     * Legt den Wert der workshopComplaintCodeChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplaintCodeChoiceType }
     *     
     */
    public void setWorkshopComplaintCodeChoice(ComplaintCodeChoiceType value) {
        this.workshopComplaintCodeChoice = value;
    }

    /**
     * Ruft den Wert der userEditor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserInfoType }
     *     
     */
    public UserInfoType getUserEditor() {
        return userEditor;
    }

    /**
     * Legt den Wert der userEditor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserInfoType }
     *     
     */
    public void setUserEditor(UserInfoType value) {
        this.userEditor = value;
    }

}

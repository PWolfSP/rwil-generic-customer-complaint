
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ComplaintRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComplaintRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="RequestProcessingStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="RequestProcessingLevel" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="JustifiableRequestIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="RequestRefusalReasons" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}RequestRefusalReasonsType" minOccurs="0"/>
 *         &lt;element name="Keywords" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}KeywordsType" minOccurs="0"/>
 *         &lt;element name="InterimWorkshopCodeChoice" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}ComplaintCodeChoiceType" minOccurs="0"/>
 *         &lt;element name="TechnicalSolution" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}TechnicalSolutionType" minOccurs="0"/>
 *         &lt;element name="LayerThicknesses" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}LayerThicknessesType" minOccurs="0"/>
 *         &lt;element name="RequestTimestamps" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}RequestTimestampsType" minOccurs="0"/>
 *         &lt;element name="AggregateExchangeIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="RequestInfo" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}RequestInfoType" minOccurs="0"/>
 *         &lt;element name="CurrentUserEditor" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}UserInfoType" minOccurs="0"/>
 *         &lt;element name="CurrentPartnerEditor" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}UserInfoType" minOccurs="0"/>
 *         &lt;element name="PartnerFeedback" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}PartnerFeedbackType" minOccurs="0"/>
 *         &lt;element name="HistoryMessages" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}HistoryMessagesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplaintRequestType", propOrder = {
    "requestType",
    "requestProcessingStatus",
    "requestProcessingLevel",
    "justifiableRequestIND",
    "requestRefusalReasons",
    "keywords",
    "interimWorkshopCodeChoice",
    "technicalSolution",
    "layerThicknesses",
    "requestTimestamps",
    "aggregateExchangeIND",
    "requestInfo",
    "currentUserEditor",
    "currentPartnerEditor",
    "partnerFeedback",
    "historyMessages"
})
public class ComplaintRequestType {

    @XmlElement(name = "RequestType")
    protected CodeType requestType;
    @XmlElement(name = "RequestProcessingStatus")
    protected CodeType requestProcessingStatus;
    @XmlElement(name = "RequestProcessingLevel")
    protected CodeType requestProcessingLevel;
    @XmlElement(name = "JustifiableRequestIND")
    protected Boolean justifiableRequestIND;
    @XmlElement(name = "RequestRefusalReasons")
    protected RequestRefusalReasonsType requestRefusalReasons;
    @XmlElement(name = "Keywords")
    protected KeywordsType keywords;
    @XmlElement(name = "InterimWorkshopCodeChoice")
    protected ComplaintCodeChoiceType interimWorkshopCodeChoice;
    @XmlElement(name = "TechnicalSolution")
    protected TechnicalSolutionType technicalSolution;
    @XmlElement(name = "LayerThicknesses")
    protected LayerThicknessesType layerThicknesses;
    @XmlElement(name = "RequestTimestamps")
    protected RequestTimestampsType requestTimestamps;
    @XmlElement(name = "AggregateExchangeIND")
    protected Boolean aggregateExchangeIND;
    @XmlElement(name = "RequestInfo")
    protected RequestInfoType requestInfo;
    @XmlElement(name = "CurrentUserEditor")
    protected UserInfoType currentUserEditor;
    @XmlElement(name = "CurrentPartnerEditor")
    protected UserInfoType currentPartnerEditor;
    @XmlElement(name = "PartnerFeedback")
    protected PartnerFeedbackType partnerFeedback;
    @XmlElement(name = "HistoryMessages")
    protected HistoryMessagesType historyMessages;

    /**
     * Ruft den Wert der requestType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getRequestType() {
        return requestType;
    }

    /**
     * Legt den Wert der requestType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setRequestType(CodeType value) {
        this.requestType = value;
    }

    /**
     * Ruft den Wert der requestProcessingStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getRequestProcessingStatus() {
        return requestProcessingStatus;
    }

    /**
     * Legt den Wert der requestProcessingStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setRequestProcessingStatus(CodeType value) {
        this.requestProcessingStatus = value;
    }

    /**
     * Ruft den Wert der requestProcessingLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getRequestProcessingLevel() {
        return requestProcessingLevel;
    }

    /**
     * Legt den Wert der requestProcessingLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setRequestProcessingLevel(CodeType value) {
        this.requestProcessingLevel = value;
    }

    /**
     * Ruft den Wert der justifiableRequestIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isJustifiableRequestIND() {
        return justifiableRequestIND;
    }

    /**
     * Legt den Wert der justifiableRequestIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setJustifiableRequestIND(Boolean value) {
        this.justifiableRequestIND = value;
    }

    /**
     * Ruft den Wert der requestRefusalReasons-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RequestRefusalReasonsType }
     *     
     */
    public RequestRefusalReasonsType getRequestRefusalReasons() {
        return requestRefusalReasons;
    }

    /**
     * Legt den Wert der requestRefusalReasons-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestRefusalReasonsType }
     *     
     */
    public void setRequestRefusalReasons(RequestRefusalReasonsType value) {
        this.requestRefusalReasons = value;
    }

    /**
     * Ruft den Wert der keywords-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KeywordsType }
     *     
     */
    public KeywordsType getKeywords() {
        return keywords;
    }

    /**
     * Legt den Wert der keywords-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KeywordsType }
     *     
     */
    public void setKeywords(KeywordsType value) {
        this.keywords = value;
    }

    /**
     * Ruft den Wert der interimWorkshopCodeChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ComplaintCodeChoiceType }
     *     
     */
    public ComplaintCodeChoiceType getInterimWorkshopCodeChoice() {
        return interimWorkshopCodeChoice;
    }

    /**
     * Legt den Wert der interimWorkshopCodeChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplaintCodeChoiceType }
     *     
     */
    public void setInterimWorkshopCodeChoice(ComplaintCodeChoiceType value) {
        this.interimWorkshopCodeChoice = value;
    }

    /**
     * Ruft den Wert der technicalSolution-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalSolutionType }
     *     
     */
    public TechnicalSolutionType getTechnicalSolution() {
        return technicalSolution;
    }

    /**
     * Legt den Wert der technicalSolution-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalSolutionType }
     *     
     */
    public void setTechnicalSolution(TechnicalSolutionType value) {
        this.technicalSolution = value;
    }

    /**
     * Ruft den Wert der layerThicknesses-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LayerThicknessesType }
     *     
     */
    public LayerThicknessesType getLayerThicknesses() {
        return layerThicknesses;
    }

    /**
     * Legt den Wert der layerThicknesses-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LayerThicknessesType }
     *     
     */
    public void setLayerThicknesses(LayerThicknessesType value) {
        this.layerThicknesses = value;
    }

    /**
     * Ruft den Wert der requestTimestamps-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RequestTimestampsType }
     *     
     */
    public RequestTimestampsType getRequestTimestamps() {
        return requestTimestamps;
    }

    /**
     * Legt den Wert der requestTimestamps-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestTimestampsType }
     *     
     */
    public void setRequestTimestamps(RequestTimestampsType value) {
        this.requestTimestamps = value;
    }

    /**
     * Ruft den Wert der aggregateExchangeIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAggregateExchangeIND() {
        return aggregateExchangeIND;
    }

    /**
     * Legt den Wert der aggregateExchangeIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAggregateExchangeIND(Boolean value) {
        this.aggregateExchangeIND = value;
    }

    /**
     * Ruft den Wert der requestInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RequestInfoType }
     *     
     */
    public RequestInfoType getRequestInfo() {
        return requestInfo;
    }

    /**
     * Legt den Wert der requestInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestInfoType }
     *     
     */
    public void setRequestInfo(RequestInfoType value) {
        this.requestInfo = value;
    }

    /**
     * Ruft den Wert der currentUserEditor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserInfoType }
     *     
     */
    public UserInfoType getCurrentUserEditor() {
        return currentUserEditor;
    }

    /**
     * Legt den Wert der currentUserEditor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserInfoType }
     *     
     */
    public void setCurrentUserEditor(UserInfoType value) {
        this.currentUserEditor = value;
    }

    /**
     * Ruft den Wert der currentPartnerEditor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserInfoType }
     *     
     */
    public UserInfoType getCurrentPartnerEditor() {
        return currentPartnerEditor;
    }

    /**
     * Legt den Wert der currentPartnerEditor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserInfoType }
     *     
     */
    public void setCurrentPartnerEditor(UserInfoType value) {
        this.currentPartnerEditor = value;
    }

    /**
     * Ruft den Wert der partnerFeedback-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerFeedbackType }
     *     
     */
    public PartnerFeedbackType getPartnerFeedback() {
        return partnerFeedback;
    }

    /**
     * Legt den Wert der partnerFeedback-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerFeedbackType }
     *     
     */
    public void setPartnerFeedback(PartnerFeedbackType value) {
        this.partnerFeedback = value;
    }

    /**
     * Ruft den Wert der historyMessages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HistoryMessagesType }
     *     
     */
    public HistoryMessagesType getHistoryMessages() {
        return historyMessages;
    }

    /**
     * Legt den Wert der historyMessages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoryMessagesType }
     *     
     */
    public void setHistoryMessages(HistoryMessagesType value) {
        this.historyMessages = value;
    }

}

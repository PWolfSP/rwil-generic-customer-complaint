
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r RequestTimestampsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RequestTimestampsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreationLocalDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="ResponseDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="ResponseDeadlineDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="LastSendDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="LastSendLocalDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestTimestampsType", propOrder = {
    "creationLocalDate",
    "responseDate",
    "responseDeadlineDate",
    "lastSendDate",
    "lastSendLocalDate"
})
public class RequestTimestampsType {

    @XmlElement(name = "CreationLocalDate")
    protected String creationLocalDate;
    @XmlElement(name = "ResponseDate")
    protected String responseDate;
    @XmlElement(name = "ResponseDeadlineDate")
    protected String responseDeadlineDate;
    @XmlElement(name = "LastSendDate")
    protected String lastSendDate;
    @XmlElement(name = "LastSendLocalDate")
    protected String lastSendLocalDate;

    /**
     * Ruft den Wert der creationLocalDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationLocalDate() {
        return creationLocalDate;
    }

    /**
     * Legt den Wert der creationLocalDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationLocalDate(String value) {
        this.creationLocalDate = value;
    }

    /**
     * Ruft den Wert der responseDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseDate() {
        return responseDate;
    }

    /**
     * Legt den Wert der responseDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseDate(String value) {
        this.responseDate = value;
    }

    /**
     * Ruft den Wert der responseDeadlineDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseDeadlineDate() {
        return responseDeadlineDate;
    }

    /**
     * Legt den Wert der responseDeadlineDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseDeadlineDate(String value) {
        this.responseDeadlineDate = value;
    }

    /**
     * Ruft den Wert der lastSendDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastSendDate() {
        return lastSendDate;
    }

    /**
     * Legt den Wert der lastSendDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastSendDate(String value) {
        this.lastSendDate = value;
    }

    /**
     * Ruft den Wert der lastSendLocalDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastSendLocalDate() {
        return lastSendLocalDate;
    }

    /**
     * Legt den Wert der lastSendLocalDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastSendLocalDate(String value) {
        this.lastSendLocalDate = value;
    }

}


package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * A complaint attachment.
 * 
 * <p>Java-Klasse für ComplaintEventsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComplaintEventsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ComplaintEvent" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}ComplaintEventType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplaintEventsType", propOrder = {
    "complaintEvent"
})



public class ComplaintEventsType {
	
	
    @XmlElement(name = "ComplaintEvent", required = true)
    private List<ComplaintEventType> complaintEvent;


		public void setComplaintEventType(List<ComplaintEventType> complaintEvent) {
			this.complaintEvent = complaintEvent;
		}
		
	    public List<ComplaintEventType> getComplaintEvent() {
	        if (complaintEvent == null) {
	            complaintEvent = new ArrayList<ComplaintEventType>();
	        }
	        return this.complaintEvent;
	    }

}

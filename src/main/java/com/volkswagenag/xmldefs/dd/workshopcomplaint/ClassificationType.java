
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;
import com.volkswagenag.xmldefs.dd.efacode.EFACodeIdentifierType;

import lombok.Generated;


/**
 * A classification of the complaint (DISS, EFA or Testimony)
 * 
 * <p>Java-Klasse für ClassificationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ClassificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Type" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DISSCode" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}DISSCodeType" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/EFACode}EFACodeRef" minOccurs="0"/>
 *         &lt;element name="Testimony" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassificationType", propOrder = {
    "type",
    "dissCode",
    "efaCodeRef",
    "testimony"
})
public class ClassificationType {

    @XmlElement(name = "Type")
    protected CodeType type;
    @XmlElement(name = "DISSCode")
    protected DISSCodeType dissCode;
    @XmlElement(name = "EFACodeRef", namespace = "http://xmldefs.volkswagenag.com/DD/EFACode")
    protected EFACodeIdentifierType efaCodeRef;
    @XmlElement(name = "Testimony")
    protected TextType testimony;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setType(CodeType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der dissCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DISSCodeType }
     *     
     */
    public DISSCodeType getDISSCode() {
        return dissCode;
    }

    /**
     * Legt den Wert der dissCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DISSCodeType }
     *     
     */
    public void setDISSCode(DISSCodeType value) {
        this.dissCode = value;
    }

    /**
     * Ruft den Wert der efaCodeRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EFACodeIdentifierType }
     *     
     */
    public EFACodeIdentifierType getEFACodeRef() {
        return efaCodeRef;
    }

    /**
     * Legt den Wert der efaCodeRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EFACodeIdentifierType }
     *     
     */
    public void setEFACodeRef(EFACodeIdentifierType value) {
        this.efaCodeRef = value;
    }

    /**
     * Ruft den Wert der testimony-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getTestimony() {
        return testimony;
    }

    /**
     * Legt den Wert der testimony-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setTestimony(TextType value) {
        this.testimony = value;
    }

}

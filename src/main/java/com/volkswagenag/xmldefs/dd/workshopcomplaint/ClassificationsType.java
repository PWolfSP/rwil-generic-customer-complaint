
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * List of classifications.
 * 
 * <p>Java-Klasse für ClassificationsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ClassificationsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Classification" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}ClassificationType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassificationsType", propOrder = {
    "classification"
})



public class ClassificationsType {
	
	
    @XmlElement(name = "Classification")
    private List<ClassificationType> classification;


		public void setClassificationType(List<ClassificationType> classification) {
			this.classification = classification;
		}
		
	    public List<ClassificationType> getClassification() {
	        if (classification == null) {
	            classification = new ArrayList<ClassificationType>();
	        }
	        return this.classification;
	    }

}

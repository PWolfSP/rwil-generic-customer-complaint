
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r CustomerInformationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CustomerInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ComplaintSince" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ComplaintSinceDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="Frequency" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="PriorWorkshopInspection" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DrivableIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerInformationType", propOrder = {
    "complaintSince",
    "complaintSinceDate",
    "frequency",
    "priorWorkshopInspection",
    "drivableIND"
})
public class CustomerInformationType {

    @XmlElement(name = "ComplaintSince")
    protected CodeType complaintSince;
    @XmlElement(name = "ComplaintSinceDate")
    protected String complaintSinceDate;
    @XmlElement(name = "Frequency")
    protected CodeType frequency;
    @XmlElement(name = "PriorWorkshopInspection")
    protected CodeType priorWorkshopInspection;
    @XmlElement(name = "DrivableIND")
    protected Boolean drivableIND;

    /**
     * Ruft den Wert der complaintSince-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getComplaintSince() {
        return complaintSince;
    }

    /**
     * Legt den Wert der complaintSince-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setComplaintSince(CodeType value) {
        this.complaintSince = value;
    }

    /**
     * Ruft den Wert der complaintSinceDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplaintSinceDate() {
        return complaintSinceDate;
    }

    /**
     * Legt den Wert der complaintSinceDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplaintSinceDate(String value) {
        this.complaintSinceDate = value;
    }

    /**
     * Ruft den Wert der frequency-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getFrequency() {
        return frequency;
    }

    /**
     * Legt den Wert der frequency-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setFrequency(CodeType value) {
        this.frequency = value;
    }

    /**
     * Ruft den Wert der priorWorkshopInspection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getPriorWorkshopInspection() {
        return priorWorkshopInspection;
    }

    /**
     * Legt den Wert der priorWorkshopInspection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setPriorWorkshopInspection(CodeType value) {
        this.priorWorkshopInspection = value;
    }

    /**
     * Ruft den Wert der drivableIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDrivableIND() {
        return drivableIND;
    }

    /**
     * Legt den Wert der drivableIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDrivableIND(Boolean value) {
        this.drivableIND = value;
    }

}


package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * List of reasons for refusal of request.
 * 
 * <p>Java-Klasse für RequestRefusalReasonsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RequestRefusalReasonsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RefusalReason" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}RefusalReasonType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestRefusalReasonsType", propOrder = {
    "refusalReason"
})



public class RequestRefusalReasonsType {
	
	
    @XmlElement(name = "RefusalReason", required = true)
    private List<RefusalReasonType> refusalReason;


		public void setRefusalReasonType(List<RefusalReasonType> refusalReason) {
			this.refusalReason = refusalReason;
		}
		
	    public List<RefusalReasonType> getRefusalReason() {
	        if (refusalReason == null) {
	            refusalReason = new ArrayList<RefusalReasonType>();
	        }
	        return this.refusalReason;
	    }

}

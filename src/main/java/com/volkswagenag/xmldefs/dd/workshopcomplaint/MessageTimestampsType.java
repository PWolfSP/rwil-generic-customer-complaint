
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r MessageTimestampsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MessageTimestampsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreationDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="CreationLocalDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="LastChangeDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="LastChangeLocalDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageTimestampsType", propOrder = {
    "creationDate",
    "creationLocalDate",
    "lastChangeDate",
    "lastChangeLocalDate"
})
public class MessageTimestampsType {

    @XmlElement(name = "CreationDate")
    protected String creationDate;
    @XmlElement(name = "CreationLocalDate")
    protected String creationLocalDate;
    @XmlElement(name = "LastChangeDate")
    protected String lastChangeDate;
    @XmlElement(name = "LastChangeLocalDate")
    protected String lastChangeLocalDate;

    /**
     * Ruft den Wert der creationDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * Legt den Wert der creationDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationDate(String value) {
        this.creationDate = value;
    }

    /**
     * Ruft den Wert der creationLocalDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationLocalDate() {
        return creationLocalDate;
    }

    /**
     * Legt den Wert der creationLocalDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationLocalDate(String value) {
        this.creationLocalDate = value;
    }

    /**
     * Ruft den Wert der lastChangeDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastChangeDate() {
        return lastChangeDate;
    }

    /**
     * Legt den Wert der lastChangeDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastChangeDate(String value) {
        this.lastChangeDate = value;
    }

    /**
     * Ruft den Wert der lastChangeLocalDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastChangeLocalDate() {
        return lastChangeLocalDate;
    }

    /**
     * Legt den Wert der lastChangeLocalDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastChangeLocalDate(String value) {
        this.lastChangeLocalDate = value;
    }

}

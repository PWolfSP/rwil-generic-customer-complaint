
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.efacode.EFACodeIdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ComplaintCodeChoiceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComplaintCodeChoiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodeOrigin" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}ComplaintCodeOriginChoiceCodelist"/>
 *         &lt;element name="DISSCode" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}DISSCodeType" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/EFACode}EFACodeRef" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplaintCodeChoiceType", propOrder = {
    "codeOrigin",
    "dissCode",
    "efaCodeRef"
})
public class ComplaintCodeChoiceType {

    @XmlElement(name = "CodeOrigin", required = true)
    @XmlSchemaType(name = "string")
    protected ComplaintCodeOriginChoiceCodelist codeOrigin;
    @XmlElement(name = "DISSCode")
    protected DISSCodeType dissCode;
    @XmlElement(name = "EFACodeRef", namespace = "http://xmldefs.volkswagenag.com/DD/EFACode")
    protected EFACodeIdentifierType efaCodeRef;

    /**
     * Ruft den Wert der codeOrigin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ComplaintCodeOriginChoiceCodelist }
     *     
     */
    public ComplaintCodeOriginChoiceCodelist getCodeOrigin() {
        return codeOrigin;
    }

    /**
     * Legt den Wert der codeOrigin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplaintCodeOriginChoiceCodelist }
     *     
     */
    public void setCodeOrigin(ComplaintCodeOriginChoiceCodelist value) {
        this.codeOrigin = value;
    }

    /**
     * Ruft den Wert der dissCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DISSCodeType }
     *     
     */
    public DISSCodeType getDISSCode() {
        return dissCode;
    }

    /**
     * Legt den Wert der dissCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DISSCodeType }
     *     
     */
    public void setDISSCode(DISSCodeType value) {
        this.dissCode = value;
    }

    /**
     * Ruft den Wert der efaCodeRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EFACodeIdentifierType }
     *     
     */
    public EFACodeIdentifierType getEFACodeRef() {
        return efaCodeRef;
    }

    /**
     * Legt den Wert der efaCodeRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EFACodeIdentifierType }
     *     
     */
    public void setEFACodeRef(EFACodeIdentifierType value) {
        this.efaCodeRef = value;
    }

}

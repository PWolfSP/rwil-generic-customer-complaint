
package com.volkswagenag.xmldefs.dd.commons;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.commons package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _Info_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Commons", "Info");
    private final static QName _Error_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Commons", "Error");
    private final static QName _Confirmation_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Commons", "Confirmation");
    private final static QName _AliveTestAcknowledgement_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Commons", "AliveTestAcknowledgement");
    private final static QName _Information_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Commons", "Information");
    private final static QName _Fault_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Commons", "Fault");
    private final static QName _FaultBasic_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Commons", "FaultBasic");
    private final static QName _Acknowledgement_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Commons", "Acknowledgement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.commons
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AcknowledgementType }
     * 
     */
    public AcknowledgementType createAcknowledgementType() {
        return new AcknowledgementType();
    }

    /**
     * Create an instance of {@link FaultBasicType }
     * 
     */
    public FaultBasicType createFaultBasicType() {
        return new FaultBasicType();
    }

    /**
     * Create an instance of {@link AliveTestAcknowledgementType }
     * 
     */
    public AliveTestAcknowledgementType createAliveTestAcknowledgementType() {
        return new AliveTestAcknowledgementType();
    }

    /**
     * Create an instance of {@link OldFaultType }
     * 
     */
    public OldFaultType createOldFaultType() {
        return new OldFaultType();
    }

    /**
     * Create an instance of {@link FaultType }
     * 
     */
    public FaultType createFaultType() {
        return new FaultType();
    }

    /**
     * Create an instance of {@link ConfirmationType }
     * 
     */
    public ConfirmationType createConfirmationType() {
        return new ConfirmationType();
    }

    /**
     * Create an instance of {@link SystemParamType }
     * 
     */
    public SystemParamType createSystemParamType() {
        return new SystemParamType();
    }

    /**
     * Create an instance of {@link FaultCodeDetailType }
     * 
     */
    public FaultCodeDetailType createFaultCodeDetailType() {
        return new FaultCodeDetailType();
    }

    /**
     * Create an instance of {@link OldFaultSystemType }
     * 
     */
    public OldFaultSystemType createOldFaultSystemType() {
        return new OldFaultSystemType();
    }

    /**
     * Create an instance of {@link FaultSystemDetailType }
     * 
     */
    public FaultSystemDetailType createFaultSystemDetailType() {
        return new FaultSystemDetailType();
    }

    /**
     * Create an instance of {@link CodeParamType }
     * 
     */
    public CodeParamType createCodeParamType() {
        return new CodeParamType();
    }

    /**
     * Create an instance of {@link ServiceProviderType }
     * 
     */
    public ServiceProviderType createServiceProviderType() {
        return new ServiceProviderType();
    }

    /**
     * Create an instance of {@link ServiceInfoType }
     * 
     */
    public ServiceInfoType createServiceInfoType() {
        return new ServiceInfoType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Commons", name = "Info")
    public JAXBElement<FaultType> createInfo(FaultType value) {
        return new JAXBElement<FaultType>(_Info_QNAME, FaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OldFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Commons", name = "Error")
    public JAXBElement<OldFaultType> createError(OldFaultType value) {
        return new JAXBElement<OldFaultType>(_Error_QNAME, OldFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Commons", name = "Confirmation")
    public JAXBElement<ConfirmationType> createConfirmation(ConfirmationType value) {
        return new JAXBElement<ConfirmationType>(_Confirmation_QNAME, ConfirmationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AliveTestAcknowledgementType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Commons", name = "AliveTestAcknowledgement")
    public JAXBElement<AliveTestAcknowledgementType> createAliveTestAcknowledgement(AliveTestAcknowledgementType value) {
        return new JAXBElement<AliveTestAcknowledgementType>(_AliveTestAcknowledgement_QNAME, AliveTestAcknowledgementType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OldFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Commons", name = "Information")
    public JAXBElement<OldFaultType> createInformation(OldFaultType value) {
        return new JAXBElement<OldFaultType>(_Information_QNAME, OldFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Commons", name = "Fault")
    public JAXBElement<FaultType> createFault(FaultType value) {
        return new JAXBElement<FaultType>(_Fault_QNAME, FaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultBasicType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Commons", name = "FaultBasic")
    public JAXBElement<FaultBasicType> createFaultBasic(FaultBasicType value) {
        return new JAXBElement<FaultBasicType>(_FaultBasic_QNAME, FaultBasicType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcknowledgementType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Commons", name = "Acknowledgement")
    public JAXBElement<AcknowledgementType> createAcknowledgement(AcknowledgementType value) {
        return new JAXBElement<AcknowledgementType>(_Acknowledgement_QNAME, AcknowledgementType.class, null, value);
    }

}

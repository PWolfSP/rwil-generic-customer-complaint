
package com.volkswagenag.xmldefs.dd.commons;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r FaultBasicType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FaultBasicType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FaultSystem" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="FaultClass" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="FaultCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String"/>
 *         &lt;element name="FaultLevel" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="FaultID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="FaultTimestamp" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType"/>
 *         &lt;element name="FaultDescription" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType"/>
 *         &lt;element name="FaultCodeDetails" type="{http://xmldefs.volkswagenag.com/DD/Commons}FaultCodeDetailType" minOccurs="0"/>
 *         &lt;element name="FaultSystemDetails" type="{http://xmldefs.volkswagenag.com/DD/Commons}FaultSystemDetailType" minOccurs="0"/>
 *         &lt;element name="FaultReason" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaultBasicType", propOrder = {
    "faultSystem",
    "faultClass",
    "faultCode",
    "faultLevel",
    "faultID",
    "faultTimestamp",
    "faultDescription",
    "faultCodeDetails",
    "faultSystemDetails",
    "faultReason"
})
public class FaultBasicType {

    @XmlElement(name = "FaultSystem", required = true)
    protected CodeType faultSystem;
    @XmlElement(name = "FaultClass", required = true)
    protected CodeType faultClass;
    @XmlElement(name = "FaultCode", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String faultCode;
    @XmlElement(name = "FaultLevel", required = true)
    protected CodeType faultLevel;
    @XmlElement(name = "FaultID")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String faultID;
    @XmlElement(name = "FaultTimestamp", required = true)
    protected String faultTimestamp;
    @XmlElement(name = "FaultDescription", required = true)
    protected TextType faultDescription;
    @XmlElement(name = "FaultCodeDetails")
    protected FaultCodeDetailType faultCodeDetails;
    @XmlElement(name = "FaultSystemDetails")
    protected FaultSystemDetailType faultSystemDetails;
    @XmlElement(name = "FaultReason")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String faultReason;

    /**
     * Ruft den Wert der faultSystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getFaultSystem() {
        return faultSystem;
    }

    /**
     * Legt den Wert der faultSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setFaultSystem(CodeType value) {
        this.faultSystem = value;
    }

    /**
     * Ruft den Wert der faultClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getFaultClass() {
        return faultClass;
    }

    /**
     * Legt den Wert der faultClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setFaultClass(CodeType value) {
        this.faultClass = value;
    }

    /**
     * Ruft den Wert der faultCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaultCode() {
        return faultCode;
    }

    /**
     * Legt den Wert der faultCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaultCode(String value) {
        this.faultCode = value;
    }

    /**
     * Ruft den Wert der faultLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getFaultLevel() {
        return faultLevel;
    }

    /**
     * Legt den Wert der faultLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setFaultLevel(CodeType value) {
        this.faultLevel = value;
    }

    /**
     * Ruft den Wert der faultID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaultID() {
        return faultID;
    }

    /**
     * Legt den Wert der faultID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaultID(String value) {
        this.faultID = value;
    }

    /**
     * Ruft den Wert der faultTimestamp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaultTimestamp() {
        return faultTimestamp;
    }

    /**
     * Legt den Wert der faultTimestamp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaultTimestamp(String value) {
        this.faultTimestamp = value;
    }

    /**
     * Ruft den Wert der faultDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getFaultDescription() {
        return faultDescription;
    }

    /**
     * Legt den Wert der faultDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setFaultDescription(TextType value) {
        this.faultDescription = value;
    }

    /**
     * Ruft den Wert der faultCodeDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FaultCodeDetailType }
     *     
     */
    public FaultCodeDetailType getFaultCodeDetails() {
        return faultCodeDetails;
    }

    /**
     * Legt den Wert der faultCodeDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FaultCodeDetailType }
     *     
     */
    public void setFaultCodeDetails(FaultCodeDetailType value) {
        this.faultCodeDetails = value;
    }

    /**
     * Ruft den Wert der faultSystemDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FaultSystemDetailType }
     *     
     */
    public FaultSystemDetailType getFaultSystemDetails() {
        return faultSystemDetails;
    }

    /**
     * Legt den Wert der faultSystemDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FaultSystemDetailType }
     *     
     */
    public void setFaultSystemDetails(FaultSystemDetailType value) {
        this.faultSystemDetails = value;
    }

    /**
     * Ruft den Wert der faultReason-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaultReason() {
        return faultReason;
    }

    /**
     * Legt den Wert der faultReason-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaultReason(String value) {
        this.faultReason = value;
    }

}


package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für DealersType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DealersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Dealer" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}DealerType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealersType", propOrder = {
    "dealer"
})



public class DealersType {
	
	
    @XmlElement(name = "Dealer", required = true)
    private List<DealerType> dealer;


		public void setDealerType(List<DealerType> dealer) {
			this.dealer = dealer;
		}
		
	    public List<DealerType> getDealer() {
	        if (dealer == null) {
	            dealer = new ArrayList<DealerType>();
	        }
	        return this.dealer;
	    }

}


package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für TrailerLoadsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TrailerLoadsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TrailerLoad" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TrailerLoadType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrailerLoadsType", propOrder = {
    "trailerLoad"
})



public class TrailerLoadsType {
	
	
    @XmlElement(name = "TrailerLoad", required = true)
    private List<TrailerLoadType> trailerLoad;


		public void setTrailerLoadType(List<TrailerLoadType> trailerLoad) {
			this.trailerLoad = trailerLoad;
		}
		
	    public List<TrailerLoadType> getTrailerLoad() {
	        if (trailerLoad == null) {
	            trailerLoad = new ArrayList<TrailerLoadType>();
	        }
	        return this.trailerLoad;
	    }

}

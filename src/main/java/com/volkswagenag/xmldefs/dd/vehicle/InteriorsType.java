
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für InteriorsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InteriorsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InteriorCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InteriorsType", propOrder = {
    "interiorCode"
})



public class InteriorsType {
	
	
    @XmlElement(name = "InteriorCode", required = true)
    private List<CodeType> interiorCode;


		public void setCodeType(List<CodeType> interiorCode) {
			this.interiorCode = interiorCode;
		}
		
	    public List<CodeType> getInteriorCode() {
	        if (interiorCode == null) {
	            interiorCode = new ArrayList<CodeType>();
	        }
	        return this.interiorCode;
	    }

}

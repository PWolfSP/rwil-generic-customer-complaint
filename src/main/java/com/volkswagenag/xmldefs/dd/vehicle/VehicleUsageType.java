
package com.volkswagenag.xmldefs.dd.vehicle;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für VehicleUsageType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VehicleUsageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DrivingSchoolCarIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="DemonstrationCarIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="PilotRunIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="RentalCarIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="ShowroomVehicleIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="TaxiIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="LeasingIND" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}LeasingType" minOccurs="0"/>
 *         &lt;element name="OwnerCNT" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="OwnerKind" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleUsageType", propOrder = {
    "drivingSchoolCarIND",
    "demonstrationCarIND",
    "pilotRunIND",
    "rentalCarIND",
    "showroomVehicleIND",
    "taxiIND",
    "leasingIND",
    "ownerCNT",
    "ownerKind"
})
public class VehicleUsageType {

    @XmlElement(name = "DrivingSchoolCarIND")
    protected Boolean drivingSchoolCarIND;
    @XmlElement(name = "DemonstrationCarIND")
    protected Boolean demonstrationCarIND;
    @XmlElement(name = "PilotRunIND")
    protected Boolean pilotRunIND;
    @XmlElement(name = "RentalCarIND")
    protected Boolean rentalCarIND;
    @XmlElement(name = "ShowroomVehicleIND")
    protected Boolean showroomVehicleIND;
    @XmlElement(name = "TaxiIND")
    protected Boolean taxiIND;
    @XmlElement(name = "LeasingIND")
    protected LeasingType leasingIND;
    @XmlElement(name = "OwnerCNT")
    protected BigDecimal ownerCNT;
    @XmlElement(name = "OwnerKind")
    protected CodeType ownerKind;

    /**
     * Ruft den Wert der drivingSchoolCarIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDrivingSchoolCarIND() {
        return drivingSchoolCarIND;
    }

    /**
     * Legt den Wert der drivingSchoolCarIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDrivingSchoolCarIND(Boolean value) {
        this.drivingSchoolCarIND = value;
    }

    /**
     * Ruft den Wert der demonstrationCarIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDemonstrationCarIND() {
        return demonstrationCarIND;
    }

    /**
     * Legt den Wert der demonstrationCarIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDemonstrationCarIND(Boolean value) {
        this.demonstrationCarIND = value;
    }

    /**
     * Ruft den Wert der pilotRunIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPilotRunIND() {
        return pilotRunIND;
    }

    /**
     * Legt den Wert der pilotRunIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPilotRunIND(Boolean value) {
        this.pilotRunIND = value;
    }

    /**
     * Ruft den Wert der rentalCarIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRentalCarIND() {
        return rentalCarIND;
    }

    /**
     * Legt den Wert der rentalCarIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRentalCarIND(Boolean value) {
        this.rentalCarIND = value;
    }

    /**
     * Ruft den Wert der showroomVehicleIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowroomVehicleIND() {
        return showroomVehicleIND;
    }

    /**
     * Legt den Wert der showroomVehicleIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowroomVehicleIND(Boolean value) {
        this.showroomVehicleIND = value;
    }

    /**
     * Ruft den Wert der taxiIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxiIND() {
        return taxiIND;
    }

    /**
     * Legt den Wert der taxiIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxiIND(Boolean value) {
        this.taxiIND = value;
    }

    /**
     * Ruft den Wert der leasingIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LeasingType }
     *     
     */
    public LeasingType getLeasingIND() {
        return leasingIND;
    }

    /**
     * Legt den Wert der leasingIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LeasingType }
     *     
     */
    public void setLeasingIND(LeasingType value) {
        this.leasingIND = value;
    }

    /**
     * Ruft den Wert der ownerCNT-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOwnerCNT() {
        return ownerCNT;
    }

    /**
     * Legt den Wert der ownerCNT-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOwnerCNT(BigDecimal value) {
        this.ownerCNT = value;
    }

    /**
     * Ruft den Wert der ownerKind-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOwnerKind() {
        return ownerKind;
    }

    /**
     * Legt den Wert der ownerKind-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOwnerKind(CodeType value) {
        this.ownerKind = value;
    }

}

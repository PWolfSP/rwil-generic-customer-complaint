
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für MediaDataType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MediaDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Media" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}MediaType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MediaDataType", propOrder = {
    "media"
})



public class MediaDataType {
	
	
    @XmlElement(name = "Media", required = true)
    private List<MediaType> media;


		public void setMediaType(List<MediaType> media) {
			this.media = media;
		}
		
	    public List<MediaType> getMedia() {
	        if (media == null) {
	            media = new ArrayList<MediaType>();
	        }
	        return this.media;
	    }

}

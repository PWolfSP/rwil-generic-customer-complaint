
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ConsumptionsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConsumptionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Consumption" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ConsumptionType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsumptionsType", propOrder = {
    "consumption"
})



public class ConsumptionsType {
	
	
    @XmlElement(name = "Consumption", required = true)
    private List<ConsumptionType> consumption;


		public void setConsumptionType(List<ConsumptionType> consumption) {
			this.consumption = consumption;
		}
		
	    public List<ConsumptionType> getConsumption() {
	        if (consumption == null) {
	            consumption = new ArrayList<ConsumptionType>();
	        }
	        return this.consumption;
	    }

}

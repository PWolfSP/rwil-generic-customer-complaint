
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für OdometerReadingsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OdometerReadingsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OdometerReading" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}OdometerReadingType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OdometerReadingsType", propOrder = {
    "odometerReading"
})



public class OdometerReadingsType {
	
	
    @XmlElement(name = "OdometerReading", required = true)
    private List<OdometerReadingType> odometerReading;


		public void setOdometerReadingType(List<OdometerReadingType> odometerReading) {
			this.odometerReading = odometerReading;
		}
		
	    public List<OdometerReadingType> getOdometerReading() {
	        if (odometerReading == null) {
	            odometerReading = new ArrayList<OdometerReadingType>();
	        }
	        return this.odometerReading;
	    }

}

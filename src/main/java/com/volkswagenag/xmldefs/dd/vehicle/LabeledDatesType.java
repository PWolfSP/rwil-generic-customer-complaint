
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für LabeledDatesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LabeledDatesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Date" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}LabeledDateType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LabeledDatesType", propOrder = {
    "date"
})



public class LabeledDatesType {
	
	
    @XmlElement(name = "Date", required = true)
    private List<LabeledDateType> date;


		public void setLabeledDateType(List<LabeledDateType> date) {
			this.date = date;
		}
		
	    public List<LabeledDateType> getDate() {
	        if (date == null) {
	            date = new ArrayList<LabeledDateType>();
	        }
	        return this.date;
	    }

}

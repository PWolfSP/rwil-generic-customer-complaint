
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.CountryCodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * Date form of the first registration of the vehicle.
 * 
 * <p>Java-Klasse für RegistrationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RegistrationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InitialRegistrationIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="LicenseTags" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}LicenseTagsType" minOccurs="0"/>
 *         &lt;element name="Owner" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}OwnerType" minOccurs="0"/>
 *         &lt;element name="CertificateNo" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Country" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CountryCodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationType", propOrder = {
    "initialRegistrationIND",
    "licenseTags",
    "owner",
    "certificateNo",
    "status",
    "country"
})
public class RegistrationType {

    @XmlElement(name = "InitialRegistrationIND", defaultValue = "false")
    protected Boolean initialRegistrationIND;
    @XmlElement(name = "LicenseTags")
    protected LicenseTagsType licenseTags;
    @XmlElement(name = "Owner")
    protected OwnerType owner;
    @XmlElement(name = "CertificateNo")
    protected IdentifierType certificateNo;
    @XmlElement(name = "Status")
    protected CodeType status;
    @XmlElement(name = "Country")
    protected CountryCodeType country;

    /**
     * Ruft den Wert der initialRegistrationIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInitialRegistrationIND() {
        return initialRegistrationIND;
    }

    /**
     * Legt den Wert der initialRegistrationIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInitialRegistrationIND(Boolean value) {
        this.initialRegistrationIND = value;
    }

    /**
     * Ruft den Wert der licenseTags-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTagsType }
     *     
     */
    public LicenseTagsType getLicenseTags() {
        return licenseTags;
    }

    /**
     * Legt den Wert der licenseTags-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTagsType }
     *     
     */
    public void setLicenseTags(LicenseTagsType value) {
        this.licenseTags = value;
    }

    /**
     * Ruft den Wert der owner-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OwnerType }
     *     
     */
    public OwnerType getOwner() {
        return owner;
    }

    /**
     * Legt den Wert der owner-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OwnerType }
     *     
     */
    public void setOwner(OwnerType value) {
        this.owner = value;
    }

    /**
     * Ruft den Wert der certificateNo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getCertificateNo() {
        return certificateNo;
    }

    /**
     * Legt den Wert der certificateNo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setCertificateNo(IdentifierType value) {
        this.certificateNo = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setStatus(CodeType value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der country-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getCountry() {
        return country;
    }

    /**
     * Legt den Wert der country-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setCountry(CountryCodeType value) {
        this.country = value;
    }

}

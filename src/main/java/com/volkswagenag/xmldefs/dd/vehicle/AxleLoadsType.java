
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AxleLoadsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AxleLoadsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AxleLoad" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}AxleLoadType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AxleLoadsType", propOrder = {
    "axleLoad"
})



public class AxleLoadsType {
	
	
    @XmlElement(name = "AxleLoad", required = true)
    private List<AxleLoadType> axleLoad;


		public void setAxleLoadType(List<AxleLoadType> axleLoad) {
			this.axleLoad = axleLoad;
		}
		
	    public List<AxleLoadType> getAxleLoad() {
	        if (axleLoad == null) {
	            axleLoad = new ArrayList<AxleLoadType>();
	        }
	        return this.axleLoad;
	    }

}

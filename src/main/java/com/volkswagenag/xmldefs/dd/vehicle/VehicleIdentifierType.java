
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für VehicleIdentifierType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VehicleIdentifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VehicleUID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="ApplicationIdentifiers" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ApplicationIdentifiersType" minOccurs="0"/>
 *         &lt;element name="PKN" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="VIN" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="LicensePlate" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}LicensePlateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleIdentifierType", propOrder = {
    "vehicleUID",
    "applicationIdentifiers",
    "pkn",
    "vin",
    "licensePlate"
})
public class VehicleIdentifierType {

    @XmlElement(name = "VehicleUID")
    protected IdentifierType vehicleUID;
    @XmlElement(name = "ApplicationIdentifiers")
    protected ApplicationIdentifiersType applicationIdentifiers;
    @XmlElement(name = "PKN")
    protected IdentifierType pkn;
    @XmlElement(name = "VIN")
    protected IdentifierType vin;
    @XmlElement(name = "LicensePlate")
    protected LicensePlateType licensePlate;

    /**
     * Ruft den Wert der vehicleUID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getVehicleUID() {
        return vehicleUID;
    }

    /**
     * Legt den Wert der vehicleUID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setVehicleUID(IdentifierType value) {
        this.vehicleUID = value;
    }

    /**
     * Ruft den Wert der applicationIdentifiers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public ApplicationIdentifiersType getApplicationIdentifiers() {
        return applicationIdentifiers;
    }

    /**
     * Legt den Wert der applicationIdentifiers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public void setApplicationIdentifiers(ApplicationIdentifiersType value) {
        this.applicationIdentifiers = value;
    }

    /**
     * Ruft den Wert der pkn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getPKN() {
        return pkn;
    }

    /**
     * Legt den Wert der pkn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setPKN(IdentifierType value) {
        this.pkn = value;
    }

    /**
     * Ruft den Wert der vin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getVIN() {
        return vin;
    }

    /**
     * Legt den Wert der vin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setVIN(IdentifierType value) {
        this.vin = value;
    }

    /**
     * Ruft den Wert der licensePlate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LicensePlateType }
     *     
     */
    public LicensePlateType getLicensePlate() {
        return licensePlate;
    }

    /**
     * Legt den Wert der licensePlate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LicensePlateType }
     *     
     */
    public void setLicensePlate(LicensePlateType value) {
        this.licensePlate = value;
    }

}

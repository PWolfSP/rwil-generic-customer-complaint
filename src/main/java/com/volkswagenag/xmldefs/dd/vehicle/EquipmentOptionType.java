
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EquipmentOptionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EquipmentOptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OptionID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="OptionCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="EquipmentClass" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="OptionGroup" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ISetCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="XSetCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="Family" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="Descriptions" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentDescriptionsType" minOccurs="0"/>
 *         &lt;element name="Manufacturer" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ManufacturerType" minOccurs="0"/>
 *         &lt;element name="Fitting" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}QualifiedDateType" minOccurs="0"/>
 *         &lt;element name="Removal" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}QualifiedDateType" minOccurs="0"/>
 *         &lt;element name="LastModified" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}QualifiedDateType" minOccurs="0"/>
 *         &lt;element name="Warranties" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}WarrantiesType" minOccurs="0"/>
 *         &lt;element name="Includes" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentRefsType" minOccurs="0"/>
 *         &lt;element name="Excludes" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentRefsType" minOccurs="0"/>
 *         &lt;element name="Replaces" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentRefsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentOptionType", propOrder = {
    "optionID",
    "optionCode",
    "equipmentClass",
    "optionGroup",
    "iSetCode",
    "xSetCode",
    "family",
    "descriptions",
    "manufacturer",
    "fitting",
    "removal",
    "lastModified",
    "warranties",
    "includes",
    "excludes",
    "replaces"
})
public class EquipmentOptionType {

    @XmlElement(name = "OptionID")
    protected IdentifierType optionID;
    @XmlElement(name = "OptionCode", required = true)
    protected CodeType optionCode;
    @XmlElement(name = "EquipmentClass")
    protected CodeType equipmentClass;
    @XmlElement(name = "OptionGroup")
    protected CodeType optionGroup;
    @XmlElement(name = "ISetCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String iSetCode;
    @XmlElement(name = "XSetCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String xSetCode;
    @XmlElement(name = "Family")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String family;
    @XmlElement(name = "Descriptions")
    protected EquipmentDescriptionsType descriptions;
    @XmlElement(name = "Manufacturer")
    protected ManufacturerType manufacturer;
    @XmlElement(name = "Fitting")
    protected QualifiedDateType fitting;
    @XmlElement(name = "Removal")
    protected QualifiedDateType removal;
    @XmlElement(name = "LastModified")
    protected QualifiedDateType lastModified;
    @XmlElement(name = "Warranties")
    protected WarrantiesType warranties;
    @XmlElement(name = "Includes")
    protected EquipmentRefsType includes;
    @XmlElement(name = "Excludes")
    protected EquipmentRefsType excludes;
    @XmlElement(name = "Replaces")
    protected EquipmentRefsType replaces;

    /**
     * Ruft den Wert der optionID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getOptionID() {
        return optionID;
    }

    /**
     * Legt den Wert der optionID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setOptionID(IdentifierType value) {
        this.optionID = value;
    }

    /**
     * Ruft den Wert der optionCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOptionCode() {
        return optionCode;
    }

    /**
     * Legt den Wert der optionCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOptionCode(CodeType value) {
        this.optionCode = value;
    }

    /**
     * Ruft den Wert der equipmentClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getEquipmentClass() {
        return equipmentClass;
    }

    /**
     * Legt den Wert der equipmentClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setEquipmentClass(CodeType value) {
        this.equipmentClass = value;
    }

    /**
     * Ruft den Wert der optionGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOptionGroup() {
        return optionGroup;
    }

    /**
     * Legt den Wert der optionGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOptionGroup(CodeType value) {
        this.optionGroup = value;
    }

    /**
     * Ruft den Wert der iSetCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISetCode() {
        return iSetCode;
    }

    /**
     * Legt den Wert der iSetCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISetCode(String value) {
        this.iSetCode = value;
    }

    /**
     * Ruft den Wert der xSetCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXSetCode() {
        return xSetCode;
    }

    /**
     * Legt den Wert der xSetCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXSetCode(String value) {
        this.xSetCode = value;
    }

    /**
     * Ruft den Wert der family-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamily() {
        return family;
    }

    /**
     * Legt den Wert der family-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamily(String value) {
        this.family = value;
    }

    /**
     * Ruft den Wert der descriptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentDescriptionsType }
     *     
     */
    public EquipmentDescriptionsType getDescriptions() {
        return descriptions;
    }

    /**
     * Legt den Wert der descriptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentDescriptionsType }
     *     
     */
    public void setDescriptions(EquipmentDescriptionsType value) {
        this.descriptions = value;
    }

    /**
     * Ruft den Wert der manufacturer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ManufacturerType }
     *     
     */
    public ManufacturerType getManufacturer() {
        return manufacturer;
    }

    /**
     * Legt den Wert der manufacturer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ManufacturerType }
     *     
     */
    public void setManufacturer(ManufacturerType value) {
        this.manufacturer = value;
    }

    /**
     * Ruft den Wert der fitting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QualifiedDateType }
     *     
     */
    public QualifiedDateType getFitting() {
        return fitting;
    }

    /**
     * Legt den Wert der fitting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifiedDateType }
     *     
     */
    public void setFitting(QualifiedDateType value) {
        this.fitting = value;
    }

    /**
     * Ruft den Wert der removal-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QualifiedDateType }
     *     
     */
    public QualifiedDateType getRemoval() {
        return removal;
    }

    /**
     * Legt den Wert der removal-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifiedDateType }
     *     
     */
    public void setRemoval(QualifiedDateType value) {
        this.removal = value;
    }

    /**
     * Ruft den Wert der lastModified-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QualifiedDateType }
     *     
     */
    public QualifiedDateType getLastModified() {
        return lastModified;
    }

    /**
     * Legt den Wert der lastModified-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifiedDateType }
     *     
     */
    public void setLastModified(QualifiedDateType value) {
        this.lastModified = value;
    }

    /**
     * Ruft den Wert der warranties-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WarrantiesType }
     *     
     */
    public WarrantiesType getWarranties() {
        return warranties;
    }

    /**
     * Legt den Wert der warranties-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WarrantiesType }
     *     
     */
    public void setWarranties(WarrantiesType value) {
        this.warranties = value;
    }

    /**
     * Ruft den Wert der includes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentRefsType }
     *     
     */
    public EquipmentRefsType getIncludes() {
        return includes;
    }

    /**
     * Legt den Wert der includes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentRefsType }
     *     
     */
    public void setIncludes(EquipmentRefsType value) {
        this.includes = value;
    }

    /**
     * Ruft den Wert der excludes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentRefsType }
     *     
     */
    public EquipmentRefsType getExcludes() {
        return excludes;
    }

    /**
     * Legt den Wert der excludes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentRefsType }
     *     
     */
    public void setExcludes(EquipmentRefsType value) {
        this.excludes = value;
    }

    /**
     * Ruft den Wert der replaces-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentRefsType }
     *     
     */
    public EquipmentRefsType getReplaces() {
        return replaces;
    }

    /**
     * Legt den Wert der replaces-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentRefsType }
     *     
     */
    public void setReplaces(EquipmentRefsType value) {
        this.replaces = value;
    }

}

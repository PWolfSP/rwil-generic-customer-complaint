
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.MeasureType;

import lombok.Generated;


/**
 * <p>Java-Klasse für DimensionsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DimensionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Length" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="Width" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="Height" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="Wheelbase" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="Tracks" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TracksType" minOccurs="0"/>
 *         &lt;element name="LoadCompartments" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}LoadCompartmentsType" minOccurs="0"/>
 *         &lt;element name="LoadCompartmentVolume" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DimensionsType", propOrder = {
    "length",
    "width",
    "height",
    "wheelbase",
    "tracks",
    "loadCompartments",
    "loadCompartmentVolume"
})
public class DimensionsType {

    @XmlElement(name = "Length")
    protected MeasureType length;
    @XmlElement(name = "Width")
    protected MeasureType width;
    @XmlElement(name = "Height")
    protected MeasureType height;
    @XmlElement(name = "Wheelbase")
    protected MeasureType wheelbase;
    @XmlElement(name = "Tracks")
    protected TracksType tracks;
    @XmlElement(name = "LoadCompartments")
    protected LoadCompartmentsType loadCompartments;
    @XmlElement(name = "LoadCompartmentVolume")
    protected MeasureType loadCompartmentVolume;

    /**
     * Ruft den Wert der length-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getLength() {
        return length;
    }

    /**
     * Legt den Wert der length-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setLength(MeasureType value) {
        this.length = value;
    }

    /**
     * Ruft den Wert der width-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getWidth() {
        return width;
    }

    /**
     * Legt den Wert der width-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setWidth(MeasureType value) {
        this.width = value;
    }

    /**
     * Ruft den Wert der height-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getHeight() {
        return height;
    }

    /**
     * Legt den Wert der height-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setHeight(MeasureType value) {
        this.height = value;
    }

    /**
     * Ruft den Wert der wheelbase-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getWheelbase() {
        return wheelbase;
    }

    /**
     * Legt den Wert der wheelbase-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setWheelbase(MeasureType value) {
        this.wheelbase = value;
    }

    /**
     * Ruft den Wert der tracks-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TracksType }
     *     
     */
    public TracksType getTracks() {
        return tracks;
    }

    /**
     * Legt den Wert der tracks-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TracksType }
     *     
     */
    public void setTracks(TracksType value) {
        this.tracks = value;
    }

    /**
     * Ruft den Wert der loadCompartments-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LoadCompartmentsType }
     *     
     */
    public LoadCompartmentsType getLoadCompartments() {
        return loadCompartments;
    }

    /**
     * Legt den Wert der loadCompartments-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LoadCompartmentsType }
     *     
     */
    public void setLoadCompartments(LoadCompartmentsType value) {
        this.loadCompartments = value;
    }

    /**
     * Ruft den Wert der loadCompartmentVolume-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getLoadCompartmentVolume() {
        return loadCompartmentVolume;
    }

    /**
     * Legt den Wert der loadCompartmentVolume-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setLoadCompartmentVolume(MeasureType value) {
        this.loadCompartmentVolume = value;
    }

}

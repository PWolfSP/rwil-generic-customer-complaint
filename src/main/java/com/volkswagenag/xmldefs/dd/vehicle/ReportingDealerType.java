
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerIdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ReportingDealerType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReportingDealerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DealerRef" minOccurs="0"/>
 *         &lt;element name="Reporter" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingDealerType", propOrder = {
    "dealerRef",
    "reporter"
})
public class ReportingDealerType {

    @XmlElement(name = "DealerRef", namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner")
    protected PartnerIdentifierType dealerRef;
    @XmlElement(name = "Reporter", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String reporter;

    /**
     * Ruft den Wert der dealerRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getDealerRef() {
        return dealerRef;
    }

    /**
     * Legt den Wert der dealerRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setDealerRef(PartnerIdentifierType value) {
        this.dealerRef = value;
    }

    /**
     * Ruft den Wert der reporter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporter() {
        return reporter;
    }

    /**
     * Legt den Wert der reporter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporter(String value) {
        this.reporter = value;
    }

}


package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für TransmissionTypesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TransmissionTypesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransmissionType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionTypesType", propOrder = {
    "transmissionType"
})



public class TransmissionTypesType {
	
	
    @XmlElement(name = "TransmissionType", required = true)
    private List<CodeType> transmissionType;


		public void setCodeType(List<CodeType> transmissionType) {
			this.transmissionType = transmissionType;
		}
		
	    public List<CodeType> getTransmissionType() {
	        if (transmissionType == null) {
	            transmissionType = new ArrayList<CodeType>();
	        }
	        return this.transmissionType;
	    }

}

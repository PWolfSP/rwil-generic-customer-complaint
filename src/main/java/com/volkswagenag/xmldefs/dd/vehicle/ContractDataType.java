
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ContractDataType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ContractDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Warranties" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}WarrantiesType" minOccurs="0"/>
 *         &lt;element name="Services" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ServicesType" minOccurs="0"/>
 *         &lt;element name="AdditionalConditions" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}AdditionalConditionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractDataType", propOrder = {
    "warranties",
    "services",
    "additionalConditions"
})
public class ContractDataType {

    @XmlElement(name = "Warranties")
    protected WarrantiesType warranties;
    @XmlElement(name = "Services")
    protected ServicesType services;
    @XmlElement(name = "AdditionalConditions")
    protected AdditionalConditionsType additionalConditions;

    /**
     * Ruft den Wert der warranties-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WarrantiesType }
     *     
     */
    public WarrantiesType getWarranties() {
        return warranties;
    }

    /**
     * Legt den Wert der warranties-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WarrantiesType }
     *     
     */
    public void setWarranties(WarrantiesType value) {
        this.warranties = value;
    }

    /**
     * Ruft den Wert der services-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServicesType }
     *     
     */
    public ServicesType getServices() {
        return services;
    }

    /**
     * Legt den Wert der services-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicesType }
     *     
     */
    public void setServices(ServicesType value) {
        this.services = value;
    }

    /**
     * Ruft den Wert der additionalConditions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalConditionsType }
     *     
     */
    public AdditionalConditionsType getAdditionalConditions() {
        return additionalConditions;
    }

    /**
     * Legt den Wert der additionalConditions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalConditionsType }
     *     
     */
    public void setAdditionalConditions(AdditionalConditionsType value) {
        this.additionalConditions = value;
    }

}


package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * standard form. The kind of the date must be stated by the TYPE attribute. The
 *           element may occur multiple. Applicable values are: ZP7 -> date the vehicle passed the production point ZP7 ZP8
 *           -> date the vehicle passed the production point ZP8 trade-in -> date of trade-in or purchase of a dealer (used
 *           vehicle context) (optional) sales -> selling date (used vehicle context) (optional) GEWAS -> special date to
 *           hold the delivery date of VW group company cars
 * 
 * <p>Java-Klasse f�r TimeLineType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeLineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductionDate" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ProductionDateType" minOccurs="0"/>
 *         &lt;element name="PurchaseDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="ServiceProvisionDate" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ServiceProvisionDateType" minOccurs="0"/>
 *         &lt;element name="DeliveryDate" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}DeliveryDateType" minOccurs="0"/>
 *         &lt;element name="InitialRegistrationDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="Dates" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}LabeledDatesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeLineType", propOrder = {
    "productionDate",
    "purchaseDate",
    "serviceProvisionDate",
    "deliveryDate",
    "initialRegistrationDate",
    "dates"
})
public class TimeLineType {

    @XmlElement(name = "ProductionDate")
    protected ProductionDateType productionDate;
    @XmlElement(name = "PurchaseDate")
    protected String purchaseDate;
    @XmlElement(name = "ServiceProvisionDate")
    protected ServiceProvisionDateType serviceProvisionDate;
    @XmlElement(name = "DeliveryDate")
    protected DeliveryDateType deliveryDate;
    @XmlElement(name = "InitialRegistrationDate")
    protected String initialRegistrationDate;
    @XmlElement(name = "Dates")
    protected LabeledDatesType dates;

    /**
     * Ruft den Wert der productionDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProductionDateType }
     *     
     */
    public ProductionDateType getProductionDate() {
        return productionDate;
    }

    /**
     * Legt den Wert der productionDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductionDateType }
     *     
     */
    public void setProductionDate(ProductionDateType value) {
        this.productionDate = value;
    }

    /**
     * Ruft den Wert der purchaseDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseDate() {
        return purchaseDate;
    }

    /**
     * Legt den Wert der purchaseDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseDate(String value) {
        this.purchaseDate = value;
    }

    /**
     * Ruft den Wert der serviceProvisionDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProvisionDateType }
     *     
     */
    public ServiceProvisionDateType getServiceProvisionDate() {
        return serviceProvisionDate;
    }

    /**
     * Legt den Wert der serviceProvisionDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProvisionDateType }
     *     
     */
    public void setServiceProvisionDate(ServiceProvisionDateType value) {
        this.serviceProvisionDate = value;
    }

    /**
     * Ruft den Wert der deliveryDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryDateType }
     *     
     */
    public DeliveryDateType getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Legt den Wert der deliveryDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryDateType }
     *     
     */
    public void setDeliveryDate(DeliveryDateType value) {
        this.deliveryDate = value;
    }

    /**
     * Ruft den Wert der initialRegistrationDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitialRegistrationDate() {
        return initialRegistrationDate;
    }

    /**
     * Legt den Wert der initialRegistrationDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitialRegistrationDate(String value) {
        this.initialRegistrationDate = value;
    }

    /**
     * Ruft den Wert der dates-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledDatesType }
     *     
     */
    public LabeledDatesType getDates() {
        return dates;
    }

    /**
     * Legt den Wert der dates-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledDatesType }
     *     
     */
    public void setDates(LabeledDatesType value) {
        this.dates = value;
    }

}

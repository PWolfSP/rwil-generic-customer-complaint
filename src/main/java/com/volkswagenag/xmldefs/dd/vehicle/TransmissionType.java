
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse für TransmissionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TransmissionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Kind" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="TransmissionCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="TransmissionTypes" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TransmissionTypesType" minOccurs="0"/>
 *         &lt;element name="TransmissionNo" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="TransmissionClass" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ProductionDate" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ProductionDateType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionType", propOrder = {
    "kind",
    "transmissionCode",
    "transmissionTypes",
    "transmissionNo",
    "transmissionClass",
    "productionDate",
    "description"
})
public class TransmissionType {

    @XmlElement(name = "Kind", required = true)
    protected CodeType kind;
    @XmlElement(name = "TransmissionCode")
    protected CodeType transmissionCode;
    @XmlElement(name = "TransmissionTypes")
    protected TransmissionTypesType transmissionTypes;
    @XmlElement(name = "TransmissionNo")
    protected IdentifierType transmissionNo;
    @XmlElement(name = "TransmissionClass")
    protected CodeType transmissionClass;
    @XmlElement(name = "ProductionDate")
    protected ProductionDateType productionDate;
    @XmlElement(name = "Description")
    protected TextType description;

    /**
     * Ruft den Wert der kind-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getKind() {
        return kind;
    }

    /**
     * Legt den Wert der kind-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setKind(CodeType value) {
        this.kind = value;
    }

    /**
     * Ruft den Wert der transmissionCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getTransmissionCode() {
        return transmissionCode;
    }

    /**
     * Legt den Wert der transmissionCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setTransmissionCode(CodeType value) {
        this.transmissionCode = value;
    }

    /**
     * Ruft den Wert der transmissionTypes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionTypesType }
     *     
     */
    public TransmissionTypesType getTransmissionTypes() {
        return transmissionTypes;
    }

    /**
     * Legt den Wert der transmissionTypes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionTypesType }
     *     
     */
    public void setTransmissionTypes(TransmissionTypesType value) {
        this.transmissionTypes = value;
    }

    /**
     * Ruft den Wert der transmissionNo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getTransmissionNo() {
        return transmissionNo;
    }

    /**
     * Legt den Wert der transmissionNo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setTransmissionNo(IdentifierType value) {
        this.transmissionNo = value;
    }

    /**
     * Ruft den Wert der transmissionClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getTransmissionClass() {
        return transmissionClass;
    }

    /**
     * Legt den Wert der transmissionClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setTransmissionClass(CodeType value) {
        this.transmissionClass = value;
    }

    /**
     * Ruft den Wert der productionDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProductionDateType }
     *     
     */
    public ProductionDateType getProductionDate() {
        return productionDate;
    }

    /**
     * Legt den Wert der productionDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductionDateType }
     *     
     */
    public void setProductionDate(ProductionDateType value) {
        this.productionDate = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

}


package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AdditionalConditionsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AdditionalConditionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdditionalCondition" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}AdditionalConditionType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalConditionsType", propOrder = {
    "additionalCondition"
})



public class AdditionalConditionsType {
	
	
    @XmlElement(name = "AdditionalCondition", required = true)
    private List<AdditionalConditionType> additionalCondition;


		public void setAdditionalConditionType(List<AdditionalConditionType> additionalCondition) {
			this.additionalCondition = additionalCondition;
		}
		
	    public List<AdditionalConditionType> getAdditionalCondition() {
	        if (additionalCondition == null) {
	            additionalCondition = new ArrayList<AdditionalConditionType>();
	        }
	        return this.additionalCondition;
	    }

}


package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EquipmentsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EquipmentsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Equipment" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentsType", propOrder = {
    "equipment"
})



public class EquipmentsType {
	
	
    @XmlElement(name = "Equipment", required = true)
    private List<EquipmentType> equipment;


		public void setEquipmentType(List<EquipmentType> equipment) {
			this.equipment = equipment;
		}
		
	    public List<EquipmentType> getEquipment() {
	        if (equipment == null) {
	            equipment = new ArrayList<EquipmentType>();
	        }
	        return this.equipment;
	    }

}


package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für LoadCompartmentsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LoadCompartmentsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoadCompartment" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}LoadCompartmentType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoadCompartmentsType", propOrder = {
    "loadCompartment"
})



public class LoadCompartmentsType {
	
	
    @XmlElement(name = "LoadCompartment", required = true)
    private List<LoadCompartmentType> loadCompartment;


		public void setLoadCompartmentType(List<LoadCompartmentType> loadCompartment) {
			this.loadCompartment = loadCompartment;
		}
		
	    public List<LoadCompartmentType> getLoadCompartment() {
	        if (loadCompartment == null) {
	            loadCompartment = new ArrayList<LoadCompartmentType>();
	        }
	        return this.loadCompartment;
	    }

}

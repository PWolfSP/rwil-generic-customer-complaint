
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für MaintenancesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MaintenancesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LastMaintenance" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}MaintenanceType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaintenancesType", propOrder = {
    "lastMaintenance"
})



public class MaintenancesType {
	
	
    @XmlElement(name = "LastMaintenance", required = true)
    private List<MaintenanceType> lastMaintenance;


		public void setMaintenanceType(List<MaintenanceType> lastMaintenance) {
			this.lastMaintenance = lastMaintenance;
		}
		
	    public List<MaintenanceType> getLastMaintenance() {
	        if (lastMaintenance == null) {
	            lastMaintenance = new ArrayList<MaintenanceType>();
	        }
	        return this.lastMaintenance;
	    }

}

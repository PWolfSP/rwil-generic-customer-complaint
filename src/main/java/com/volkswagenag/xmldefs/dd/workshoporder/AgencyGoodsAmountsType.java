
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AgencyGoodsAmountsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AgencyGoodsAmountsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyGoodsAmount" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}PriceAmountType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgencyGoodsAmountsType", propOrder = {
    "agencyGoodsAmount"
})



public class AgencyGoodsAmountsType {
	
	
    @XmlElement(name = "AgencyGoodsAmount", required = true)
    private List<PriceAmountType> agencyGoodsAmount;


		public void setPriceAmountType(List<PriceAmountType> agencyGoodsAmount) {
			this.agencyGoodsAmount = agencyGoodsAmount;
		}
		
	    public List<PriceAmountType> getAgencyGoodsAmount() {
	        if (agencyGoodsAmount == null) {
	            agencyGoodsAmount = new ArrayList<PriceAmountType>();
	        }
	        return this.agencyGoodsAmount;
	    }

}

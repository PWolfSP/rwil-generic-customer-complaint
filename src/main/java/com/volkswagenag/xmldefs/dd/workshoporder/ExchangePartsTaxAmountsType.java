
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ExchangePartsTaxAmountsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ExchangePartsTaxAmountsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExchangePartsTaxAmount" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}TaxAmountType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangePartsTaxAmountsType", propOrder = {
    "exchangePartsTaxAmount"
})



public class ExchangePartsTaxAmountsType {
	
	
    @XmlElement(name = "ExchangePartsTaxAmount", required = true)
    private List<TaxAmountType> exchangePartsTaxAmount;


		public void setTaxAmountType(List<TaxAmountType> exchangePartsTaxAmount) {
			this.exchangePartsTaxAmount = exchangePartsTaxAmount;
		}
		
	    public List<TaxAmountType> getExchangePartsTaxAmount() {
	        if (exchangePartsTaxAmount == null) {
	            exchangePartsTaxAmount = new ArrayList<TaxAmountType>();
	        }
	        return this.exchangePartsTaxAmount;
	    }

}


package com.volkswagenag.xmldefs.dd.efacode;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ConditionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConditionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConditionID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *         &lt;element name="ConditionTitle" type="{http://xmldefs.volkswagenag.com/DD/EFACode}TitleType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConditionType", propOrder = {
    "conditionID",
    "conditionTitle"
})



public class ConditionType {
	
	
    @XmlElement(name = "ConditionID", required = true)
    protected IdentifierType conditionID;
    @XmlElement(name = "ConditionTitle")
    private List<TitleType> conditionTitle;

    /**
     * Ruft den Wert der conditionID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getConditionID() {
        return conditionID;
    }

    /**
     * Legt den Wert der conditionID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setConditionID(IdentifierType value) {
        this.conditionID = value;
    }


		public void setTitleType(List<TitleType> conditionTitle) {
			this.conditionTitle = conditionTitle;
		}
		
	    public List<TitleType> getConditionTitle() {
	        if (conditionTitle == null) {
	            conditionTitle = new ArrayList<TitleType>();
	        }
	        return this.conditionTitle;
	    }

}

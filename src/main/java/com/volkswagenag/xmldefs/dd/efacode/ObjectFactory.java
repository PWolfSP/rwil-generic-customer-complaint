
package com.volkswagenag.xmldefs.dd.efacode;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.efacode package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _EFACode_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/EFACode", "EFACode");
    private final static QName _EFACodeRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/EFACode", "EFACodeRef");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.efacode
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EFACodeType }
     * 
     */
    public EFACodeType createEFACodeType() {
        return new EFACodeType();
    }

    /**
     * Create an instance of {@link EFACodeIdentifierType }
     * 
     */
    public EFACodeIdentifierType createEFACodeIdentifierType() {
        return new EFACodeIdentifierType();
    }

    /**
     * Create an instance of {@link PositionType }
     * 
     */
    public PositionType createPositionType() {
        return new PositionType();
    }

    /**
     * Create an instance of {@link ObjectTitleType }
     * 
     */
    public ObjectTitleType createObjectTitleType() {
        return new ObjectTitleType();
    }

    /**
     * Create an instance of {@link ApplicationIdentifiersType }
     * 
     */
    public ApplicationIdentifiersType createApplicationIdentifiersType() {
        return new ApplicationIdentifiersType();
    }

    /**
     * Create an instance of {@link ErrorObjectType }
     * 
     */
    public ErrorObjectType createErrorObjectType() {
        return new ErrorObjectType();
    }

    /**
     * Create an instance of {@link PictureMappingType }
     * 
     */
    public PictureMappingType createPictureMappingType() {
        return new PictureMappingType();
    }

    /**
     * Create an instance of {@link TitleType }
     * 
     */
    public TitleType createTitleType() {
        return new TitleType();
    }

    /**
     * Create an instance of {@link ConditionType }
     * 
     */
    public ConditionType createConditionType() {
        return new ConditionType();
    }

    /**
     * Create an instance of {@link ErrorTypeType }
     * 
     */
    public ErrorTypeType createErrorTypeType() {
        return new ErrorTypeType();
    }

    /**
     * Create an instance of {@link ErrorLocationType }
     * 
     */
    public ErrorLocationType createErrorLocationType() {
        return new ErrorLocationType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EFACodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/EFACode", name = "EFACode")
    public JAXBElement<EFACodeType> createEFACode(EFACodeType value) {
        return new JAXBElement<EFACodeType>(_EFACode_QNAME, EFACodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EFACodeIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/EFACode", name = "EFACodeRef")
    public JAXBElement<EFACodeIdentifierType> createEFACodeRef(EFACodeIdentifierType value) {
        return new JAXBElement<EFACodeIdentifierType>(_EFACodeRef_QNAME, EFACodeIdentifierType.class, null, value);
    }

}


package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für OrganizationalDataType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OrganizationalDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Brand" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Wholesaler" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}OwningWholesalerType" minOccurs="0"/>
 *         &lt;element name="Dealer" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}OwningDealerType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganizationalDataType", propOrder = {
    "brand",
    "wholesaler",
    "dealer"
})
public class OrganizationalDataType {

    @XmlElement(name = "Brand")
    protected CodeType brand;
    @XmlElement(name = "Wholesaler")
    protected OwningWholesalerType wholesaler;
    @XmlElement(name = "Dealer")
    protected OwningDealerType dealer;

    /**
     * Ruft den Wert der brand-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBrand() {
        return brand;
    }

    /**
     * Legt den Wert der brand-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBrand(CodeType value) {
        this.brand = value;
    }

    /**
     * Ruft den Wert der wholesaler-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OwningWholesalerType }
     *     
     */
    public OwningWholesalerType getWholesaler() {
        return wholesaler;
    }

    /**
     * Legt den Wert der wholesaler-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OwningWholesalerType }
     *     
     */
    public void setWholesaler(OwningWholesalerType value) {
        this.wholesaler = value;
    }

    /**
     * Ruft den Wert der dealer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OwningDealerType }
     *     
     */
    public OwningDealerType getDealer() {
        return dealer;
    }

    /**
     * Legt den Wert der dealer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OwningDealerType }
     *     
     */
    public void setDealer(OwningDealerType value) {
        this.dealer = value;
    }

}

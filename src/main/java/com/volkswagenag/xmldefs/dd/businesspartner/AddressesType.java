
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * List of addresses.
 * 
 * <p>Java-Klasse für AddressesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AddressesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Address" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AddressType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressesType", propOrder = {
    "address"
})



public class AddressesType {
	
	
    @XmlElement(name = "Address", required = true)
    private List<AddressType> address;


		public void setAddressType(List<AddressType> address) {
			this.address = address;
		}
		
	    public List<AddressType> getAddress() {
	        if (address == null) {
	            address = new ArrayList<AddressType>();
	        }
	        return this.address;
	    }

}

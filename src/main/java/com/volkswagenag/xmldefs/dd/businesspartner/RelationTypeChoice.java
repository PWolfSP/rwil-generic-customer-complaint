
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für RelationTypeChoice complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RelationTypeChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RelationType" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}RelationTypeChoiceCodelist"/>
 *         &lt;element name="ContactPerson" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ContactPersonType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationTypeChoice", propOrder = {
    "relationType",
    "contactPerson"
})
public class RelationTypeChoice {

    @XmlElement(name = "RelationType", required = true)
    @XmlSchemaType(name = "string")
    protected RelationTypeChoiceCodelist relationType;
    @XmlElement(name = "ContactPerson")
    protected ContactPersonType contactPerson;

    /**
     * Ruft den Wert der relationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RelationTypeChoiceCodelist }
     *     
     */
    public RelationTypeChoiceCodelist getRelationType() {
        return relationType;
    }

    /**
     * Legt den Wert der relationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationTypeChoiceCodelist }
     *     
     */
    public void setRelationType(RelationTypeChoiceCodelist value) {
        this.relationType = value;
    }

    /**
     * Ruft den Wert der contactPerson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ContactPersonType }
     *     
     */
    public ContactPersonType getContactPerson() {
        return contactPerson;
    }

    /**
     * Legt den Wert der contactPerson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPersonType }
     *     
     */
    public void setContactPerson(ContactPersonType value) {
        this.contactPerson = value;
    }

}

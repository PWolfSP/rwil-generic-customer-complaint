
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * Customer allows to be contacted for marketing reasons
 * 
 * <p>Java-Klasse f�r MarketingAllowanceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MarketingAllowanceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BrandCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="GeneralMarketingAllowanceIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="LastChangeDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="LastChangeBy" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}LastChangeByType" minOccurs="0"/>
 *         &lt;element name="InboundChannel" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="MarketingBlockades" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MarketingBlockadesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingAllowanceType", propOrder = {
    "brandCode",
    "generalMarketingAllowanceIND",
    "lastChangeDate",
    "lastChangeBy",
    "inboundChannel",
    "marketingBlockades"
})
public class MarketingAllowanceType {

    @XmlElement(name = "BrandCode")
    protected CodeType brandCode;
    @XmlElement(name = "GeneralMarketingAllowanceIND")
    protected Boolean generalMarketingAllowanceIND;
    @XmlElement(name = "LastChangeDate")
    protected String lastChangeDate;
    @XmlElement(name = "LastChangeBy")
    protected LastChangeByType lastChangeBy;
    @XmlElement(name = "InboundChannel")
    protected CodeType inboundChannel;
    @XmlElement(name = "MarketingBlockades")
    protected MarketingBlockadesType marketingBlockades;

    /**
     * Ruft den Wert der brandCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBrandCode() {
        return brandCode;
    }

    /**
     * Legt den Wert der brandCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBrandCode(CodeType value) {
        this.brandCode = value;
    }

    /**
     * Ruft den Wert der generalMarketingAllowanceIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGeneralMarketingAllowanceIND() {
        return generalMarketingAllowanceIND;
    }

    /**
     * Legt den Wert der generalMarketingAllowanceIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGeneralMarketingAllowanceIND(Boolean value) {
        this.generalMarketingAllowanceIND = value;
    }

    /**
     * Ruft den Wert der lastChangeDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastChangeDate() {
        return lastChangeDate;
    }

    /**
     * Legt den Wert der lastChangeDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastChangeDate(String value) {
        this.lastChangeDate = value;
    }

    /**
     * Ruft den Wert der lastChangeBy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LastChangeByType }
     *     
     */
    public LastChangeByType getLastChangeBy() {
        return lastChangeBy;
    }

    /**
     * Legt den Wert der lastChangeBy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LastChangeByType }
     *     
     */
    public void setLastChangeBy(LastChangeByType value) {
        this.lastChangeBy = value;
    }

    /**
     * Ruft den Wert der inboundChannel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getInboundChannel() {
        return inboundChannel;
    }

    /**
     * Legt den Wert der inboundChannel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setInboundChannel(CodeType value) {
        this.inboundChannel = value;
    }

    /**
     * Ruft den Wert der marketingBlockades-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MarketingBlockadesType }
     *     
     */
    public MarketingBlockadesType getMarketingBlockades() {
        return marketingBlockades;
    }

    /**
     * Legt den Wert der marketingBlockades-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketingBlockadesType }
     *     
     */
    public void setMarketingBlockades(MarketingBlockadesType value) {
        this.marketingBlockades = value;
    }

}

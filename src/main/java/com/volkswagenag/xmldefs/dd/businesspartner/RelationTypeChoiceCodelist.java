
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für RelationTypeChoiceCodelist.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="RelationTypeChoiceCodelist">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ContactPerson"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@Generated
@XmlType(name = "RelationTypeChoiceCodelist")
@XmlEnum
public enum RelationTypeChoiceCodelist {

    @XmlEnumValue("ContactPerson")
    CONTACT_PERSON("ContactPerson");
    private final String value;

    RelationTypeChoiceCodelist(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RelationTypeChoiceCodelist fromValue(String v) {
        for (RelationTypeChoiceCodelist c: RelationTypeChoiceCodelist.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

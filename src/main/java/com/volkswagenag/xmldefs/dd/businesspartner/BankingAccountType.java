
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.volkswagenag.xmldefs.dd.basictypes.CountryCodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * Bank Account
 * 
 * <p>Java-Klasse f�r BankingAccountType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BankingAccountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankingAccountID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="IsStandardBankAccountIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="HolderName" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="AccountNumber" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="BankIdentificationCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="BankName" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="BankCountry" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="BankControlKey" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="IBAN" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="IBANValidFrom" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="SWIFTCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="DirectDebitAuthorizationIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="Usages" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BankAccountUsagesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankingAccountType", propOrder = {
    "bankingAccountID",
    "isStandardBankAccountIND",
    "holderName",
    "accountNumber",
    "bankIdentificationCode",
    "bankName",
    "bankCountry",
    "bankControlKey",
    "iban",
    "ibanValidFrom",
    "swiftCode",
    "directDebitAuthorizationIND",
    "usages"
})
public class BankingAccountType {

    @XmlElement(name = "BankingAccountID")
    protected IdentifierType bankingAccountID;
    @XmlElement(name = "IsStandardBankAccountIND")
    protected Boolean isStandardBankAccountIND;
    @XmlElement(name = "HolderName")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String holderName;
    @XmlElement(name = "AccountNumber")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String accountNumber;
    @XmlElement(name = "BankIdentificationCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String bankIdentificationCode;
    @XmlElement(name = "BankName")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String bankName;
    @XmlElement(name = "BankCountry")
    protected CountryCodeType bankCountry;
    @XmlElement(name = "BankControlKey")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String bankControlKey;
    @XmlElement(name = "IBAN")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String iban;
    @XmlElement(name = "IBANValidFrom")
    protected String ibanValidFrom;
    @XmlElement(name = "SWIFTCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String swiftCode;
    @XmlElement(name = "DirectDebitAuthorizationIND")
    protected Boolean directDebitAuthorizationIND;
    @XmlElement(name = "Usages")
    protected BankAccountUsagesType usages;

    /**
     * Ruft den Wert der bankingAccountID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getBankingAccountID() {
        return bankingAccountID;
    }

    /**
     * Legt den Wert der bankingAccountID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setBankingAccountID(IdentifierType value) {
        this.bankingAccountID = value;
    }

    /**
     * Ruft den Wert der isStandardBankAccountIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsStandardBankAccountIND() {
        return isStandardBankAccountIND;
    }

    /**
     * Legt den Wert der isStandardBankAccountIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsStandardBankAccountIND(Boolean value) {
        this.isStandardBankAccountIND = value;
    }

    /**
     * Ruft den Wert der holderName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHolderName() {
        return holderName;
    }

    /**
     * Legt den Wert der holderName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHolderName(String value) {
        this.holderName = value;
    }

    /**
     * Ruft den Wert der accountNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Legt den Wert der accountNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Ruft den Wert der bankIdentificationCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankIdentificationCode() {
        return bankIdentificationCode;
    }

    /**
     * Legt den Wert der bankIdentificationCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankIdentificationCode(String value) {
        this.bankIdentificationCode = value;
    }

    /**
     * Ruft den Wert der bankName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Legt den Wert der bankName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankName(String value) {
        this.bankName = value;
    }

    /**
     * Ruft den Wert der bankCountry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getBankCountry() {
        return bankCountry;
    }

    /**
     * Legt den Wert der bankCountry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setBankCountry(CountryCodeType value) {
        this.bankCountry = value;
    }

    /**
     * Ruft den Wert der bankControlKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankControlKey() {
        return bankControlKey;
    }

    /**
     * Legt den Wert der bankControlKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankControlKey(String value) {
        this.bankControlKey = value;
    }

    /**
     * Ruft den Wert der iban-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBAN() {
        return iban;
    }

    /**
     * Legt den Wert der iban-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBAN(String value) {
        this.iban = value;
    }

    /**
     * Ruft den Wert der ibanValidFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBANValidFrom() {
        return ibanValidFrom;
    }

    /**
     * Legt den Wert der ibanValidFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBANValidFrom(String value) {
        this.ibanValidFrom = value;
    }

    /**
     * Ruft den Wert der swiftCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSWIFTCode() {
        return swiftCode;
    }

    /**
     * Legt den Wert der swiftCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSWIFTCode(String value) {
        this.swiftCode = value;
    }

    /**
     * Ruft den Wert der directDebitAuthorizationIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectDebitAuthorizationIND() {
        return directDebitAuthorizationIND;
    }

    /**
     * Legt den Wert der directDebitAuthorizationIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectDebitAuthorizationIND(Boolean value) {
        this.directDebitAuthorizationIND = value;
    }

    /**
     * Ruft den Wert der usages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BankAccountUsagesType }
     *     
     */
    public BankAccountUsagesType getUsages() {
        return usages;
    }

    /**
     * Legt den Wert der usages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BankAccountUsagesType }
     *     
     */
    public void setUsages(BankAccountUsagesType value) {
        this.usages = value;
    }

}

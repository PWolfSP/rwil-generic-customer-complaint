
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * List of communication channels.
 * 
 * <p>Java-Klasse für CommunicationChannelsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommunicationChannelsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommunicationChannel" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}CommunicationChannelType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationChannelsType", propOrder = {
    "communicationChannel"
})



public class CommunicationChannelsType {
	
	
    @XmlElement(name = "CommunicationChannel", required = true)
    private List<CommunicationChannelType> communicationChannel;


		public void setCommunicationChannelType(List<CommunicationChannelType> communicationChannel) {
			this.communicationChannel = communicationChannel;
		}
		
	    public List<CommunicationChannelType> getCommunicationChannel() {
	        if (communicationChannel == null) {
	            communicationChannel = new ArrayList<CommunicationChannelType>();
	        }
	        return this.communicationChannel;
	    }

}


package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * TODO
 * 
 * <p>Java-Klasse für AdditionalNotesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AdditionalNotesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdditionalNote" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AdditionalNoteType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalNotesType", propOrder = {
    "additionalNote"
})



public class AdditionalNotesType {
	
	
    @XmlElement(name = "AdditionalNote", required = true)
    private List<AdditionalNoteType> additionalNote;


		public void setAdditionalNoteType(List<AdditionalNoteType> additionalNote) {
			this.additionalNote = additionalNote;
		}
		
	    public List<AdditionalNoteType> getAdditionalNote() {
	        if (additionalNote == null) {
	            additionalNote = new ArrayList<AdditionalNoteType>();
	        }
	        return this.additionalNote;
	    }

}

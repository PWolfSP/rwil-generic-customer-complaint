
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.CountryCodeType;

import lombok.Generated;


/**
 * Details about the person.
 * 
 * <p>Java-Klasse f�r PersonType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PersonType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Salutation" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PersonNameType" minOccurs="0"/>
 *         &lt;element name="Occupation" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Profession" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="IndustrySectors" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}IndustrialSectorsType" minOccurs="0"/>
 *         &lt;element name="FamilyStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DateOfBirth" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="YearOfBirth" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}YearType" minOccurs="0"/>
 *         &lt;element name="BirthPlace" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="CountryOfOrigin" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="Gender" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ChildrenCNT" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="DateOfDeath" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="DrivingLicence" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DrivingLicenceType" minOccurs="0"/>
 *         &lt;element name="SocialStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Languages" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}LanguagesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonType", propOrder = {
    "salutation",
    "name",
    "occupation",
    "profession",
    "industrySectors",
    "familyStatus",
    "dateOfBirth",
    "yearOfBirth",
    "birthPlace",
    "countryOfOrigin",
    "gender",
    "childrenCNT",
    "nationality",
    "dateOfDeath",
    "drivingLicence",
    "socialStatus",
    "languages"
})
public class PersonType {

    @XmlElement(name = "Salutation")
    protected CodeType salutation;
    @XmlElement(name = "Name")
    protected PersonNameType name;
    @XmlElement(name = "Occupation")
    protected CodeType occupation;
    @XmlElement(name = "Profession")
    protected CodeType profession;
    @XmlElement(name = "IndustrySectors")
    protected IndustrialSectorsType industrySectors;
    @XmlElement(name = "FamilyStatus")
    protected CodeType familyStatus;
    @XmlElement(name = "DateOfBirth")
    protected String dateOfBirth;
    @XmlElement(name = "YearOfBirth")
    protected String yearOfBirth;
    @XmlElement(name = "BirthPlace")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String birthPlace;
    @XmlElement(name = "CountryOfOrigin")
    protected CountryCodeType countryOfOrigin;
    @XmlElement(name = "Gender")
    protected CodeType gender;
    @XmlElement(name = "ChildrenCNT")
    protected BigDecimal childrenCNT;
    @XmlElement(name = "Nationality")
    protected CountryCodeType nationality;
    @XmlElement(name = "DateOfDeath")
    protected String dateOfDeath;
    @XmlElement(name = "DrivingLicence")
    protected DrivingLicenceType drivingLicence;
    @XmlElement(name = "SocialStatus")
    protected CodeType socialStatus;
    @XmlElement(name = "Languages")
    protected LanguagesType languages;

    /**
     * Ruft den Wert der salutation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getSalutation() {
        return salutation;
    }

    /**
     * Legt den Wert der salutation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setSalutation(CodeType value) {
        this.salutation = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameType }
     *     
     */
    public PersonNameType getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameType }
     *     
     */
    public void setName(PersonNameType value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der occupation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOccupation() {
        return occupation;
    }

    /**
     * Legt den Wert der occupation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOccupation(CodeType value) {
        this.occupation = value;
    }

    /**
     * Ruft den Wert der profession-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getProfession() {
        return profession;
    }

    /**
     * Legt den Wert der profession-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setProfession(CodeType value) {
        this.profession = value;
    }

    /**
     * Ruft den Wert der industrySectors-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IndustrialSectorsType }
     *     
     */
    public IndustrialSectorsType getIndustrySectors() {
        return industrySectors;
    }

    /**
     * Legt den Wert der industrySectors-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IndustrialSectorsType }
     *     
     */
    public void setIndustrySectors(IndustrialSectorsType value) {
        this.industrySectors = value;
    }

    /**
     * Ruft den Wert der familyStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getFamilyStatus() {
        return familyStatus;
    }

    /**
     * Legt den Wert der familyStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setFamilyStatus(CodeType value) {
        this.familyStatus = value;
    }

    /**
     * Ruft den Wert der dateOfBirth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Legt den Wert der dateOfBirth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfBirth(String value) {
        this.dateOfBirth = value;
    }

    /**
     * Ruft den Wert der yearOfBirth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYearOfBirth() {
        return yearOfBirth;
    }

    /**
     * Legt den Wert der yearOfBirth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYearOfBirth(String value) {
        this.yearOfBirth = value;
    }

    /**
     * Ruft den Wert der birthPlace-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthPlace() {
        return birthPlace;
    }

    /**
     * Legt den Wert der birthPlace-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthPlace(String value) {
        this.birthPlace = value;
    }

    /**
     * Ruft den Wert der countryOfOrigin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getCountryOfOrigin() {
        return countryOfOrigin;
    }

    /**
     * Legt den Wert der countryOfOrigin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setCountryOfOrigin(CountryCodeType value) {
        this.countryOfOrigin = value;
    }

    /**
     * Ruft den Wert der gender-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getGender() {
        return gender;
    }

    /**
     * Legt den Wert der gender-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setGender(CodeType value) {
        this.gender = value;
    }

    /**
     * Ruft den Wert der childrenCNT-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChildrenCNT() {
        return childrenCNT;
    }

    /**
     * Legt den Wert der childrenCNT-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChildrenCNT(BigDecimal value) {
        this.childrenCNT = value;
    }

    /**
     * Ruft den Wert der nationality-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getNationality() {
        return nationality;
    }

    /**
     * Legt den Wert der nationality-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setNationality(CountryCodeType value) {
        this.nationality = value;
    }

    /**
     * Ruft den Wert der dateOfDeath-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfDeath() {
        return dateOfDeath;
    }

    /**
     * Legt den Wert der dateOfDeath-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfDeath(String value) {
        this.dateOfDeath = value;
    }

    /**
     * Ruft den Wert der drivingLicence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DrivingLicenceType }
     *     
     */
    public DrivingLicenceType getDrivingLicence() {
        return drivingLicence;
    }

    /**
     * Legt den Wert der drivingLicence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DrivingLicenceType }
     *     
     */
    public void setDrivingLicence(DrivingLicenceType value) {
        this.drivingLicence = value;
    }

    /**
     * Ruft den Wert der socialStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getSocialStatus() {
        return socialStatus;
    }

    /**
     * Legt den Wert der socialStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setSocialStatus(CodeType value) {
        this.socialStatus = value;
    }

    /**
     * Ruft den Wert der languages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LanguagesType }
     *     
     */
    public LanguagesType getLanguages() {
        return languages;
    }

    /**
     * Legt den Wert der languages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LanguagesType }
     *     
     */
    public void setLanguages(LanguagesType value) {
        this.languages = value;
    }

}


package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ProcessingInformationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProcessingInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Timestamps" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}TimestampsType" minOccurs="0"/>
 *         &lt;element name="LastChangeBy" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}LastChangeByType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessingInformationType", propOrder = {
    "timestamps",
    "lastChangeBy"
})
public class ProcessingInformationType {

    @XmlElement(name = "Timestamps")
    protected TimestampsType timestamps;
    @XmlElement(name = "LastChangeBy")
    protected LastChangeByType lastChangeBy;

    /**
     * Ruft den Wert der timestamps-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimestampsType }
     *     
     */
    public TimestampsType getTimestamps() {
        return timestamps;
    }

    /**
     * Legt den Wert der timestamps-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimestampsType }
     *     
     */
    public void setTimestamps(TimestampsType value) {
        this.timestamps = value;
    }

    /**
     * Ruft den Wert der lastChangeBy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LastChangeByType }
     *     
     */
    public LastChangeByType getLastChangeBy() {
        return lastChangeBy;
    }

    /**
     * Legt den Wert der lastChangeBy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LastChangeByType }
     *     
     */
    public void setLastChangeBy(LastChangeByType value) {
        this.lastChangeBy = value;
    }

}

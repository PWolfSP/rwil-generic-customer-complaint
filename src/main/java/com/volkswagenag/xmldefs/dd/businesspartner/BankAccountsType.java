
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * List of bank accounts.
 * 
 * <p>Java-Klasse für BankAccountsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BankAccountsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankingAccount" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BankingAccountType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankAccountsType", propOrder = {
    "bankingAccount"
})



public class BankAccountsType {
	
	
    @XmlElement(name = "BankingAccount", required = true)
    private List<BankingAccountType> bankingAccount;


		public void setBankingAccountType(List<BankingAccountType> bankingAccount) {
			this.bankingAccount = bankingAccount;
		}
		
	    public List<BankingAccountType> getBankingAccount() {
	        if (bankingAccount == null) {
	            bankingAccount = new ArrayList<BankingAccountType>();
	        }
	        return this.bankingAccount;
	    }

}

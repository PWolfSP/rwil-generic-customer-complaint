
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für FleetCustomerDetailsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FleetCustomerDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LifecycleStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Importances" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ImportancesType" minOccurs="0"/>
 *         &lt;element name="MarketingImportance" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="CarParkSizes" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}CarParkSizesType" minOccurs="0"/>
 *         &lt;element name="BlockType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="FleetCompanyType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FleetCustomerDetailsType", propOrder = {
    "lifecycleStatus",
    "importances",
    "marketingImportance",
    "carParkSizes",
    "blockType",
    "fleetCompanyType"
})
public class FleetCustomerDetailsType {

    @XmlElement(name = "LifecycleStatus")
    protected CodeType lifecycleStatus;
    @XmlElement(name = "Importances")
    protected ImportancesType importances;
    @XmlElement(name = "MarketingImportance")
    protected CodeType marketingImportance;
    @XmlElement(name = "CarParkSizes")
    protected CarParkSizesType carParkSizes;
    @XmlElement(name = "BlockType")
    protected CodeType blockType;
    @XmlElement(name = "FleetCompanyType")
    protected CodeType fleetCompanyType;

    /**
     * Ruft den Wert der lifecycleStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getLifecycleStatus() {
        return lifecycleStatus;
    }

    /**
     * Legt den Wert der lifecycleStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setLifecycleStatus(CodeType value) {
        this.lifecycleStatus = value;
    }

    /**
     * Ruft den Wert der importances-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ImportancesType }
     *     
     */
    public ImportancesType getImportances() {
        return importances;
    }

    /**
     * Legt den Wert der importances-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ImportancesType }
     *     
     */
    public void setImportances(ImportancesType value) {
        this.importances = value;
    }

    /**
     * Ruft den Wert der marketingImportance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getMarketingImportance() {
        return marketingImportance;
    }

    /**
     * Legt den Wert der marketingImportance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setMarketingImportance(CodeType value) {
        this.marketingImportance = value;
    }

    /**
     * Ruft den Wert der carParkSizes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CarParkSizesType }
     *     
     */
    public CarParkSizesType getCarParkSizes() {
        return carParkSizes;
    }

    /**
     * Legt den Wert der carParkSizes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CarParkSizesType }
     *     
     */
    public void setCarParkSizes(CarParkSizesType value) {
        this.carParkSizes = value;
    }

    /**
     * Ruft den Wert der blockType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBlockType() {
        return blockType;
    }

    /**
     * Legt den Wert der blockType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBlockType(CodeType value) {
        this.blockType = value;
    }

    /**
     * Ruft den Wert der fleetCompanyType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getFleetCompanyType() {
        return fleetCompanyType;
    }

    /**
     * Legt den Wert der fleetCompanyType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setFleetCompanyType(CodeType value) {
        this.fleetCompanyType = value;
    }

}

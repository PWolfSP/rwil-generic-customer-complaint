
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * DealerAuth Marke Betriebsnummer BasisStatus (alle von-bis, freigegeben ab, am, von)
 *           NebenStatus (0..n)
 * 
 * <p>Java-Klasse für DealerSalesAuthorisationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DealerSalesAuthorisationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Brand" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="OrganisationNumber" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="BaseStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="BaseStatusValidityRange" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ValidityRangeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealerSalesAuthorisationType", propOrder = {
    "brand",
    "organisationNumber",
    "baseStatus",
    "baseStatusValidityRange"
})
public class DealerSalesAuthorisationType {

    @XmlElement(name = "Brand")
    protected CodeType brand;
    @XmlElement(name = "OrganisationNumber")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String organisationNumber;
    @XmlElement(name = "BaseStatus")
    protected CodeType baseStatus;
    @XmlElement(name = "BaseStatusValidityRange")
    protected ValidityRangeType baseStatusValidityRange;

    /**
     * Ruft den Wert der brand-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBrand() {
        return brand;
    }

    /**
     * Legt den Wert der brand-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBrand(CodeType value) {
        this.brand = value;
    }

    /**
     * Ruft den Wert der organisationNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganisationNumber() {
        return organisationNumber;
    }

    /**
     * Legt den Wert der organisationNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganisationNumber(String value) {
        this.organisationNumber = value;
    }

    /**
     * Ruft den Wert der baseStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBaseStatus() {
        return baseStatus;
    }

    /**
     * Legt den Wert der baseStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBaseStatus(CodeType value) {
        this.baseStatus = value;
    }

    /**
     * Ruft den Wert der baseStatusValidityRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValidityRangeType }
     *     
     */
    public ValidityRangeType getBaseStatusValidityRange() {
        return baseStatusValidityRange;
    }

    /**
     * Legt den Wert der baseStatusValidityRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidityRangeType }
     *     
     */
    public void setBaseStatusValidityRange(ValidityRangeType value) {
        this.baseStatusValidityRange = value;
    }

}


package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * Details about the driving license.
 * 
 * <p>Java-Klasse f�r DrivingLicenceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DrivingLicenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LicenseNumber" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="AuthorityIssuing" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AuthorityIssuingType" minOccurs="0"/>
 *         &lt;element name="Classes" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DrivingLicenceClassesType" minOccurs="0"/>
 *         &lt;element name="ProbationEnd" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="ExistingCopy" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="ValidFrom" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="ValidTo" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DrivingLicenceType", propOrder = {
    "licenseNumber",
    "authorityIssuing",
    "classes",
    "probationEnd",
    "existingCopy",
    "validFrom",
    "validTo"
})
public class DrivingLicenceType {

    @XmlElement(name = "LicenseNumber")
    protected IdentifierType licenseNumber;
    @XmlElement(name = "AuthorityIssuing")
    protected AuthorityIssuingType authorityIssuing;
    @XmlElement(name = "Classes")
    protected DrivingLicenceClassesType classes;
    @XmlElement(name = "ProbationEnd")
    protected String probationEnd;
    @XmlElement(name = "ExistingCopy")
    protected Boolean existingCopy;
    @XmlElement(name = "ValidFrom")
    protected String validFrom;
    @XmlElement(name = "ValidTo")
    protected String validTo;

    /**
     * Ruft den Wert der licenseNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * Legt den Wert der licenseNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setLicenseNumber(IdentifierType value) {
        this.licenseNumber = value;
    }

    /**
     * Ruft den Wert der authorityIssuing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorityIssuingType }
     *     
     */
    public AuthorityIssuingType getAuthorityIssuing() {
        return authorityIssuing;
    }

    /**
     * Legt den Wert der authorityIssuing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorityIssuingType }
     *     
     */
    public void setAuthorityIssuing(AuthorityIssuingType value) {
        this.authorityIssuing = value;
    }

    /**
     * Ruft den Wert der classes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DrivingLicenceClassesType }
     *     
     */
    public DrivingLicenceClassesType getClasses() {
        return classes;
    }

    /**
     * Legt den Wert der classes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DrivingLicenceClassesType }
     *     
     */
    public void setClasses(DrivingLicenceClassesType value) {
        this.classes = value;
    }

    /**
     * Ruft den Wert der probationEnd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProbationEnd() {
        return probationEnd;
    }

    /**
     * Legt den Wert der probationEnd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProbationEnd(String value) {
        this.probationEnd = value;
    }

    /**
     * Ruft den Wert der existingCopy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExistingCopy() {
        return existingCopy;
    }

    /**
     * Legt den Wert der existingCopy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExistingCopy(Boolean value) {
        this.existingCopy = value;
    }

    /**
     * Ruft den Wert der validFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidFrom() {
        return validFrom;
    }

    /**
     * Legt den Wert der validFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidFrom(String value) {
        this.validFrom = value;
    }

    /**
     * Ruft den Wert der validTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidTo() {
        return validTo;
    }

    /**
     * Legt den Wert der validTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidTo(String value) {
        this.validTo = value;
    }

}

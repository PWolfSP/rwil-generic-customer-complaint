
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AddressKindChoiceCodelist.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="AddressKindChoiceCodelist">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PostBoxAddress"/>
 *     &lt;enumeration value="StreetAddress"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@Generated
@XmlType(name = "AddressKindChoiceCodelist")
@XmlEnum
public enum AddressKindChoiceCodelist {

    @XmlEnumValue("PostBoxAddress")
    POST_BOX_ADDRESS("PostBoxAddress"),
    @XmlEnumValue("StreetAddress")
    STREET_ADDRESS("StreetAddress");
    private final String value;

    AddressKindChoiceCodelist(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AddressKindChoiceCodelist fromValue(String v) {
        for (AddressKindChoiceCodelist c: AddressKindChoiceCodelist.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

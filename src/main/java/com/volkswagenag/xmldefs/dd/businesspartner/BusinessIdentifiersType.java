
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für BusinessIdentifiersType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BusinessIdentifiersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BusinessIdentifier" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BusinessIdentifierType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessIdentifiersType", propOrder = {
    "businessIdentifier"
})



public class BusinessIdentifiersType {
	
	
    @XmlElement(name = "BusinessIdentifier", required = true)
    private List<BusinessIdentifierType> businessIdentifier;


		public void setBusinessIdentifierType(List<BusinessIdentifierType> businessIdentifier) {
			this.businessIdentifier = businessIdentifier;
		}
		
	    public List<BusinessIdentifierType> getBusinessIdentifier() {
	        if (businessIdentifier == null) {
	            businessIdentifier = new ArrayList<BusinessIdentifierType>();
	        }
	        return this.businessIdentifier;
	    }

}

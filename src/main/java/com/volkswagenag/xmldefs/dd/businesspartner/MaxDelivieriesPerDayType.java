
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Max. deliveries per day.
 * 
 * <p>Java-Klasse für MaxDelivieriesPerDayType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MaxDelivieriesPerDayType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MaxDeliveryPerDay" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MaxDelivieryPerDayType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaxDelivieriesPerDayType", propOrder = {
    "maxDeliveryPerDay"
})



public class MaxDelivieriesPerDayType {
	
	
    @XmlElement(name = "MaxDeliveryPerDay", required = true)
    private List<MaxDelivieryPerDayType> maxDeliveryPerDay;


		public void setMaxDelivieryPerDayType(List<MaxDelivieryPerDayType> maxDeliveryPerDay) {
			this.maxDeliveryPerDay = maxDeliveryPerDay;
		}
		
	    public List<MaxDelivieryPerDayType> getMaxDeliveryPerDay() {
	        if (maxDeliveryPerDay == null) {
	            maxDeliveryPerDay = new ArrayList<MaxDelivieryPerDayType>();
	        }
	        return this.maxDeliveryPerDay;
	    }

}


package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für MarketingBlockadesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MarketingBlockadesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MarketingBlockade" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MarketingBlockadeType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingBlockadesType", propOrder = {
    "marketingBlockade"
})



public class MarketingBlockadesType {
	
	
    @XmlElement(name = "MarketingBlockade", required = true)
    private List<MarketingBlockadeType> marketingBlockade;


		public void setMarketingBlockadeType(List<MarketingBlockadeType> marketingBlockade) {
			this.marketingBlockade = marketingBlockade;
		}
		
	    public List<MarketingBlockadeType> getMarketingBlockade() {
	        if (marketingBlockade == null) {
	            marketingBlockade = new ArrayList<MarketingBlockadeType>();
	        }
	        return this.marketingBlockade;
	    }

}

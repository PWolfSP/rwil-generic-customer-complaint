
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * List of address usages.
 * 
 * <p>Java-Klasse für AddressUsagesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AddressUsagesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Usage" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AddressUsageType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressUsagesType", propOrder = {
    "usage"
})



public class AddressUsagesType {
	
	
    @XmlElement(name = "Usage", required = true)
    private List<AddressUsageType> usage;


		public void setAddressUsageType(List<AddressUsageType> usage) {
			this.usage = usage;
		}
		
	    public List<AddressUsageType> getUsage() {
	        if (usage == null) {
	            usage = new ArrayList<AddressUsageType>();
	        }
	        return this.usage;
	    }

}

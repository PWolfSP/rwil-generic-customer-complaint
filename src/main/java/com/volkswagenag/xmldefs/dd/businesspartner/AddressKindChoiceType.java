
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AddressKindChoiceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AddressKindChoiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddressKind" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AddressKindChoiceCodelist"/>
 *         &lt;element name="StreetAddress" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}StreetAddressType" minOccurs="0"/>
 *         &lt;element name="PostBoxAddress" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PostBoxAddressType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressKindChoiceType", propOrder = {
    "addressKind",
    "streetAddress",
    "postBoxAddress"
})
public class AddressKindChoiceType {

    @XmlElement(name = "AddressKind", required = true)
    @XmlSchemaType(name = "string")
    protected AddressKindChoiceCodelist addressKind;
    @XmlElement(name = "StreetAddress")
    protected StreetAddressType streetAddress;
    @XmlElement(name = "PostBoxAddress")
    protected PostBoxAddressType postBoxAddress;

    /**
     * Ruft den Wert der addressKind-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AddressKindChoiceCodelist }
     *     
     */
    public AddressKindChoiceCodelist getAddressKind() {
        return addressKind;
    }

    /**
     * Legt den Wert der addressKind-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressKindChoiceCodelist }
     *     
     */
    public void setAddressKind(AddressKindChoiceCodelist value) {
        this.addressKind = value;
    }

    /**
     * Ruft den Wert der streetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddressType }
     *     
     */
    public StreetAddressType getStreetAddress() {
        return streetAddress;
    }

    /**
     * Legt den Wert der streetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddressType }
     *     
     */
    public void setStreetAddress(StreetAddressType value) {
        this.streetAddress = value;
    }

    /**
     * Ruft den Wert der postBoxAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PostBoxAddressType }
     *     
     */
    public PostBoxAddressType getPostBoxAddress() {
        return postBoxAddress;
    }

    /**
     * Legt den Wert der postBoxAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PostBoxAddressType }
     *     
     */
    public void setPostBoxAddress(PostBoxAddressType value) {
        this.postBoxAddress = value;
    }

}

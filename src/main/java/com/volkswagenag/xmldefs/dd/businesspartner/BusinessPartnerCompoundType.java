
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Business partner. Comprises all properties of the a general partner and its
 *           specializations.
 * 
 * <p>Java-Klasse für BusinessPartnerCompoundType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BusinessPartnerCompoundType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartnerIdentifier" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerIdentifierType" minOccurs="0"/>
 *         &lt;element name="Partner" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerType" minOccurs="0"/>
 *         &lt;element name="CustomerDetails" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}CustomerDetailsType" minOccurs="0"/>
 *         &lt;element name="DealerDetails" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DealerDetailsType" minOccurs="0"/>
 *         &lt;element name="SalesPersonDetails" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}SalesPersonDetailsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessPartnerCompoundType", propOrder = {
    "partnerIdentifier",
    "partner",
    "customerDetails",
    "dealerDetails",
    "salesPersonDetails"
})
public class BusinessPartnerCompoundType {

    @XmlElement(name = "PartnerIdentifier")
    protected PartnerIdentifierType partnerIdentifier;
    @XmlElement(name = "Partner")
    protected PartnerType partner;
    @XmlElement(name = "CustomerDetails")
    protected CustomerDetailsType customerDetails;
    @XmlElement(name = "DealerDetails")
    protected DealerDetailsType dealerDetails;
    @XmlElement(name = "SalesPersonDetails")
    protected SalesPersonDetailsType salesPersonDetails;

    /**
     * Ruft den Wert der partnerIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getPartnerIdentifier() {
        return partnerIdentifier;
    }

    /**
     * Legt den Wert der partnerIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setPartnerIdentifier(PartnerIdentifierType value) {
        this.partnerIdentifier = value;
    }

    /**
     * Ruft den Wert der partner-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerType }
     *     
     */
    public PartnerType getPartner() {
        return partner;
    }

    /**
     * Legt den Wert der partner-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerType }
     *     
     */
    public void setPartner(PartnerType value) {
        this.partner = value;
    }

    /**
     * Ruft den Wert der customerDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CustomerDetailsType }
     *     
     */
    public CustomerDetailsType getCustomerDetails() {
        return customerDetails;
    }

    /**
     * Legt den Wert der customerDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerDetailsType }
     *     
     */
    public void setCustomerDetails(CustomerDetailsType value) {
        this.customerDetails = value;
    }

    /**
     * Ruft den Wert der dealerDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DealerDetailsType }
     *     
     */
    public DealerDetailsType getDealerDetails() {
        return dealerDetails;
    }

    /**
     * Legt den Wert der dealerDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DealerDetailsType }
     *     
     */
    public void setDealerDetails(DealerDetailsType value) {
        this.dealerDetails = value;
    }

    /**
     * Ruft den Wert der salesPersonDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SalesPersonDetailsType }
     *     
     */
    public SalesPersonDetailsType getSalesPersonDetails() {
        return salesPersonDetails;
    }

    /**
     * Legt den Wert der salesPersonDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesPersonDetailsType }
     *     
     */
    public void setSalesPersonDetails(SalesPersonDetailsType value) {
        this.salesPersonDetails = value;
    }

}


package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * An arbitrary Address (street or post box).
 * 
 * <p>Java-Klasse f�r AddressType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AddressType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddressID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="IsStandardAddressIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="AddressKindChoice" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AddressKindChoiceType" minOccurs="0"/>
 *         &lt;element name="Usages" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AddressUsagesType" minOccurs="0"/>
 *         &lt;element name="UndeliverabilityStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ValidationStatus" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ValidationStatusType" minOccurs="0"/>
 *         &lt;element name="AreaCodes" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ValidFrom" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="ValidTo" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressType", propOrder = {
    "addressID",
    "isStandardAddressIND",
    "addressKindChoice",
    "usages",
    "undeliverabilityStatus",
    "validationStatus",
    "areaCodes",
    "validFrom",
    "validTo"
})
public class AddressType {

    @XmlElement(name = "AddressID")
    protected IdentifierType addressID;
    @XmlElement(name = "IsStandardAddressIND")
    protected Boolean isStandardAddressIND;
    @XmlElement(name = "AddressKindChoice")
    protected AddressKindChoiceType addressKindChoice;
    @XmlElement(name = "Usages")
    protected AddressUsagesType usages;
    @XmlElement(name = "UndeliverabilityStatus")
    protected CodeType undeliverabilityStatus;
    @XmlElement(name = "ValidationStatus")
    protected ValidationStatusType validationStatus;
    @XmlElement(name = "AreaCodes")
    protected CodeType areaCodes;
    @XmlElement(name = "ValidFrom")
    protected String validFrom;
    @XmlElement(name = "ValidTo")
    protected String validTo;

    /**
     * Ruft den Wert der addressID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getAddressID() {
        return addressID;
    }

    /**
     * Legt den Wert der addressID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setAddressID(IdentifierType value) {
        this.addressID = value;
    }

    /**
     * Ruft den Wert der isStandardAddressIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsStandardAddressIND() {
        return isStandardAddressIND;
    }

    /**
     * Legt den Wert der isStandardAddressIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsStandardAddressIND(Boolean value) {
        this.isStandardAddressIND = value;
    }

    /**
     * Ruft den Wert der addressKindChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AddressKindChoiceType }
     *     
     */
    public AddressKindChoiceType getAddressKindChoice() {
        return addressKindChoice;
    }

    /**
     * Legt den Wert der addressKindChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressKindChoiceType }
     *     
     */
    public void setAddressKindChoice(AddressKindChoiceType value) {
        this.addressKindChoice = value;
    }

    /**
     * Ruft den Wert der usages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AddressUsagesType }
     *     
     */
    public AddressUsagesType getUsages() {
        return usages;
    }

    /**
     * Legt den Wert der usages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressUsagesType }
     *     
     */
    public void setUsages(AddressUsagesType value) {
        this.usages = value;
    }

    /**
     * Ruft den Wert der undeliverabilityStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getUndeliverabilityStatus() {
        return undeliverabilityStatus;
    }

    /**
     * Legt den Wert der undeliverabilityStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setUndeliverabilityStatus(CodeType value) {
        this.undeliverabilityStatus = value;
    }

    /**
     * Ruft den Wert der validationStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValidationStatusType }
     *     
     */
    public ValidationStatusType getValidationStatus() {
        return validationStatus;
    }

    /**
     * Legt den Wert der validationStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidationStatusType }
     *     
     */
    public void setValidationStatus(ValidationStatusType value) {
        this.validationStatus = value;
    }

    /**
     * Ruft den Wert der areaCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getAreaCodes() {
        return areaCodes;
    }

    /**
     * Legt den Wert der areaCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setAreaCodes(CodeType value) {
        this.areaCodes = value;
    }

    /**
     * Ruft den Wert der validFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidFrom() {
        return validFrom;
    }

    /**
     * Legt den Wert der validFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidFrom(String value) {
        this.validFrom = value;
    }

    /**
     * Ruft den Wert der validTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidTo() {
        return validTo;
    }

    /**
     * Legt den Wert der validTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidTo(String value) {
        this.validTo = value;
    }

}

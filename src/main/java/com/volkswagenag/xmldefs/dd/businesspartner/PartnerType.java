
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * Partner.
 * 
 * <p>Java-Klasse für PartnerType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PartnerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartnerClassification" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Roles" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerRolesType" minOccurs="0"/>
 *         &lt;element name="PartnerKindChoice" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerKindChoiceType" minOccurs="0"/>
 *         &lt;element name="BusinessIdentifiers" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BusinessIdentifiersType" minOccurs="0"/>
 *         &lt;element name="Addresses" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AddressesType" minOccurs="0"/>
 *         &lt;element name="CommunicationChannels" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}CommunicationChannelsType" minOccurs="0"/>
 *         &lt;element name="BankingAccounts" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BankAccountsType" minOccurs="0"/>
 *         &lt;element name="PaymentCards" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PaymentCardsType" minOccurs="0"/>
 *         &lt;element name="TaxData" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}TaxDataType" minOccurs="0"/>
 *         &lt;element name="MarketingProfile" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MarketingProfileType" minOccurs="0"/>
 *         &lt;element name="InactiveStatus" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}InactiveStatusType" minOccurs="0"/>
 *         &lt;element name="ProcessingInformation" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ProcessingInformationType" minOccurs="0"/>
 *         &lt;element name="AdditionalNotes" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AdditionalNotesType" minOccurs="0"/>
 *         &lt;element name="OrganizationalData" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}OrganizationalDataType" minOccurs="0"/>
 *         &lt;element name="ControlData" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ControlDataType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerType", propOrder = {
    "partnerClassification",
    "roles",
    "partnerKindChoice",
    "businessIdentifiers",
    "addresses",
    "communicationChannels",
    "bankingAccounts",
    "paymentCards",
    "taxData",
    "marketingProfile",
    "inactiveStatus",
    "processingInformation",
    "additionalNotes",
    "organizationalData",
    "controlData"
})
public class PartnerType {

    @XmlElement(name = "PartnerClassification")
    protected CodeType partnerClassification;
    @XmlElement(name = "Roles")
    protected PartnerRolesType roles;
    @XmlElement(name = "PartnerKindChoice")
    protected PartnerKindChoiceType partnerKindChoice;
    @XmlElement(name = "BusinessIdentifiers")
    protected BusinessIdentifiersType businessIdentifiers;
    @XmlElement(name = "Addresses")
    protected AddressesType addresses;
    @XmlElement(name = "CommunicationChannels")
    protected CommunicationChannelsType communicationChannels;
    @XmlElement(name = "BankingAccounts")
    protected BankAccountsType bankingAccounts;
    @XmlElement(name = "PaymentCards")
    protected PaymentCardsType paymentCards;
    @XmlElement(name = "TaxData")
    protected TaxDataType taxData;
    @XmlElement(name = "MarketingProfile")
    protected MarketingProfileType marketingProfile;
    @XmlElement(name = "InactiveStatus")
    protected InactiveStatusType inactiveStatus;
    @XmlElement(name = "ProcessingInformation")
    protected ProcessingInformationType processingInformation;
    @XmlElement(name = "AdditionalNotes")
    protected AdditionalNotesType additionalNotes;
    @XmlElement(name = "OrganizationalData")
    protected OrganizationalDataType organizationalData;
    @XmlElement(name = "ControlData")
    protected ControlDataType controlData;

    /**
     * Ruft den Wert der partnerClassification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getPartnerClassification() {
        return partnerClassification;
    }

    /**
     * Legt den Wert der partnerClassification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setPartnerClassification(CodeType value) {
        this.partnerClassification = value;
    }

    /**
     * Ruft den Wert der roles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerRolesType }
     *     
     */
    public PartnerRolesType getRoles() {
        return roles;
    }

    /**
     * Legt den Wert der roles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerRolesType }
     *     
     */
    public void setRoles(PartnerRolesType value) {
        this.roles = value;
    }

    /**
     * Ruft den Wert der partnerKindChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerKindChoiceType }
     *     
     */
    public PartnerKindChoiceType getPartnerKindChoice() {
        return partnerKindChoice;
    }

    /**
     * Legt den Wert der partnerKindChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerKindChoiceType }
     *     
     */
    public void setPartnerKindChoice(PartnerKindChoiceType value) {
        this.partnerKindChoice = value;
    }

    /**
     * Ruft den Wert der businessIdentifiers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BusinessIdentifiersType }
     *     
     */
    public BusinessIdentifiersType getBusinessIdentifiers() {
        return businessIdentifiers;
    }

    /**
     * Legt den Wert der businessIdentifiers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessIdentifiersType }
     *     
     */
    public void setBusinessIdentifiers(BusinessIdentifiersType value) {
        this.businessIdentifiers = value;
    }

    /**
     * Ruft den Wert der addresses-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AddressesType }
     *     
     */
    public AddressesType getAddresses() {
        return addresses;
    }

    /**
     * Legt den Wert der addresses-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressesType }
     *     
     */
    public void setAddresses(AddressesType value) {
        this.addresses = value;
    }

    /**
     * Ruft den Wert der communicationChannels-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationChannelsType }
     *     
     */
    public CommunicationChannelsType getCommunicationChannels() {
        return communicationChannels;
    }

    /**
     * Legt den Wert der communicationChannels-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationChannelsType }
     *     
     */
    public void setCommunicationChannels(CommunicationChannelsType value) {
        this.communicationChannels = value;
    }

    /**
     * Ruft den Wert der bankingAccounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BankAccountsType }
     *     
     */
    public BankAccountsType getBankingAccounts() {
        return bankingAccounts;
    }

    /**
     * Legt den Wert der bankingAccounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BankAccountsType }
     *     
     */
    public void setBankingAccounts(BankAccountsType value) {
        this.bankingAccounts = value;
    }

    /**
     * Ruft den Wert der paymentCards-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardsType }
     *     
     */
    public PaymentCardsType getPaymentCards() {
        return paymentCards;
    }

    /**
     * Legt den Wert der paymentCards-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardsType }
     *     
     */
    public void setPaymentCards(PaymentCardsType value) {
        this.paymentCards = value;
    }

    /**
     * Ruft den Wert der taxData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TaxDataType }
     *     
     */
    public TaxDataType getTaxData() {
        return taxData;
    }

    /**
     * Legt den Wert der taxData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxDataType }
     *     
     */
    public void setTaxData(TaxDataType value) {
        this.taxData = value;
    }

    /**
     * Ruft den Wert der marketingProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MarketingProfileType }
     *     
     */
    public MarketingProfileType getMarketingProfile() {
        return marketingProfile;
    }

    /**
     * Legt den Wert der marketingProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketingProfileType }
     *     
     */
    public void setMarketingProfile(MarketingProfileType value) {
        this.marketingProfile = value;
    }

    /**
     * Ruft den Wert der inactiveStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InactiveStatusType }
     *     
     */
    public InactiveStatusType getInactiveStatus() {
        return inactiveStatus;
    }

    /**
     * Legt den Wert der inactiveStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InactiveStatusType }
     *     
     */
    public void setInactiveStatus(InactiveStatusType value) {
        this.inactiveStatus = value;
    }

    /**
     * Ruft den Wert der processingInformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProcessingInformationType }
     *     
     */
    public ProcessingInformationType getProcessingInformation() {
        return processingInformation;
    }

    /**
     * Legt den Wert der processingInformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessingInformationType }
     *     
     */
    public void setProcessingInformation(ProcessingInformationType value) {
        this.processingInformation = value;
    }

    /**
     * Ruft den Wert der additionalNotes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalNotesType }
     *     
     */
    public AdditionalNotesType getAdditionalNotes() {
        return additionalNotes;
    }

    /**
     * Legt den Wert der additionalNotes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalNotesType }
     *     
     */
    public void setAdditionalNotes(AdditionalNotesType value) {
        this.additionalNotes = value;
    }

    /**
     * Ruft den Wert der organizationalData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationalDataType }
     *     
     */
    public OrganizationalDataType getOrganizationalData() {
        return organizationalData;
    }

    /**
     * Legt den Wert der organizationalData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationalDataType }
     *     
     */
    public void setOrganizationalData(OrganizationalDataType value) {
        this.organizationalData = value;
    }

    /**
     * Ruft den Wert der controlData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ControlDataType }
     *     
     */
    public ControlDataType getControlData() {
        return controlData;
    }

    /**
     * Legt den Wert der controlData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ControlDataType }
     *     
     */
    public void setControlData(ControlDataType value) {
        this.controlData = value;
    }

}


package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;
import com.volkswagenag.xmldefs.dd.basictypes.LanguageCodeType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * A communication channel describes a way to contact a business
 *           partner.
 * 
 * <p>Java-Klasse für CommunicationChannelType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommunicationChannelType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommunicationChannelID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="IsStandardIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="AddressIDRef" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="ChannelTypeChoice" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ChannelTypeChoiceType" minOccurs="0"/>
 *         &lt;element name="IsStandardForChannelType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="CommunicationLanguage" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}LanguageCodeType" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="PreferedContactTime" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DoNotUseIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="Usage" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationChannelType", propOrder = {
    "communicationChannelID",
    "isStandardIND",
    "addressIDRef",
    "channelTypeChoice",
    "isStandardForChannelType",
    "communicationLanguage",
    "remark",
    "preferedContactTime",
    "doNotUseIND",
    "usage"
})
public class CommunicationChannelType {

    @XmlElement(name = "CommunicationChannelID")
    protected IdentifierType communicationChannelID;
    @XmlElement(name = "IsStandardIND")
    protected Boolean isStandardIND;
    @XmlElement(name = "AddressIDRef")
    protected IdentifierType addressIDRef;
    @XmlElement(name = "ChannelTypeChoice")
    protected ChannelTypeChoiceType channelTypeChoice;
    @XmlElement(name = "IsStandardForChannelType")
    protected Boolean isStandardForChannelType;
    @XmlElement(name = "CommunicationLanguage")
    protected LanguageCodeType communicationLanguage;
    @XmlElement(name = "Remark")
    protected TextType remark;
    @XmlElement(name = "PreferedContactTime")
    protected CodeType preferedContactTime;
    @XmlElement(name = "DoNotUseIND")
    protected Boolean doNotUseIND;
    @XmlElement(name = "Usage")
    protected CodeType usage;

    /**
     * Ruft den Wert der communicationChannelID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getCommunicationChannelID() {
        return communicationChannelID;
    }

    /**
     * Legt den Wert der communicationChannelID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setCommunicationChannelID(IdentifierType value) {
        this.communicationChannelID = value;
    }

    /**
     * Ruft den Wert der isStandardIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsStandardIND() {
        return isStandardIND;
    }

    /**
     * Legt den Wert der isStandardIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsStandardIND(Boolean value) {
        this.isStandardIND = value;
    }

    /**
     * Ruft den Wert der addressIDRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getAddressIDRef() {
        return addressIDRef;
    }

    /**
     * Legt den Wert der addressIDRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setAddressIDRef(IdentifierType value) {
        this.addressIDRef = value;
    }

    /**
     * Ruft den Wert der channelTypeChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ChannelTypeChoiceType }
     *     
     */
    public ChannelTypeChoiceType getChannelTypeChoice() {
        return channelTypeChoice;
    }

    /**
     * Legt den Wert der channelTypeChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelTypeChoiceType }
     *     
     */
    public void setChannelTypeChoice(ChannelTypeChoiceType value) {
        this.channelTypeChoice = value;
    }

    /**
     * Ruft den Wert der isStandardForChannelType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsStandardForChannelType() {
        return isStandardForChannelType;
    }

    /**
     * Legt den Wert der isStandardForChannelType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsStandardForChannelType(Boolean value) {
        this.isStandardForChannelType = value;
    }

    /**
     * Ruft den Wert der communicationLanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LanguageCodeType }
     *     
     */
    public LanguageCodeType getCommunicationLanguage() {
        return communicationLanguage;
    }

    /**
     * Legt den Wert der communicationLanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LanguageCodeType }
     *     
     */
    public void setCommunicationLanguage(LanguageCodeType value) {
        this.communicationLanguage = value;
    }

    /**
     * Ruft den Wert der remark-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getRemark() {
        return remark;
    }

    /**
     * Legt den Wert der remark-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setRemark(TextType value) {
        this.remark = value;
    }

    /**
     * Ruft den Wert der preferedContactTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getPreferedContactTime() {
        return preferedContactTime;
    }

    /**
     * Legt den Wert der preferedContactTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setPreferedContactTime(CodeType value) {
        this.preferedContactTime = value;
    }

    /**
     * Ruft den Wert der doNotUseIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoNotUseIND() {
        return doNotUseIND;
    }

    /**
     * Legt den Wert der doNotUseIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoNotUseIND(Boolean value) {
        this.doNotUseIND = value;
    }

    /**
     * Ruft den Wert der usage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getUsage() {
        return usage;
    }

    /**
     * Legt den Wert der usage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setUsage(CodeType value) {
        this.usage = value;
    }

}


package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für IndustrialSectorsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="IndustrialSectorsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IndustrySector" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}IndustrialSectorType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndustrialSectorsType", propOrder = {
    "industrySector"
})



public class IndustrialSectorsType {
	
	
    @XmlElement(name = "IndustrySector", required = true)
    private List<IndustrialSectorType> industrySector;


		public void setIndustrialSectorType(List<IndustrialSectorType> industrySector) {
			this.industrySector = industrySector;
		}
		
	    public List<IndustrialSectorType> getIndustrySector() {
	        if (industrySector == null) {
	            industrySector = new ArrayList<IndustrialSectorType>();
	        }
	        return this.industrySector;
	    }

}


package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * Details about an organisation (e.g. administration, corporate,
 *           dealer)
 * 
 * <p>Java-Klasse f�r OrganizationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OrganizationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Salutation" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}OrganizationNameType" minOccurs="0"/>
 *         &lt;element name="FoundationDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="LiquidationDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="LegalForm" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="IndustrySectors" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}IndustrialSectorsType" minOccurs="0"/>
 *         &lt;element name="RegistryEntries" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}RegistryEntriesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganizationType", propOrder = {
    "salutation",
    "name",
    "foundationDate",
    "liquidationDate",
    "legalForm",
    "industrySectors",
    "registryEntries"
})
public class OrganizationType {

    @XmlElement(name = "Salutation")
    protected CodeType salutation;
    @XmlElement(name = "Name")
    protected OrganizationNameType name;
    @XmlElement(name = "FoundationDate")
    protected String foundationDate;
    @XmlElement(name = "LiquidationDate")
    protected String liquidationDate;
    @XmlElement(name = "LegalForm")
    protected CodeType legalForm;
    @XmlElement(name = "IndustrySectors")
    protected IndustrialSectorsType industrySectors;
    @XmlElement(name = "RegistryEntries")
    protected RegistryEntriesType registryEntries;

    /**
     * Ruft den Wert der salutation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getSalutation() {
        return salutation;
    }

    /**
     * Legt den Wert der salutation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setSalutation(CodeType value) {
        this.salutation = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationNameType }
     *     
     */
    public OrganizationNameType getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationNameType }
     *     
     */
    public void setName(OrganizationNameType value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der foundationDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFoundationDate() {
        return foundationDate;
    }

    /**
     * Legt den Wert der foundationDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFoundationDate(String value) {
        this.foundationDate = value;
    }

    /**
     * Ruft den Wert der liquidationDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLiquidationDate() {
        return liquidationDate;
    }

    /**
     * Legt den Wert der liquidationDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLiquidationDate(String value) {
        this.liquidationDate = value;
    }

    /**
     * Ruft den Wert der legalForm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getLegalForm() {
        return legalForm;
    }

    /**
     * Legt den Wert der legalForm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setLegalForm(CodeType value) {
        this.legalForm = value;
    }

    /**
     * Ruft den Wert der industrySectors-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IndustrialSectorsType }
     *     
     */
    public IndustrialSectorsType getIndustrySectors() {
        return industrySectors;
    }

    /**
     * Legt den Wert der industrySectors-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IndustrialSectorsType }
     *     
     */
    public void setIndustrySectors(IndustrialSectorsType value) {
        this.industrySectors = value;
    }

    /**
     * Ruft den Wert der registryEntries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RegistryEntriesType }
     *     
     */
    public RegistryEntriesType getRegistryEntries() {
        return registryEntries;
    }

    /**
     * Legt den Wert der registryEntries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistryEntriesType }
     *     
     */
    public void setRegistryEntries(RegistryEntriesType value) {
        this.registryEntries = value;
    }

}

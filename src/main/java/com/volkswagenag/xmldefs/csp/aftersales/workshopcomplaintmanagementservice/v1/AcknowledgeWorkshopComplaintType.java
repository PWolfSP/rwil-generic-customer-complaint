
package com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.workshopcomplaint.WorkshopComplaintIdentifierType;

import lombok.Generated;


/**
 * reference to the created or updated workshop complaint
 * 
 * <p>Java-Klasse für AcknowledgeWorkshopComplaintType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AcknowledgeWorkshopComplaintType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}WorkshopComplaintRef"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcknowledgeWorkshopComplaintType", propOrder = {
    "workshopComplaintRef"
})
public class AcknowledgeWorkshopComplaintType {

    @XmlElement(name = "WorkshopComplaintRef", namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopComplaint", required = true)
    protected WorkshopComplaintIdentifierType workshopComplaintRef;

    /**
     * Ruft den Wert der workshopComplaintRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopComplaintIdentifierType }
     *     
     */
    public WorkshopComplaintIdentifierType getWorkshopComplaintRef() {
        return workshopComplaintRef;
    }

    /**
     * Legt den Wert der workshopComplaintRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopComplaintIdentifierType }
     *     
     */
    public void setWorkshopComplaintRef(WorkshopComplaintIdentifierType value) {
        this.workshopComplaintRef = value;
    }

}

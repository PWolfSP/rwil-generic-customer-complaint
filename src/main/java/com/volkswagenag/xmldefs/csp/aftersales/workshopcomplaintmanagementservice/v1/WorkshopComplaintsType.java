
package com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.workshopcomplaint.WorkshopComplaintType;

import lombok.Generated;


/**
 * <p>Java-Klasse für WorkshopComplaintsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="WorkshopComplaintsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}WorkshopComplaint" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkshopComplaintsType", propOrder = {
    "workshopComplaint"
})



public class WorkshopComplaintsType {
	
	
    @XmlElement(name = "WorkshopComplaint", namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopComplaint", required = true)
    private List<WorkshopComplaintType> workshopComplaint;


		public void setWorkshopComplaintType(List<WorkshopComplaintType> workshopComplaint) {
			this.workshopComplaint = workshopComplaint;
		}
		
	    public List<WorkshopComplaintType> getWorkshopComplaint() {
	        if (workshopComplaint == null) {
	            workshopComplaint = new ArrayList<WorkshopComplaintType>();
	        }
	        return this.workshopComplaint;
	    }

}


package com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.workshopcomplaint.WorkshopComplaintType;

import lombok.Generated;


/**
 * <p>Java-Klasse für GetWorkshopComplaintsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetWorkshopComplaintsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}WorkshopComplaint"/>
 *         &lt;element name="LanguageID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}LanguageTagCodeType" minOccurs="0"/>
 *         &lt;element name="ChunkID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetWorkshopComplaintsType", propOrder = {
    "workshopComplaint",
    "languageID",
    "chunkID"
})
public class GetWorkshopComplaintsType {

    @XmlElement(name = "WorkshopComplaint", namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopComplaint", required = true)
    protected WorkshopComplaintType workshopComplaint;
    @XmlElement(name = "LanguageID")
    protected String languageID;
    @XmlElement(name = "ChunkID")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String chunkID;

    /**
     * Ruft den Wert der workshopComplaint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopComplaintType }
     *     
     */
    public WorkshopComplaintType getWorkshopComplaint() {
        return workshopComplaint;
    }

    /**
     * Legt den Wert der workshopComplaint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopComplaintType }
     *     
     */
    public void setWorkshopComplaint(WorkshopComplaintType value) {
        this.workshopComplaint = value;
    }

    /**
     * Ruft den Wert der languageID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguageID() {
        return languageID;
    }

    /**
     * Legt den Wert der languageID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguageID(String value) {
        this.languageID = value;
    }

    /**
     * Ruft den Wert der chunkID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChunkID() {
        return chunkID;
    }

    /**
     * Legt den Wert der chunkID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChunkID(String value) {
        this.chunkID = value;
    }

}


package com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerIdentifierType;
import com.volkswagenag.xmldefs.dd.workshopcomplaint.WorkshopComplaintType;

import lombok.Generated;


/**
 * Dealer reference, identification of targeted organization (cpecified by Country,
 *           Brand und Dealer Number).
 * 
 * <p>Java-Klasse für ProcessWorkshopComplaintType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProcessWorkshopComplaintType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DealerRef"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}WorkshopComplaint"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessWorkshopComplaintType", propOrder = {
    "dealerRef",
    "workshopComplaint"
})
public class ProcessWorkshopComplaintType {

    @XmlElement(name = "DealerRef", namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", required = true)
    protected PartnerIdentifierType dealerRef;
    @XmlElement(name = "WorkshopComplaint", namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopComplaint", required = true)
    protected WorkshopComplaintType workshopComplaint;

    /**
     * Ruft den Wert der dealerRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getDealerRef() {
        return dealerRef;
    }

    /**
     * Legt den Wert der dealerRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setDealerRef(PartnerIdentifierType value) {
        this.dealerRef = value;
    }

    /**
     * Ruft den Wert der workshopComplaint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopComplaintType }
     *     
     */
    public WorkshopComplaintType getWorkshopComplaint() {
        return workshopComplaint;
    }

    /**
     * Legt den Wert der workshopComplaint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopComplaintType }
     *     
     */
    public void setWorkshopComplaint(WorkshopComplaintType value) {
        this.workshopComplaint = value;
    }

}

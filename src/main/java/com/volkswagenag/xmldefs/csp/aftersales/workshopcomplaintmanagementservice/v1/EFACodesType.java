
package com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.efacode.EFACodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EFACodesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EFACodesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/EFACode}EFACode" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EFACodesType", propOrder = {
    "efaCode"
})

public class EFACodesType {
	
	
    @XmlElement(name = "EFACode", namespace = "http://xmldefs.volkswagenag.com/DD/EFACode", required = true)
    private List<EFACodeType> efaCode;


		public void setEFACodeType(List<EFACodeType> efaCode) {
			this.efaCode = efaCode;
		}
		
	    public List<EFACodeType> getEFACode() {
	        if (efaCode == null) {
	            efaCode = new ArrayList<EFACodeType>();
	        }
	        return this.efaCode;
	    }

}

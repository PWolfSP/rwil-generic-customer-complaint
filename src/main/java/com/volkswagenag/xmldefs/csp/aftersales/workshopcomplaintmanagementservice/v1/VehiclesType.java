
package com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.vehicle.VehicleType;

import lombok.Generated;


/**
 * <p>Java-Klasse für VehiclesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VehiclesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/Vehicle}Vehicle" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehiclesType", propOrder = {
    "vehicle"
})



public class VehiclesType {
	
	
    @XmlElement(name = "Vehicle", namespace = "http://xmldefs.volkswagenag.com/DD/Vehicle", required = true)
    private List<VehicleType> vehicle;


		public void setVehicleType(List<VehicleType> vehicle) {
			this.vehicle = vehicle;
		}
		
	    public List<VehicleType> getVehicle() {
	        if (vehicle == null) {
	            vehicle = new ArrayList<VehicleType>();
	        }
	        return this.vehicle;
	    }

}
